package net.ihe.gazelle.common.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <b>Class Description : </b>PathLinkingADocument<br>
 * <br>
 * This class stores the path of a document (eg. DICOM Conformance Statement, HL7 Conformance Statement, etc... )
 * <p/>
 * PathLinkingADocument possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the PathLinkingADocument</li>
 * <li><b>path</b> : String of the filename containing the document</li>
 * <li><b>comment</b> : String describing the document file</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         jchatel@irisa.fr
 *         </pre>
 * @version 1.0 , 2009, March 5th
 * @class PathLinkingADocument.java
 * @package net.ihe.gazelle.common.file.model
 */

@Entity
@Table(name = "cmn_path_linking_a_document", schema = "public")
@SequenceGenerator(name = "cmn_path_linking_a_document_sequence", sequenceName = "cmn_path_linking_a_document_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class PathLinkingADocument implements Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450973231501093760L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cmn_path_linking_a_document_sequence")
    private Integer id;

    @Column(name = "path")
    @NotNull
    private String path;

    @Column(name = "type")
    @NotNull
    private String type;

    @Column(name = "comment")
    private String comment;

    public PathLinkingADocument() {

    }

    public PathLinkingADocument(PathLinkingADocument testToCopy) {
        if (testToCopy.getPath() != null) {
            path = new String(testToCopy.getPath());
        }
        if (testToCopy.getType() != null) {
            type = new String(testToCopy.getType());
        }
        if (testToCopy.getComment() != null) {
            comment = new String(testToCopy.getComment());
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
