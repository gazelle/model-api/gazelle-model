package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.factories.IntegrationProfileFactory;
import net.ihe.gazelle.tf.model.factories.IntegrationProfileStatusTypeFactory;
import net.jcip.annotations.NotThreadSafe;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@NotThreadSafe
public class IntegrationProfileTest {

    List<IntegrationProfile> validForTestingSession = new ArrayList<IntegrationProfile>();
    List<IntegrationProfile> invalidForTestingSession = new ArrayList<IntegrationProfile>();
    @Before
    public void init(){
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(EntityManagerService.provideEntityManager());
    }

    @After
    public void tearDown() throws Exception {
        cleanData();
    }

    private void cleanData() {
        IntegrationProfileFactory.cleanIntegrationProfiles();
        IntegrationProfileStatusTypeFactory.cleanIntegrationProfileStatusType();
    }

    @Test
    public void find_Integration_profile_for_testing_session() {
        createStatusTypes();
        createIntegrationProfileForTest();

        List<IntegrationProfile> allIntegrationProfileForTestingSession = IntegrationProfile
                .getAllIntegrationProfileForTestingSession();

        assertEquals(2, allIntegrationProfileForTestingSession.size());
        assertTrue(validForTestingSession.containsAll(allIntegrationProfileForTestingSession));
        assertFalse(invalidForTestingSession.containsAll(allIntegrationProfileForTestingSession));
    }

    private void createStatusTypes() {

        createStatusType(IntegrationProfileStatusType.DEPRECATED_KEYWORD);
        createStatusType(IntegrationProfileStatusType.FINALTEXT_KEYWORD);
        createStatusType(IntegrationProfileStatusType.TRIALIMPLEMENTATION_KEYWORD);
        createStatusType(IntegrationProfileStatusType.PROPOSED_KEYWORD);
    }

    private void createStatusType(String status) {
        IntegrationProfileStatusType integrationProfileStatusTypes = IntegrationProfileStatusTypeFactory
                .provideIntegrationProfileStatusTypes();
        integrationProfileStatusTypes.setKeyword(status);
        DatabaseManager.writeObject(integrationProfileStatusTypes);
    }

    private void createIntegrationProfileForTest() {
        IntegrationProfile integrationProfileWithMandatoryFields = IntegrationProfileFactory
                .provideIntegrationProfileWithMandatoryFields();
        integrationProfileWithMandatoryFields.setIntegrationProfileStatusType(getTrialImplementationStatus());
        validForTestingSession.add((IntegrationProfile) DatabaseManager
                .writeObject(integrationProfileWithMandatoryFields));

        integrationProfileWithMandatoryFields = IntegrationProfileFactory
                .provideIntegrationProfileWithMandatoryFields();
        integrationProfileWithMandatoryFields.setIntegrationProfileStatusType(getFinalTextStatus());
        validForTestingSession.add((IntegrationProfile) DatabaseManager
                .writeObject(integrationProfileWithMandatoryFields));

        integrationProfileWithMandatoryFields = IntegrationProfileFactory
                .provideIntegrationProfileWithMandatoryFields();
        integrationProfileWithMandatoryFields.setIntegrationProfileStatusType(getDeprecatedStatus());
        invalidForTestingSession.add((IntegrationProfile) DatabaseManager
                .writeObject(integrationProfileWithMandatoryFields));
    }

    private IntegrationProfileStatusType getTrialImplementationStatus() {
        return getIntegrationProfileStatus(IntegrationProfileStatusType.TRIALIMPLEMENTATION_KEYWORD);
    }

    private IntegrationProfileStatusType getFinalTextStatus() {
        return getIntegrationProfileStatus(IntegrationProfileStatusType.FINALTEXT_KEYWORD);
    }

    private IntegrationProfileStatusType getDeprecatedStatus() {
        return getIntegrationProfileStatus(IntegrationProfileStatusType.DEPRECATED_KEYWORD);
    }

    private IntegrationProfileStatusType getIntegrationProfileStatus(String status) {
        IntegrationProfileStatusTypeQuery query = new IntegrationProfileStatusTypeQuery();
        query.keyword().eq(status);
        IntegrationProfileStatusType TrialStatus = query.getUniqueResult();
        return TrialStatus;
    }

}
