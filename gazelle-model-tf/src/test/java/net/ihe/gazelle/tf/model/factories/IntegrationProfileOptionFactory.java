package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tf.model.IntegrationProfileOptionQuery;

import java.util.List;

public class IntegrationProfileOptionFactory {
    private static int number = 0;

    public static IntegrationProfileOption createIntegrationProfileOptionWithMandatoryFields() {
        IntegrationProfileOption integrationProfileOption = new IntegrationProfileOption();
        integrationProfileOption.setKeyword("INT_PROFILE_OPTION" + number);
        number++;
        integrationProfileOption = (IntegrationProfileOption) DatabaseManager.writeObject(integrationProfileOption);
        return integrationProfileOption;
    }

    public static IntegrationProfileOption createNoneIntegrationProfileOption() {
        IntegrationProfileOption integrationProfileOption = provideNoneIntegrationProfileOption();
        IntegrationProfileOptionQuery query = new IntegrationProfileOptionQuery();
        query.keyword().eq(integrationProfileOption.getKeyword());
        IntegrationProfileOption uniqueResult = query.getUniqueResult();
        if (uniqueResult == null) {
            integrationProfileOption = (IntegrationProfileOption) DatabaseManager.writeObject(integrationProfileOption);
        }
        return integrationProfileOption;
    }

    private static IntegrationProfileOption provideNoneIntegrationProfileOption() {
        IntegrationProfileOption integrationProfileOption = new IntegrationProfileOption();
        integrationProfileOption.setKeyword("NONE");
        return integrationProfileOption;
    }

    public static void cleanIntegrationProfileOptions() {
        IntegrationProfileOptionQuery query = new IntegrationProfileOptionQuery();
        List<IntegrationProfileOption> integrationProfilesOption = query.getListDistinct();
        for (IntegrationProfileOption integrationProfileOption : integrationProfilesOption) {
            DatabaseManager.removeObject(integrationProfileOption);
        }
    }
}
