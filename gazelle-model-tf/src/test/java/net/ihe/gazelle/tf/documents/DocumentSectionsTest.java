package net.ihe.gazelle.tf.documents;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.documents.factories.DocumentFactory;
import net.ihe.gazelle.tf.documents.factories.DocumentSectionFactory;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.factories.*;
import net.jcip.annotations.NotThreadSafe;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@NotThreadSafe
public class DocumentSectionsTest {

    String[] sectionsTab = {"reference", "ref", "title1", "title2", "table 1.0", "title 2.0"};

    @Before
    public void init(){
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(EntityManagerService.provideEntityManager());
    }

    @After
    public void tearDown() throws Exception {
        cleanData();
    }

    @Test
    public void test_documentSection_remove_underscores_from_name() {
        DocumentSectionFactory.createSection(null, "1_0_1_1_My_title_is_cluttered");
        List<DocumentSection> docSection = DocumentSectionFactory
                .findDocumentSectionByName("1_0_1_1_My_title_is_cluttered");
        assertEquals(1, docSection.size());
        DocumentSection section = docSection.get(0);
        assertEquals("1_0_1_1 My title is cluttered", section.getSectionHumanized());
    }

    @Test
    public void test_documentSection_remove_underscores_from_name2() {
        DocumentSectionFactory.createSection(null, "1_0_1_111_1112_My_title_is_cluttered");
        List<DocumentSection> docSection = DocumentSectionFactory
                .findDocumentSectionByName("1_0_1_111_1112_My_title_is_cluttered");
        assertEquals(1, docSection.size());
        DocumentSection section = docSection.get(0);
        assertEquals("1_0_1_111_1112 My title is cluttered", section.getSectionHumanized());
    }

    @Test
    public void test_documentSection_remove_underscores_from_name3() {
        DocumentSectionFactory.createSection(null, "Figure_3_40_2__Sample_attribute");
        List<DocumentSection> docSection = DocumentSectionFactory
                .findDocumentSectionByName("Figure_3_40_2__Sample_attribute");
        assertEquals(1, docSection.size());
        DocumentSection section = docSection.get(0);
        assertEquals("Figure_3_40_2  Sample attribute", section.getSectionHumanized());
    }

    @Test
    public void test_documentSection_remove_underscores_from_name4() {
        DocumentSectionFactory.createSection(null, "Sample_attribute");
        List<DocumentSection> docSection = DocumentSectionFactory.findDocumentSectionByName("Sample_attribute");
        assertEquals(1, docSection.size());
        DocumentSection section = docSection.get(0);
        assertEquals("Sample attribute", section.getSectionHumanized());
    }

    @Test
    public void test_oneDocument_is_linked_to_DocumentSection() {
        generateData();
        DocumentSectionQuery query = new DocumentSectionQuery();
        query.eq(query.getListDistinct().get(0));
        List<Document> linkedDoc = query.document().getListDistinct();
        int nb = linkedDoc.size();
        assertEquals(1, nb);
        assertEquals("testing", linkedDoc.get(0).getName());
    }

    @Test
    public void test_documentSection_linked_to_a_document() {
        Document doc = generateData();

        DocumentSectionQuery documentSectionQuery = new DocumentSectionQuery();
        documentSectionQuery.document().eq(doc);
        List<DocumentSection> docSections = documentSectionQuery.getList();

        assertEquals(sectionsTab.length, docSections.size());

        List<String> sectionList = Arrays.asList(sectionsTab);
        int i = 0;
        for (i = 0; i < sectionsTab.length; i++) {
            assertEquals(docSections.get(i).getSection() + " Not found", true,
                    sectionList.contains(docSections.get(i).getSection()));
        }
    }

    @Test
    public void test_getPdfReference_from_section() {
        Document doc = generateData_for_getPdfRefence();

        DocumentSectionQuery documentSectionQuery = new DocumentSectionQuery();
        documentSectionQuery.document().eq(doc);
        List<DocumentSection> docSections = documentSectionQuery.getList();

        assertEquals(1, docSections.size());
        assertEquals(doc.getUrl() + "#1.0 title", docSections.get(0).getPdfReference());
        assertEquals(doc.getName() + " #1.0 title", docSections.get(0).getReferenceTitle());

        assertEquals(DocumentSectionType.TITLE, docSections.get(0).getType());
    }

    @Test
    public void test_getReferenceType_from_section() {
        Document doc = generateData();

        DocumentSectionQuery documentSectionQuery = new DocumentSectionQuery();
        documentSectionQuery.document().eq(doc);
        List<DocumentSection> docSections = documentSectionQuery.getList();

        assertEquals(6, docSections.size());
        for (DocumentSection section : docSections) {
            assertEquals(DocumentSectionType.TITLE, section.getType());
        }
    }

    @Test
    public void test_isLinkedToOtherEntities_should_be_false_when_not_linked() {
        Document doc = DocumentFactory.create_document();
        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");
        assertFalse(section.isLinkedToOtherEntities());

        DatabaseManager.update(section);

        assertFalse(section.isLinkedToOtherEntities());
    }

    @Test
    public void test_isLinkedToOtherEntities_should_be_true_when_linked_to_a_transaction() {
        Document doc = DocumentFactory.create_document();

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");

        Transaction transaction = TransactionFactory.createTransactionWithMandatoryFields();
        transaction.setDocumentSection(section);
        transaction = (Transaction) DatabaseManager.writeObject(transaction);

        DatabaseManager.update(transaction, section, doc);

        assertNotNull(section.getDocument());
        assertNotNull(doc.getDocumentSection());
        assertEquals(1, section.getTransaction().size());

        assertTrue(section.isLinkedToOtherEntities());
    }

    @Test
    public void test_isLinkedToOtherEntities_should_be_true_when_linked_to_an_ActorIntegrationProfileOption() {
        Document doc = DocumentFactory.create_document();

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");

        ActorIntegrationProfileOption aipo = ActorIntegrationProfileOptionFactory
                .createActorIntegrationProfileOptionWithMandatoryFields();
        aipo.setDocumentSection(section);
        DatabaseManager.writeObject(aipo);

        DatabaseManager.update(aipo, section, doc);

        assertNotNull(section.getDocument());
        assertNotNull(doc.getDocumentSection());
        assertEquals(1, section.getActorIntegrationProfileOption().size());

        assertTrue(section.isLinkedToOtherEntities());
    }

    @Test
    public void test_isLinkedToOtherEntities_should_be_true_when_linked_to_an_integrationProfile() {
        Document doc = DocumentFactory.create_document();

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");

        IntegrationProfile integrationProfile = IntegrationProfileFactory.createIntegrationProfileWithMandatoryFields();
        integrationProfile.setDocumentSection(section);
        DatabaseManager.writeObject(integrationProfile);

        DatabaseManager.update(section);

        assertEquals(1, section.getIntegrationProfile().size());

        assertTrue(section.isLinkedToOtherEntities());
    }

    @Test
    public void test_isLinkedToOtherEntities_should_be_true_when_linked_to_and_an_integrationProfileOption_and_A_Transaction() {
        Document doc = DocumentFactory.create_document();

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");

        Transaction transaction = TransactionFactory.createTransactionWithMandatoryFields();
        transaction.setDocumentSection(section);
        DatabaseManager.writeObject(transaction);

        IntegrationProfile integrationProfile = IntegrationProfileFactory.createIntegrationProfileWithMandatoryFields();
        integrationProfile.setDocumentSection(section);
        DatabaseManager.writeObject(integrationProfile);

        DatabaseManager.update(section);

        assertEquals(1, section.getIntegrationProfile().size());
        assertEquals(1, section.getTransaction().size());
        assertTrue(section.isLinkedToOtherEntities());
    }

    public Document generateData() {
        Document doc = DocumentFactory.create_document();
        for (String section : sectionsTab) {
            DocumentSectionFactory.createSection(doc, section);
        }
        return doc;
    }

    public Document generateData_for_getPdfRefence() {
        Document doc = DocumentFactory.create_document();
        DocumentSectionFactory.createSection(doc, "1.0 title");
        return doc;
    }

    public void cleanData() {
        ActorIntegrationProfileOptionFactory.cleanActorIntegrationProfileOptions();
        TransactionFactory.cleanTransactions();
        IntegrationProfileFactory.cleanIntegrationProfiles();
        IntegrationProfileOptionFactory.cleanIntegrationProfileOptions();
        DocumentSectionFactory.cleanDocumentSections();
        DocumentFactory.cleanDocuments();
        DomainFactory.cleanDomains();

    }
}
