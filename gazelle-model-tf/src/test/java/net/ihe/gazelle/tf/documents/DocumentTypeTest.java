package net.ihe.gazelle.tf.documents;

import junit.framework.TestCase;
import net.ihe.gazelle.tf.model.DocumentType;

public class DocumentTypeTest extends TestCase {

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testEnumeration() {
        assertEquals("Supplement", DocumentType.SUPPLEMENT.getFriendlyName());
        assertEquals("Technical framework", DocumentType.TECHNICAL_FRAMEWORK.getFriendlyName());
        assertEquals("White paper", DocumentType.WHITE_PAPER.getFriendlyName());
        assertEquals("User handbook", DocumentType.USER_HANDBOOK.getFriendlyName());
        assertEquals("User handbook", DocumentType.valueOf("USER_HANDBOOK").getFriendlyName());
    }

    public void testEnumeration_keywords() {
        assertEquals(DocumentType.SUPPLEMENT, DocumentType.valueOfByKeyword("Suppl"));
        assertEquals(DocumentType.TECHNICAL_FRAMEWORK, DocumentType.valueOfByKeyword("TF"));
        assertEquals(DocumentType.WHITE_PAPER, DocumentType.valueOfByKeyword("White-paper"));
        assertEquals(DocumentType.USER_HANDBOOK, DocumentType.valueOfByKeyword("UH"));
    }

    public void testEnumeration_keywordsFails() {
        boolean thrown = false;
        try {
            DocumentType.valueOfByKeyword("ignoredKeyword_ShouldFail");
        } catch (IllegalArgumentException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }
}
