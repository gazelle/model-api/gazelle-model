package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileQuery;

import java.util.List;
import java.util.Random;

public class IntegrationProfileFactory {
    public static IntegrationProfile createIntegrationProfileWithMandatoryFields() {
        IntegrationProfile integrationProfile = provideIntegrationProfileWithMandatoryFields();
        integrationProfile = (IntegrationProfile) DatabaseManager.writeObject(integrationProfile);
        return integrationProfile;
    }

    public static IntegrationProfile provideIntegrationProfileWithMandatoryFields() {
        IntegrationProfile integrationProfile = new IntegrationProfile();
        Random randomGenerator = new Random();
        integrationProfile.setKeyword("INT_PROFILE" + randomGenerator.nextInt());
        integrationProfile.setName("INT_PROFILE_NAME" + randomGenerator.nextInt());
        return integrationProfile;
    }

    public static void cleanIntegrationProfiles() {
        IntegrationProfileQuery query = new IntegrationProfileQuery();
        List<IntegrationProfile> integrationProfiles = query.getListDistinct();
        for (IntegrationProfile integrationProfile : integrationProfiles) {
            DatabaseManager.removeObject(integrationProfile);
        }
    }
}
