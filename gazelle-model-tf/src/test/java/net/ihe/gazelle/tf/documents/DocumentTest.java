package net.ihe.gazelle.tf.documents;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.documents.factories.DocumentFactory;
import net.ihe.gazelle.tf.documents.factories.DocumentSectionFactory;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.factories.ActorIntegrationProfileOptionFactory;
import net.ihe.gazelle.tf.model.factories.DomainFactory;
import net.ihe.gazelle.tf.model.factories.IntegrationProfileFactory;
import net.ihe.gazelle.tf.model.factories.TransactionFactory;
import net.jcip.annotations.NotThreadSafe;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

@NotThreadSafe
public class DocumentTest {
    @Before
    public void init(){
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(EntityManagerService.provideEntityManager());
    }

    @After
    public void tearDown() throws Exception {
        cleanData();
    }

    private int get_Document_Table_Size() {
        DocumentQuery query = new DocumentQuery();
        return query.getList().size();
    }

    @Test
    public void test_Document_has_a_valid_factory() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        DatabaseManager.writeObject(doc);

        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    @Test
    public void test_Document_getName() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc.setName("Testing_name");

        DatabaseManager.writeObject(doc);

        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
        assertEquals("Testing name", doc.getName());
    }

    @Test
    public void test_Document_getTitle() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc.setTitle("Testing_title");

        DatabaseManager.writeObject(doc);

        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
        assertEquals("Testing title", doc.getTitle());
    }

    @Test
    public void test_Document_linked_to_one_documentSection() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        DatabaseManager.writeObject(doc);

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");

        DatabaseManager.writeObject(section);

        assertFalse(section.isLinkedToOtherEntities());
        assertFalse(doc.hasDocumentSectionsLinkedToOtherEntities());

        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    @Test
    public void test_Document_linked_to_one_documentSection_linked_to_a_transaction() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc = (Document) DatabaseManager.writeObject(doc);

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");
        section = (DocumentSection) DatabaseManager.writeObject(section);

        Transaction transaction = TransactionFactory.createTransactionWithMandatoryFields();
        transaction.setDocumentSection(section);
        transaction = (Transaction) DatabaseManager.writeObject(transaction);

        DatabaseManager.update(transaction, section, doc);

        assertTrue(section.isLinkedToOtherEntities());
        assertTrue(doc.hasDocumentSectionsLinkedToOtherEntities());

        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    @Test
    public void test_Document_linked_to_one_documentSection_linked_to_an_AIPO() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc = (Document) DatabaseManager.writeObject(doc);

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");
        section = (DocumentSection) DatabaseManager.writeObject(section);

        ActorIntegrationProfileOption aipo = ActorIntegrationProfileOptionFactory
                .createActorIntegrationProfileOptionWithMandatoryFields();
        aipo.setDocumentSection(section);
        aipo = (ActorIntegrationProfileOption) DatabaseManager.writeObject(aipo);

        DatabaseManager.update(aipo, section, doc);

        assertTrue(section.isLinkedToOtherEntities());
        assertTrue(doc.hasDocumentSectionsLinkedToOtherEntities());

        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    @Test
    public void test_Document_linked_to_multiple_documentSections_not_linked_to_other_entities() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc = (Document) DatabaseManager.writeObject(doc);

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");
        section = (DocumentSection) DatabaseManager.writeObject(section);

        DocumentSection section2 = DocumentSectionFactory.createSection(doc, "2.0 title");
        section2 = (DocumentSection) DatabaseManager.writeObject(section2);

        DocumentSection section3 = DocumentSectionFactory.createSection(doc, "3.0 title");
        section3 = (DocumentSection) DatabaseManager.writeObject(section3);

        DatabaseManager.update(section, section2, section3, doc);

        assertFalse(section.isLinkedToOtherEntities());
        assertFalse(section2.isLinkedToOtherEntities());
        assertFalse(section3.isLinkedToOtherEntities());
        assertFalse(doc.hasDocumentSectionsLinkedToOtherEntities());
        assertTrue(doc.isNotLinkedToOtherEntities());

        assertEquals("3 documentSections should be linked to the document", 3, doc.getDocumentSection().size());
        int endSize = get_Document_Table_Size();
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    @Test
    public void test_Document_linked_to_multiple_documentSections_with_one_linked_to_an_integrationProfile() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc = (Document) DatabaseManager.writeObject(doc);

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");
        section = (DocumentSection) DatabaseManager.writeObject(section);

        DocumentSection section2 = DocumentSectionFactory.createSection(doc, "2.0 title");
        section2 = (DocumentSection) DatabaseManager.writeObject(section2);

        DocumentSection section3 = DocumentSectionFactory.createSection(doc, "3.0 title");
        section3 = (DocumentSection) DatabaseManager.writeObject(section3);

        IntegrationProfile integrationProfile = IntegrationProfileFactory.createIntegrationProfileWithMandatoryFields();
        integrationProfile.setDocumentSection(section3);
        DatabaseManager.writeObject(integrationProfile);

        DatabaseManager.update(section, section2, section3, doc);

        assertFalse(section.isLinkedToOtherEntities());
        assertFalse(section2.isLinkedToOtherEntities());
        assertTrue(section3.isLinkedToOtherEntities());
        assertTrue("One documentSection is linked to an integration profile",
                doc.hasDocumentSectionsLinkedToOtherEntities());
        assertFalse(doc.isNotLinkedToOtherEntities());

        assertEquals("3 documentSections should be linked to the document", 3, doc.getDocumentSection().size());
        int endSize = get_Document_Table_Size();

        assertEquals(1, doc.getIntegrationProfileLinked().size());
        assertEquals(0, doc.getTransactionsLinked().size());
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    @Test
    public void test_Document_linked_to_multiple_documentSections_linked_to_other_entities() {
        int initSize = get_Document_Table_Size();

        Document doc = DocumentFactory.createDocumentWithMandatoryFields();
        doc = (Document) DatabaseManager.writeObject(doc);

        DocumentSection section = DocumentSectionFactory.createSection(doc, "1.0 title");
        section = (DocumentSection) DatabaseManager.writeObject(section);

        DocumentSection section2 = DocumentSectionFactory.createSection(doc, "2.0 title");
        section2 = (DocumentSection) DatabaseManager.writeObject(section2);

        DocumentSection section3 = DocumentSectionFactory.createSection(doc, "3.0 title");
        section3 = (DocumentSection) DatabaseManager.writeObject(section3);

        IntegrationProfile integrationProfile = IntegrationProfileFactory.createIntegrationProfileWithMandatoryFields();
        integrationProfile.setDocumentSection(section);
        DatabaseManager.writeObject(integrationProfile);

        integrationProfile = IntegrationProfileFactory.createIntegrationProfileWithMandatoryFields();
        integrationProfile.setDocumentSection(section2);
        DatabaseManager.writeObject(integrationProfile);

        integrationProfile = IntegrationProfileFactory.createIntegrationProfileWithMandatoryFields();
        integrationProfile.setDocumentSection(section3);
        DatabaseManager.writeObject(integrationProfile);

        DatabaseManager.update(section, section2, section3, doc);

        assertTrue(section.isLinkedToOtherEntities());
        assertTrue(section2.isLinkedToOtherEntities());
        assertTrue(section3.isLinkedToOtherEntities());
        assertTrue("One documentSection is linked to an integration profile",
                doc.hasDocumentSectionsLinkedToOtherEntities());
        assertFalse(doc.isNotLinkedToOtherEntities());

        assertEquals("3 documentSections should be linked to the document", 3, doc.getDocumentSection().size());
        int endSize = get_Document_Table_Size();

        assertEquals(3, doc.getIntegrationProfileLinked().size());
        assertEquals(0, doc.getTransactionsLinked().size());
        assertEquals("1 document should be stored", 1, howManyNewItems(initSize, endSize));
    }

    private int howManyNewItems(int initSize, int endSize) {
        return endSize - initSize;
    }

    public void cleanData() {
        ActorIntegrationProfileOptionFactory.cleanActorIntegrationProfileOptions();
        IntegrationProfileFactory.cleanIntegrationProfiles();
        TransactionFactory.cleanTransactions();
        DocumentSectionFactory.cleanDocumentSections();
        DocumentFactory.cleanDocuments();
        DomainFactory.cleanDomains();
    }

}
