package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.TransactionQuery;

import java.util.List;

public class TransactionFactory {

    public static Transaction createTransactionWithMandatoryFields() {
        Transaction transaction = new Transaction();
        transaction.setKeyword("TRANS");
        transaction.setDescription("transaction description");
        transaction.setName("transaction name");
        return transaction;
    }

    public static void cleanTransactions() {
        TransactionQuery query = new TransactionQuery();
        List<Transaction> transactions = query.getListDistinct();
        for (Transaction transaction : transactions) {
            DatabaseManager.removeObject(transaction);
        }
    }
}
