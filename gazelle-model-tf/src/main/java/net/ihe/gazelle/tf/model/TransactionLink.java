/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * <b>Class Description : </b>TransactionLink<br>
 * <br>
 * This class describes the TransactionLink option type object, used by the Gazelle application. This class belongs to the Technical Framework module.
 */

@XmlRootElement(name = "TransactionLink")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_transaction_link", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "from_actor_id", "to_actor_id", "transaction_id"}))
@SequenceGenerator(name = "tf_transaction_link_sequence", sequenceName = "tf_transaction_link_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class TransactionLink extends AuditedObject implements java.io.Serializable,
        java.lang.Comparable<TransactionLink> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -1654698516854496L;
    private static Logger log = LoggerFactory.getLogger(TransactionLink.class);
    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_transaction_link_sequence")
    @XmlTransient
    private Integer id;

    // Variables used for Foreign Keys : Actor
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.Actor.class)
    @JoinColumn(name = "from_actor_id")
    @Fetch(value = FetchMode.SELECT)
    private Actor fromActor;

    // Variables used for Foreign Keys : Actor
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.Actor.class)
    @JoinColumn(name = "to_actor_id")
    @Fetch(value = FetchMode.SELECT)
    private Actor toActor;

    // Variables used for Foreign Keys : Transaction
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.Transaction.class)
    @JoinColumn(name = "transaction_id")
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private Transaction transaction;

    // Constructors
    public TransactionLink() {
    }

    public TransactionLink(Integer id, Actor fromActor, Transaction transaction, Actor toActor) {
        this.id = id;
        this.fromActor = fromActor;
        this.transaction = transaction;
        this.toActor = toActor;
    }

    public TransactionLink(Actor fromActor, Transaction transaction, Actor toActor) {
        this.fromActor = fromActor;
        this.transaction = transaction;
        this.toActor = toActor;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<TransactionLink> getTransactionLinkForActor(Actor inActor) {
        if (inActor != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            Query query = entityManager.createQuery("SELECT DISTINCT tl FROM TransactionLink tl "
                    + "WHERE tl.fromActor=:inActor OR tl.toActor=:inActor");
            query.setParameter("inActor", inActor);
            List<TransactionLink> transactionLinksForActor = query.getResultList();
            if (transactionLinksForActor.size() > 0) {
                return transactionLinksForActor;
            }
        }
        return null;
    }

    public static List<Actor> getFromActorsForTransaction(Transaction inTransaction) {
        if (inTransaction != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            Query query = entityManager.createQuery("SELECT DISTINCT tl.fromActor FROM TransactionLink tl "
                    + "WHERE tl.transaction=:inTransaction");
            query.setParameter("inTransaction", inTransaction);
            List<Actor> fromActorsForTransaction = query.getResultList();

            if (fromActorsForTransaction.size() > 0) {
                return fromActorsForTransaction;
            }
        }
        return null;
    }

    public static List<Actor> getToActorsForTransaction(Transaction inTransaction) {
        if (inTransaction != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            Query query = entityManager.createQuery("SELECT DISTINCT tl.toActor FROM TransactionLink tl "
                    + "WHERE tl.transaction=:inTransaction");
            query.setParameter("inTransaction", inTransaction);
            List<Actor> toActorsForTransaction = query.getResultList();

            if (toActorsForTransaction.size() > 0) {
                return toActorsForTransaction;
            }
        }
        return null;
    }

    public static List<Actor> getAllToActors() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Query query = entityManager
                .createQuery("SELECT DISTINCT tl.toActor FROM TransactionLink tl ORDER BY tl.toActor.keyword");
        List<Actor> toActors = query.getResultList();
        return toActors;
    }

    public static List<Actor> getAllFromActors() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Query query = entityManager
                .createQuery("SELECT DISTINCT tl.fromActor FROM TransactionLink tl ORDER BY tl.fromActor.keyword");
        List<Actor> fromActors = query.getResultList();
        return fromActors;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Actor getFromActor() {
        return fromActor;
    }

    public void setFromActor(Actor a) {
        fromActor = a;
    }

    public Actor getToActor() {
        return toActor;
    }

    public void setToActor(Actor a) {
        toActor = a;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction t) {
        transaction = t;
    }

    @Override
    public int compareTo(TransactionLink tl) {
        String str1 = tl.getFromActor().getKeyword();
        String str2 = this.getFromActor().getKeyword();
        return str2.compareTo(str1);
    }

    public void saveOrMerge(EntityManager entityManager, Transaction transaction) {
        this.id = null;

        this.transaction = transaction;
        fromActor.setLastModifierId(this.lastModifierId);
        fromActor.saveOrMerge(entityManager);
        toActor.setLastModifierId(this.lastModifierId);
        toActor.saveOrMerge(entityManager);
        TransactionLinkQuery query = new TransactionLinkQuery();

        query.fromActor().eq(this.fromActor);
        query.transaction().eq(this.transaction);
        query.toActor().eq(this.toActor);
        List<TransactionLink> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
