package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>AffinityDomain<br>
 * <br>
 * This class describes the AffinityDomain object used to identify in which affinity domain an HL7 message profile is applicable
 * <p/>
 * AffinityDomain possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of AffinityDomain in database</li>
 * <li><b>keyword</b> : keyword</li>
 * <li><b>labelToDisplay</b> : label displayed in the GUI</li>
 * <li><b>description</b> : short description</li>
 * </ul>
 * </br>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2011, June 23rd
 * @class AffinityDomain
 * @package net.ihe.gazelle.tf.model
 * @see > Aberge@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "affinityDomain")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_affinity_domain", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_affinity_domain_sequence", sequenceName = "tf_affinity_domain_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class AffinityDomain implements Serializable {


    private static final long serialVersionUID = 2579912986056109499L;
    private static Logger log = LoggerFactory.getLogger(AffinityDomain.class);
    @Id
    @GeneratedValue(generator = "tf_affinity_domain_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @XmlTransient
    private Integer id;

    @Column(name = "keyword")
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    public AffinityDomain() {

    }

    public static List<AffinityDomain> getAllAffinityDomains() {
        return new AffinityDomainQuery().getListNullIfEmpty();
    }

    @FilterLabel
    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }


    public void saveOrMerge(EntityManager entityManager) {
        this.setId(null);
        AffinityDomainQuery query = new AffinityDomainQuery();
        query.keyword().eq(this.keyword);
        List<AffinityDomain> listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
