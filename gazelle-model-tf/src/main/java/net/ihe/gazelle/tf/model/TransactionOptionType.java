/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>TransactionOptionType<br>
 * <br>
 * This class describes the Transaction option type object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * TransactionOptionType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Transaction option type</li>
 * <li><b>keyword</b> : keyword for the transaction option type
 * <li><b>name</b> : name corresponding to the Transaction option type</li>
 * <li><b>description</b> : description corresponding to the Transaction option type</li>
 * </ul>
 * </br> <b>Example of Transaction option type</b> : " 'R' : 'Required' " is an IHE Transaction option type<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class Transaction.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlRootElement(name = "TransactionOptionType")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_transaction_option_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "tf_transaction_option_type_sequence", sequenceName = "tf_transaction_option_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class TransactionOptionType extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5994534457658843088L;

    private static Logger log = LoggerFactory.getLogger(TransactionOptionType.class);
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    // @Length(max = 128 )
    @NotNull
    protected String keyword;
    @Column(name = "name", length = 128)
    // @Length(max = 128 )
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    protected String description;
    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_transaction_option_type_sequence")
    @XmlTransient
    private Integer id;
    @OneToMany(mappedBy = "transactionOptionType", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private Set<ProfileLink> profileLinks;

    // Constructors
    public TransactionOptionType() {
    }

    public TransactionOptionType(Integer id, String keyword) {
        this.id = id;
        this.keyword = keyword;
    }

    public TransactionOptionType(Integer id, String keyword, String name, String description) {
        this.id = id;
        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    public static List<TransactionOptionType> GetAllTransactionOptionType() {
        return AuditedObject.listAllObjectsOrderByKeyword(TransactionOptionType.class);
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<ProfileLink> getProfileLinks() {
        return HibernateHelper.getLazyValue(this, "profileLinks", this.profileLinks);
    }

    public void setProfileLinks(Set<ProfileLink> profileLinks) {
        this.profileLinks = profileLinks;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        TransactionOptionTypeQuery query = new TransactionOptionTypeQuery();
        query.keyword().eq(this.keyword);
        List<TransactionOptionType> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
