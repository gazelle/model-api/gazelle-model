package net.ihe.gazelle.tf.model.export;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="hl7MessageProfiles")
@XmlAccessorType(XmlAccessType.FIELD)
public class Hl7MessageProfileIE {

    @XmlElementRefs({@XmlElementRef(name = "HL7MessageProfile")})
    private List<Hl7MessageProfile> messageProfiles;

    @XmlTransient
    private List<Hl7MessageProfile> duplicatedProfiles;

    @XmlTransient
    private List<Hl7MessageProfile> ignoredProfiles;

    @XmlTransient
    private List<String> unknownDomains;

    @XmlTransient
    private List<String> unknownActors;

    @XmlTransient
    private List<String> unknownTransactions;

    @XmlTransient
    private boolean imported;

    public Hl7MessageProfileIE(){
    }

    public Hl7MessageProfileIE(List<Hl7MessageProfile> messageProfiles){
        this.messageProfiles = messageProfiles;
    }


    public List<Hl7MessageProfile> getMessageProfiles() {
        return messageProfiles;
    }

    public void setMessageProfiles(List<Hl7MessageProfile> messageProfiles) {
        this.messageProfiles = messageProfiles;
    }

    public List<Hl7MessageProfile> getDuplicatedProfiles() {
        return duplicatedProfiles;
    }

    public void setDuplicatedProfiles(List<Hl7MessageProfile> duplicatedProfiles) {
        this.duplicatedProfiles = duplicatedProfiles;
    }

    public void addDuplicatedProfile(Hl7MessageProfile messageProfile){
        if (this.duplicatedProfiles == null){
            this.duplicatedProfiles = new ArrayList<>();
        }
        this.duplicatedProfiles.add(messageProfile);
    }

    public List<Hl7MessageProfile> getIgnoredProfiles() {
        return ignoredProfiles;
    }

    public void setIgnoredProfiles(List<Hl7MessageProfile> ignoredProfiles) {
        this.ignoredProfiles = ignoredProfiles;
    }

    public void addIgnoredProfile(Hl7MessageProfile messageProfile){
        if (this.ignoredProfiles == null){
            this.ignoredProfiles = new ArrayList<>();
        }
        this.ignoredProfiles.add(messageProfile);
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }

    public List<String> getUnknownDomains() {
        return unknownDomains;
    }

    public void setUnknownDomains(List<String> unknownDomains) {
        this.unknownDomains = unknownDomains;
    }

    public void addUnknownDomain(String domain) {
        if (this.unknownDomains == null){
            this.unknownDomains = new ArrayList<>();
        }
        this.unknownDomains.add(domain);
    }

    public List<String> getUnknownActors() {
        return unknownActors;
    }

    public void setUnknownActors(List<String> unknownActors) {
        this.unknownActors = unknownActors;
    }

    public void addUnknownActor(String actor) {
        if (this.unknownActors == null){
            this.unknownActors = new ArrayList<>();
        }
        this.unknownActors.add(actor);
    }

    public List<String> getUnknownTransactions() {
        return unknownTransactions;
    }

    public void setUnknownTransactions(List<String> unknownTransactions) {
        this.unknownTransactions = unknownTransactions;
    }

    public void addUnknownTransaction(String transaction) {
        if (this.unknownTransactions == null){
            this.unknownTransactions = new ArrayList<>();
        }
        this.unknownTransactions.add(transaction);
    }

}
