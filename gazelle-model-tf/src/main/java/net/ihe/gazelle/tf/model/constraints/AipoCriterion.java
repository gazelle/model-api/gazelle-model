package net.ihe.gazelle.tf.model.constraints;

import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aipoCriterion", propOrder = {})
@XmlRootElement(name = "aipoCriterion")

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "tf_rule_criterion", schema = "public")
@SequenceGenerator(name = "tf_rule_criterion_sequence", sequenceName = "tf_rule_criterion_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public abstract class AipoCriterion implements Comparable<AipoCriterion> {

    private static int generalFakeId = 0;

    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_rule_criterion_sequence")
    private Integer id;

    private transient int fakeId;

    public AipoCriterion() {
        super();
        synchronized (AipoCriterion.class) {
            this.fakeId = generalFakeId++;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFakeId() {
        return fakeId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + fakeId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AipoCriterion other = (AipoCriterion) obj;
        if (fakeId != other.fakeId) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(AipoCriterion o) {
        if (o == null) return 1;
        return this.toString().compareTo(o.toString());
    }

    @Deprecated
    public abstract AipoCriterion simplify();

    public abstract void appendSimple(StringBuilder sb);

    public abstract AipoCriterion matches(List<ActorIntegrationProfileOption> aipos);

}
