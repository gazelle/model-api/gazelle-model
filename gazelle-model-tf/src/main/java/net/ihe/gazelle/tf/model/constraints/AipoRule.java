package net.ihe.gazelle.tf.model.constraints;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "cause",
        "consequence",
        "comment"
})
@XmlRootElement(name = "aipoRule")

@Entity
@Table(name = "tf_rule", schema = "public")
@SequenceGenerator(name = "tf_rule_sequence", sequenceName = "tf_rule_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AipoRule implements Comparable<AipoRule> {

    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_rule_sequence")
    private Integer id;

    @XmlElement(required = true)
    @OneToOne(cascade = {CascadeType.ALL})
    private AipoCriterion cause;

    @XmlElement(required = true)
    @OneToOne(cascade = {CascadeType.ALL})
    private AipoCriterion consequence;

    @XmlTransient
    @OneToMany(mappedBy = "aipoRule", cascade = {CascadeType.ALL})
    private List<AipoSingle> aipoRules;

    @XmlElement(required = true)
    @Column(name = "comment")
    @Size(max = 1024)
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AipoCriterion getCause() {
        return cause;
    }

    public void setCause(AipoCriterion cause) {
        this.cause = cause;
    }

    public AipoCriterion getConsequence() {
        return consequence;
    }

    public void setConsequence(AipoCriterion consequence) {
        this.consequence = consequence;
    }

    public List<AipoSingle> getAipoRules() {
        return aipoRules;
    }

    public void setAipoRules(List<AipoSingle> aipoRules) {
        this.aipoRules = aipoRules;
    }

    public String getName() {
        return toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (cause != null) {
            cause.appendSimple(sb);
        } else {
            sb.append("NOT DEFINED");
        }
        sb.append(" requires ");
        if (consequence != null) {
            consequence.appendSimple(sb);
        } else {
            sb.append("NOT DEFINED");
        }
        return sb.toString();
    }

    public boolean validate(List<ActorIntegrationProfileOption> aipos) {
        if (getCause().matches(aipos) != null) {
            if (getConsequence().matches(aipos) == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((cause == null) ? 0 : cause.hashCode());
        result = (prime * result) + ((consequence == null) ? 0 : consequence.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AipoRule other = (AipoRule) obj;
        if (cause == null) {
            if (other.cause != null) {
                return false;
            }
        } else if (!cause.equals(other.cause)) {
            return false;
        }
        if (consequence == null) {
            if (other.consequence != null) {
                return false;
            }
        } else if (!consequence.equals(other.consequence)) {
            return false;
        }
        if (comment == null) {
            if (other.comment != null) {
                return false;
            }
        } else if (!comment.equals(other.comment)) {
            return false;
        }
        return true;
    }

    @PrePersist
    @PreUpdate
    public void setRuleOnCriterions() {
        setRuleOnCriterions(cause);
        setRuleOnCriterions(consequence);
    }

    private void setRuleOnCriterions(AipoCriterion aipoCriterion) {
        if (aipoCriterion != null) {
            if (aipoCriterion instanceof AipoSingle) {
                ((AipoSingle) aipoCriterion).setAipoRule(this);
            }
            if (aipoCriterion instanceof AipoList) {
                List<AipoCriterion> aipoCriterions = ((AipoList) aipoCriterion).getAipoCriterions();
                if (aipoCriterions != null) {
                    for (AipoCriterion subCriterion : aipoCriterions) {
                        setRuleOnCriterions(subCriterion);
                    }
                }
            }
        }

    }

    @Override
    public int compareTo(AipoRule o) {
        if (o == null) {
            return 1;
        }
        return this.toString().compareTo(o.toString());
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
