/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>TransactioneStatusType<br>
 * <br>
 * This class describes the Transaction Status type object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * TransactionStatusType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Transaction Status type</li>
 * <li><b>keyword</b> : keyword for the Transaction Status type
 * <li><b>name</b> : name corresponding to the Transaction Status type</li>
 * <li><b>description</b> : description corresponding to the Transaction Status type</li>
 * </ul>
 * </br> <b>Example of Transaction Status type</b> : " 'Deprecated' " is an IHE Transaction Status type<br>
 *
 * @author Ralph Moulton / MIR St Louis IHE development Project
 * @version 1.0 - 2009, June 4
 * @class TransactionStatusType.java
 * @package net.ihe.gazelle.tf.model
 * @see > moultonr@mir.wustl.edu - http://www.ihe-europe.org
 */
@XmlRootElement(name = "TransactionStatusType")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_transaction_status_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "tf_transaction_status_type_sequence", sequenceName = "tf_transaction_status_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class TransactionStatusType extends AuditedObject implements java.io.Serializable {

    private static final long serialVersionUID = 265837323827643461L;

    private static Logger log = LoggerFactory.getLogger(TransactionStatusType.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_transaction_status_type_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "keyword", unique = true, nullable = false)
    @Size(max = 32)
    @NotNull
    private String keyword;

    @Column(name = "name")
    @Size(max = 64)
    @NotNull
    private String name;

    @Column(name = "description")
    @Size(max = 255)
    private String description;

    @OneToMany(mappedBy = "transactionStatusType", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private Set<Transaction> transactions;

    // Constructors
    public TransactionStatusType() {
    }

    public TransactionStatusType(Integer id, String keyword) {
        this.id = id;
        this.keyword = keyword;
    }

    public TransactionStatusType(Integer id, String keyword, String name, String description) {
        this.id = id;
        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<TransactionStatusType> listAllTransactionStatusType() {
        return AuditedObject.listAllObjectsOrderByKeyword(TransactionStatusType.class);
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Transaction> getTransactions() {
        return HibernateHelper.getLazyValue(this, "transactions", this.transactions);
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;


        TransactionStatusTypeQuery query = new TransactionStatusTypeQuery();
        query.keyword().eq(this.keyword);
        query.name().eq(this.name);
        List<TransactionStatusType> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).getId();
            entityManager.merge(this);
        } else {
            if (id != null) {
                entityManager.merge(this);
            } else {
                entityManager.persist(this);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
