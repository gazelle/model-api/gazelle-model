package net.ihe.gazelle.objects.test;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestSteps;
import net.ihe.gazelle.tm.gazelletest.model.definition.factories.TestFactory;
import net.ihe.gazelle.tm.gazelletest.model.definition.factories.TestStepsFactory;
import org.junit.Before;
import org.junit.Test;

public class CreateTestCasesTest {

    @Before
    public void setUp() throws Exception {
        clean();
    }

    private void clean() {
        TestStepsFactory.cleanTestStep();
        TestFactory.cleanTest();
    }

    @Test
    public void test_createATest() {
        TestSteps testStep = TestStepsFactory.createTestStepWithMandatoryFields();
        TestSteps testStep2 = TestStepsFactory.createTestStepWithMandatoryFields();
        testStep2.setTestRolesInitiator(testStep.getTestRolesInitiator());
        DatabaseManager.writeObject(testStep2);

        TestStepsFactory.createTestStepWithMandatoryFields();
    }
}
