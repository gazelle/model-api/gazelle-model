package net.ihe.gazelle.IO;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;
import org.apache.commons.io.FileUtils;
import org.custommonkey.xmlunit.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Node;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
@Ignore
public class TestsImportTest extends AbstractTestQueryJunit4 {

    private static final int TABWIDTH = 2;
    private static final int NAMECOLUMN = 1;
    private static final int PATHCOLUMN = 0;

    private static final String OutputDirectory = "/tmp/gazelle-tm-export/";

    private String input;

    public TestsImportTest(String input, String fileName) {
        this.input = input;
    }

    @Parameters(name = "{index}: import {1}")
    public static Collection<Object[]> params() {
        File folder = new File(OutputDirectory);
        File[] listOfFiles = folder.listFiles();
        Object[][] data = new Object[listOfFiles.length][TABWIDTH];
        int index = 0;
        for (File file : listOfFiles) {
            data[index][PATHCOLUMN] = file.getAbsolutePath();
            data[index][NAMECOLUMN] = file.getName();
            index++;
        }

        return Arrays.asList(data);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.setFlushMode(FlushModeType.AUTO);
        entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.getTransaction().commit();
        super.tearDown();
    }

    @Test
    public void importTests() throws Exception {
        import_test(input);
    }


    public void import_test(String filePath) throws Exception {

        int[] testId = importTestsFromFile(filePath);
        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> listDistinct = fetchImportedTests(testId);

        String exportedFilePath = exportSelectedTests(listDistinct);

        String original = FileUtils.readFileToString(new File(filePath));
        String afterExport = FileUtils.readFileToString(new File(exportedFilePath));

        XMLUnit.setIgnoreWhitespace(Boolean.TRUE);
        XMLUnit.setNormalizeWhitespace(Boolean.TRUE);
        XMLUnit.setCompareUnmatched(Boolean.FALSE);

        Diff structuralDiff = new Diff(original, afterExport);
        structuralDiff.overrideDifferenceListener(new IgnoreTextAndAttributeValuesDifferenceListener());
        assertTrue("test XML matches control skeleton XML " + structuralDiff, structuralDiff.similar());

        Diff contentDiffWithoutIds = new Diff(original, afterExport);
        contentDiffWithoutIds.overrideDifferenceListener(new IgnoreIDsDifferenceListener());
        assertTrue("pieces of XML are identical " + contentDiffWithoutIds, contentDiffWithoutIds.identical());
    }

    private List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> fetchImportedTests(int[] testId) {
        TestQuery query = new TestQuery();

        List<Integer> intList = new ArrayList<Integer>();
        for (int index = 0; index < testId.length; index++) {
            intList.add(testId[index]);
        }
        query.id().in(intList);
        query.id().order(true);
        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> listDistinct = query.getListDistinct();
        return listDistinct;
    }

    private String exportSelectedTests(List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> testsList)
            throws JAXBException, IOException, FileNotFoundException {
        TestsExporter testExporter = new TestsExporter();
        testExporter.exportTests(testsList);
        net.ihe.gazelle.tm.gazelletest.model.definition.Test test = testsList.get(0);
        String provideFilePath = provideFilePath(test.getKeyword(), test.getName());
        testExporter.write(new FileOutputStream(new File(provideFilePath)));
        return provideFilePath;
    }

    public String provideFilePath(String keyword, String name) {
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
        return OutputDirectory + "exported_" + keyword.replace('/', ' ').replace("#", "") + "_"
                + name.replace('/', ' ').replace("#", "") + "_" + formater.format(date.getTime()) + ".xml";
    }

    private int[] importTestsFromFile(String filePath) throws JAXBException {
        File file = new File(filePath);
        TestsImporter testImporter = new TestsImporter();
        return testImporter.importTests(file);
    }

    @Override
    protected String getDb() {
        return "gazelle-junit";
    }

    protected String getHbm2ddl() {
        return "create";
    }

    protected boolean getShowSql() {
        return false;
    }
}

class IgnoreIDsDifferenceListener implements DifferenceListener {
    @Override
    public void skippedComparison(Node arg0, Node arg1) {
    }

    @Override
    public int differenceFound(Difference diff) {
        if (diff.getId() == DifferenceConstants.TEXT_VALUE_ID) {
            if (diff.getTestNodeDetail().getXpathLocation().endsWith("/id[1]/text()[1]")) {
                return RETURN_IGNORE_DIFFERENCE_NODES_IDENTICAL;
            }
        }
        return RETURN_ACCEPT_DIFFERENCE;
    }
}
