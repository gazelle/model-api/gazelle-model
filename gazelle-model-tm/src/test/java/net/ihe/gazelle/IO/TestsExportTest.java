package net.ihe.gazelle.IO;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestStatus;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestsExportTest extends AbstractTestQueryJunit4 {
    private static final String OutputDirectory = "/tmp/gazelle-tm-export/";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.getTransaction().begin();

        File outputDir = new File(OutputDirectory);
        if (!outputDir.exists()) {
            outputDir.mkdir();
        }
    }

    @After
    public void tearDown() throws Exception {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.getTransaction().commit();
        super.tearDown();
    }

    private void exportSelectedTests(List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> testsList,
                                     String keyword, String name) throws JAXBException, IOException, FileNotFoundException {
        TestsExporter testExporter = new TestsExporter();
        testExporter.exportTests(testsList);
        testExporter.write(new FileOutputStream(new File(provideFilePath(keyword, name))));
    }

    @Test
    @Ignore
    public void exportConnectathon() throws Exception {
        TestQuery query = new TestQuery();
        query.testStatus().eq(TestStatus.getSTATUS_READY());
        query.testType().eq(TestType.getTYPE_CONNECTATHON());

        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> listDistinct = query.getList();
        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> oneTest = new ArrayList<net.ihe.gazelle.tm.gazelletest.model.definition.Test>();
        System.out.println("NbTestsToExport: " + listDistinct.size());
        for (net.ihe.gazelle.tm.gazelletest.model.definition.Test test : listDistinct) {
            oneTest.add(test);
            exportSelectedTests(oneTest, test.getKeyword(), test.getName());
            oneTest.clear();
        }
    }

    @Test
    @Ignore
    public void exportITB() throws Exception {
        TestQuery query = new TestQuery();
        query.testStatus().eq(TestStatus.getSTATUS_READY());
        query.testType().eq(TestType.getTYPE_INTEROPERABILITY());

        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> listDistinct = query.getList();
        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> oneTest = new ArrayList<net.ihe.gazelle.tm.gazelletest.model.definition.Test>();
        System.out.println("NbTestsToExport: " + listDistinct.size());
        for (net.ihe.gazelle.tm.gazelletest.model.definition.Test test : listDistinct) {
            oneTest.add(test);
            exportSelectedTests(oneTest, test.getKeyword(), test.getName());
            oneTest.clear();
        }
    }

    @Test
    @Ignore
    public void exportPreCatTests() throws Exception {
        TestQuery query = new TestQuery();
        query.testStatus().eq(TestStatus.getSTATUS_READY());
        query.testType().eq(TestType.getTYPE_MESA());

        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> listDistinct = query.getList();
        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> oneTest = new ArrayList<net.ihe.gazelle.tm.gazelletest.model.definition.Test>();
        System.out.println("NbTestsToExport: " + listDistinct.size());
        for (net.ihe.gazelle.tm.gazelletest.model.definition.Test test : listDistinct) {
            oneTest.add(test);
            exportSelectedTests(oneTest, test.getKeyword(), test.getName());
            oneTest.clear();
        }
    }

    public String provideFilePath(String keyword, String name) {
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
        return OutputDirectory + "exported_" + keyword.replace('/', ' ').replace("#", "") + "_"
                + formater.format(date.getTime()) + ".xml";
    }

    @Override
    protected String getDb() {
        return "gazelle-junit";
    }

    protected boolean getShowSql() {
        return false;
    }
}
