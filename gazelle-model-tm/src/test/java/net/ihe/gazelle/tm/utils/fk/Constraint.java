package net.ihe.gazelle.tm.utils.fk;

public class Constraint implements Comparable<Constraint> {

    String table;
    String id;
    String remote_id;
    String remote_table;

    public Constraint(String table, String id, String remote_id, String remote_table) {
        super();
        this.table = table;
        this.id = id;
        this.remote_id = remote_id;
        this.remote_table = remote_table;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Constraint [table=");
        builder.append(table);
        builder.append(", id=");
        builder.append(id);
        builder.append(", remote_id=");
        builder.append(remote_id);
        builder.append(", remote_table=");
        builder.append(remote_table);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((remote_id == null) ? 0 : remote_id.hashCode());
        result = prime * result + ((remote_table == null) ? 0 : remote_table.hashCode());
        result = prime * result + ((table == null) ? 0 : table.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Constraint other = (Constraint) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (remote_id == null) {
            if (other.remote_id != null)
                return false;
        } else if (!remote_id.equals(other.remote_id))
            return false;
        if (remote_table == null) {
            if (other.remote_table != null)
                return false;
        } else if (!remote_table.equals(other.remote_table))
            return false;
        if (table == null) {
            if (other.table != null)
                return false;
        } else if (!table.equals(other.table))
            return false;
        return true;
    }

    @Override
    public int compareTo(Constraint o) {
        return toString().compareTo(o.toString());
    }

}
