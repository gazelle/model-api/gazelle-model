package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Ignore
public class TestQueryFixForeignKeys {

    private static Logger logger = LoggerFactory.getLogger(TestQueryFixForeignKeys.class.getName());
    private ArrayList<String> queriesOK;

    public void testCleanFK() {
        try {
            queriesOK = new ArrayList<String>();
            EntityManager em = EntityManagerService.provideEntityManager();
            Object delegate = em.getDelegate();
            if (delegate instanceof Session) {
                Session session = (Session) delegate;
                session.doWork(new Work() {

                    @Override
                    public void execute(Connection connection) throws SQLException {
                        try {
                            cleanFK(connection);
                        } catch (IOException e) {
                            throw new SQLException(e);
                        }
                    }
                });
            }

            System.out.println("Applied queries : ");
            for (String queryOK : queriesOK) {
                System.out.println(queryOK + ";");
            }
        } catch (Exception ex) {
            throw new RuntimeException("Exception during testPersistence", ex);
        }
    }

    private void cleanFK(Connection connection) throws IOException {
        List<Constraint> constraints = new ArrayList<TestQueryFixForeignKeys.Constraint>();

        BufferedReader br = new BufferedReader(new FileReader("/tmp/server2.log"));
        for (String line = br.readLine(); line != null; line = br.readLine()) {
            if (line.contains("Unsuccessful: alter table ") && line.contains("add constraint")) {
                line = line.substring(line.indexOf("alter table"));
                String[] split = StringUtils.split(line, ' ');

                String table = split[2].replace("public.", "");
                String id = split[5];
                String remote_id = split[8].replace("(", "").replace(")", "");
                String remote_table = split[10].replace("public.", "");

                if (table != null && id != null && remote_id != null && remote_table != null) {
                    constraints.add(new Constraint(table, id, remote_id, remote_table));
                }
            }
        }
        br.close();
        // patchRecursively("", "update tm_test_aud SET keyword='10924_DEPRECATED' where id = 10924", connection);
        // patchRecursively("",
        // "update tm_test_aud SET keyword='ECHO_IM_SOP_Class_Support_DEPRECATED' where id = 323");
        // patchRecursively("",
        // "update tm_test_aud SET keyword='14352_DEPRECATED' where id = 10924");

        createConstraints(constraints, connection);
    }

    private void createConstraints(List<Constraint> constraints, Connection connection) {
        for (Constraint constraint : constraints) {

            String query = "alter table " + constraint.table + " add constraint " + constraint.id + " foreign key ("
                    + constraint.remote_id + ") references " + constraint.remote_table;

            patchRecursively("", query, connection);
        }
    }

    private boolean patchRecursively(String padding, String query, Connection connection) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try {
                System.out.println(" " + query);
                // Try to execute the update
                statement.executeUpdate(query);
            } catch (SQLException e) {
                System.out.println(" " + e.getMessage());
                // If failure, trying to restore data from _aud
                if (processMissing(padding, e.getMessage(), connection)) {
                    // If done, retry the first query
                    return patchRecursively(" " + padding, query, connection);
                } else {
                    return false;
                }
            }
            queriesOK.add(query);
            System.out.println(" OK");
            statement.close();
            return true;
        } catch (SQLException e) {
            logger.error("", e);
            return false;
        }
    }

    private boolean processMissing(String padding, String message, Connection connection) throws SQLException {
        // Détail : La clé (test_id)=(217) n'est pas présente dans la table
        // tm_test .

        // Building the query to reinsert the data
        String finalQuery = "";
        try {
            int start = message.indexOf('(');
            message = message.substring(start + 1);
            start = message.indexOf('(');
            message = message.substring(start + 1);
            String id = message.substring(0, message.indexOf(')'));
            start = message.indexOf('\u00AB');
            message = message.substring(start + 2);
            String table = message.substring(0, message.indexOf('\u00BB') - 1);

            Statement statement = null;
            statement = connection.createStatement();
            String query = "SELECT * FROM " + table + "_aud where id = " + id + " order by rev desc";
            System.out.println(padding + " - " + query);
            ResultSet result = statement.executeQuery(query);

            result.next();
            int rev = result.getInt("rev");
            result.next();
            rev = result.getInt("rev");
            statement.close();

            List<String> columns = new ArrayList<String>();

            statement = connection.createStatement();
            result = statement.executeQuery("select column_name from information_schema.columns where table_name = '"
                    + table + "'");
            while (result.next()) {
                columns.add(result.getString(1));
            }
            statement.close();

            finalQuery = "INSERT INTO " + table + " (";

            String columnsString = "";
            for (int i = 0; i < columns.size(); i++) {
                columnsString = columnsString + columns.get(i);
                if (i != columns.size() - 1) {
                    columnsString = columnsString + ", ";
                }
            }
            finalQuery = finalQuery + columnsString + ") select " + columnsString + " from " + table
                    + "_aud where id = " + id + " and rev = " + rev + ";";
        } catch (Throwable e) {
            logger.error(padding + "Failed to extract informations...", e);
            return false;
        }

        // trying to apply the update
        return patchRecursively(" " + padding, finalQuery, connection);
    }

    private class Constraint {
        String table;
        String id;
        String remote_id;
        String remote_table;

        public Constraint(String table, String id, String remote_id, String remote_table) {
            super();
            this.table = table;
            this.id = id;
            this.remote_id = remote_id;
            this.remote_table = remote_table;
        }
    }
}
