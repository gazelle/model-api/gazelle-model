package net.ihe.gazelle.tm.utils;

import junit.framework.TestCase;
import org.hibernate.cfg.Configuration;
import org.hibernate.ejb.Ejb3Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.junit.Ignore;

@Ignore
public class TestQueryDDL extends TestCase {

    public void testQueryDDL() {
        Ejb3Configuration cfg = new Ejb3Configuration();
        Ejb3Configuration configured = cfg.configure("gazellePU-tm", null);

        Configuration hibernateConfiguration = configured.getHibernateConfiguration();

        SchemaExport schemaExport = new SchemaExport(hibernateConfiguration);
        schemaExport.setDelimiter(";");
        schemaExport.setOutputFile("/tmp/ddl.sql");
        schemaExport.execute(false, false, false, true);

        SchemaUpdate schemaUpdate = new SchemaUpdate(hibernateConfiguration);
        schemaUpdate.setDelimiter(";");
        schemaUpdate.setOutputFile("/tmp/ddl_update.sql");
        schemaUpdate.execute(false, false);
    }

}
