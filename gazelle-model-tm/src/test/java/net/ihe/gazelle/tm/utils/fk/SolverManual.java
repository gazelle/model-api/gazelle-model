package net.ihe.gazelle.tm.utils.fk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SolverManual extends Solver {

    public SolverManual(List<String> queriesOK) {
        super(queriesOK);
    }

    @Override
    public boolean processMissing(String padding, String message, String query, Constraint constraint,
                                  Connection connection) throws SQLException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("May be : update tm_test_aud set keyword='14352_deprecated' where id = 10924;");
        System.out.print("Enter your query : ");
        String customQuery = null;
        try {
            customQuery = br.readLine();
        } catch (IOException e) {
            throw new SQLException();
        }

        Statement statement = null;
        statement = connection.createStatement();
        int update = statement.executeUpdate(customQuery);

        System.out.println(update + " lines updated");
        queriesOK.add(customQuery);

        return false;
    }

    @Override
    public boolean isSolved() {
        return false;
    }

    @Override
    public String getLabel() {
        return "Manual query";
    }

}
