package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestStatus;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestStatusQuery;

import java.util.List;

public class TestStatusFactory {

    static String[][] allStatus = {{TestStatus.STATUS_READY_STRING, "Ready", "ready test"},
            {TestStatus.STATUS_TOBECOMPLETED_STRING, "Completed", "Completed test"},
            {TestStatus.STATUS_DEPRECATED_STRING, "Deprecated", "Deprecated test"},
            {TestStatus.STATUS_STORAGESUBSTITUTE_STRING, "storage/substitute", "storage/substitute test"}};

    public static void createTestDefaultTestStatus() {
        for (String[] statusTab : allStatus) {
            TestStatus status = new TestStatus(statusTab[0], statusTab[1], statusTab[2]);
            DatabaseManager.writeObject(status);
        }
    }

    public static void cleanTestStatus() {
        TestStatusQuery statusQuery = new TestStatusQuery();
        List<TestStatus> status = statusQuery.getListDistinct();
        for (TestStatus item : status) {
            DatabaseManager.removeObject(item);
        }
    }
}
