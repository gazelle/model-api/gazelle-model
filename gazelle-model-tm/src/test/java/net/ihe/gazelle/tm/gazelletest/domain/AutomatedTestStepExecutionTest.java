package net.ihe.gazelle.tm.gazelletest.domain;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class AutomatedTestStepExecutionTest {

    @Test
    public void test_automated_test_step_execution() {
        Date date = new Date();
        AutomatedTestStepExecution execution1 = new AutomatedTestStepExecution();
        execution1.setId(1);
        execution1.setTestStepsInstanceId(1);
        execution1.setUserId("userId");
        execution1.setStatus(AutomatedTestStepExecutionStatus.DONE_PASSED);
        execution1.setDate(date);
        execution1.setJsonReport("value");

        AutomatedTestStepExecution execution2 = new AutomatedTestStepExecution(1, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, date, "value");
        execution2.setId(1);
        assertEquals(execution1, execution2);
    }
}
