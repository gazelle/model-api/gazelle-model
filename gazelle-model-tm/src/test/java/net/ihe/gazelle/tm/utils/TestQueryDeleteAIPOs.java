package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.systems.model.SystemActorProfilesQuery;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

@Ignore
public class TestQueryDeleteAIPOs extends AbstractTestQueryDeleteElements {

    public void testRemoveAIPOs() {
        final Set<Integer> idsToDelete = new HashSet<Integer>();

        idsToDelete.add(332);

        SystemActorProfilesQuery query = new SystemActorProfilesQuery();
        query.actorIntegrationProfileOption().id().eq(332);
        final Set<Integer> ids = new HashSet<Integer>(query.id().getListDistinct());

        EntityManager em = EntityManagerService.provideEntityManager();
        Object delegate = em.getDelegate();
        if (delegate instanceof Session) {
            Session session = (Session) delegate;
            session.doWork(new Work() {

                @Override
                public void execute(Connection connection) throws SQLException {

                    deleteElements(connection, ids, "tm_system_actor_profiles", "id");
                }
            });
        }
    }
}
