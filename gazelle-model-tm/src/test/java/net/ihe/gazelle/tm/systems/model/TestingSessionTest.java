package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.cache.SSOGumUserClientCache;
import net.ihe.gazelle.tm.gazelletest.model.factories.TestingSessionFactory;
import net.ihe.gazelle.users.model.factories.InstitutionFactory;
import net.jcip.annotations.NotThreadSafe;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;

@NotThreadSafe
public class TestingSessionTest {

    UserService spyUserClient;

    @Test
    public void TestSessionAdmin_must_have_TestSessionAdminRole() {
        TestingSession testingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        User user = TestingSessionFactory.getUser();
        Set<String> groups = new HashSet<>();
        groups.add(Group.GRP_TESTING_SESSION_MANAGER);
        user.setGroupIds(groups);
        List<TestingSessionAdmin> sessionAdmins = new ArrayList<>();
        String userId = TestingSessionFactory.getUser().getId();
        sessionAdmins.add(new TestingSessionAdmin(userId));
        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(userId);
        testingSession.setTestingSessionAdmins(sessionAdmins);
        assertNotNull(testingSession.getTestingSessionAdmins());
        assertEquals(1, testingSession.getTestingSessionAdmins().size());
        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }
    }

    @Test
    public void setTestingSessionAdmin_TestSessionAdmin_must_have_TestSessionAdminRole() {
        TestingSession testingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        User user = TestingSessionFactory.getUser();
        Set<String> groups = new HashSet<>();
        groups.add(Group.GRP_TESTING_SESSION_MANAGER);
        user.setGroupIds(groups);
        List<TestingSessionAdmin> sessionAdmins = new ArrayList<>();
        String userId = TestingSessionFactory.getUser().getId();
        sessionAdmins.add(new TestingSessionAdmin(userId));
        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(userId);
        testingSession.setTestingSessionAdmins(sessionAdmins);
        assertNotNull(testingSession.getTestingSessionAdmins());
        assertEquals(1, testingSession.getTestingSessionAdmins().size());
        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }
    }

    @Test
    public void TestSessionAdmin_can_have_haveOtherRoles() {
        TestingSession testingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        User user = TestingSessionFactory.getUser();
        Set<String> groups = new HashSet<>();
        groups.add(Group.GRP_TESTING_SESSION_MANAGER);
        groups.add(Group.GRP_GAZELLE_ADMIN);
        user.setGroupIds(groups);
        List<TestingSessionAdmin> sessionAdmins = new ArrayList<>();
        String userId = TestingSessionFactory.getUser().getId();
        sessionAdmins.add(new TestingSessionAdmin(userId));
        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(userId);
        testingSession.setTestingSessionAdmins(sessionAdmins);
        assertNotNull(testingSession.getTestingSessionAdmins());
        assertEquals(1, testingSession.getTestingSessionAdmins().size());
        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }
    }

    @Test
    public void setTestingSessionAdmin_can_change_admins() {
        // Create a user and add it as a testing session admin
        TestingSession testingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        User user = TestingSessionFactory.getUser();
        Set<String> groups = new HashSet<>();
        groups.add(Group.GRP_TESTING_SESSION_MANAGER);
        user.setGroupIds(groups);
        List<TestingSessionAdmin> sessionAdmins = new ArrayList<>();
        String userId = TestingSessionFactory.getUser().getId();
        sessionAdmins.add(new TestingSessionAdmin(userId));
        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(userId);
        testingSession.setTestingSessionAdmins(sessionAdmins);
        assertNotNull(testingSession.getTestingSessionAdmins());
        assertEquals(1, testingSession.getTestingSessionAdmins().size());
        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }

        // Change the testing session admin
        User newUser = provideNewUserWithMandatoryFields();
        newUser.setGroupIds(groups);
        List<TestingSessionAdmin> sessionAdmins1 = new ArrayList<>();
        sessionAdmins1.add(new TestingSessionAdmin(userId));
        spyUserClient = Mockito.spy(userClient);
        testingSession.setUserClient(spyUserClient);
        doReturn(user).when(spyUserClient).getUserById(user.getId());

        sessionAdmins1.add(new TestingSessionAdmin(userId));
        doReturn(newUser).when(spyUserClient).getUserById(newUser.getId());
        testingSession.setTestingSessionAdmins(sessionAdmins1);

        assertNotNull(testingSession.getTestingSessionAdmins());
        assertEquals(2, testingSession.getTestingSessionAdmins().size());
        java.lang.System.out.println("testingSession.getTestingSessionAdminId().get(0) => " + testingSession.getTestingSessionAdmins().get(0));
        java.lang.System.out.println("newUser.getId() => " + newUser.getId());
        java.lang.System.out.println("user.getId() => " + user.getId());

        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }
        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins1) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }
    }

    @Test
    public void can_get_testing_sessions_from_user() {
        TestingSession testingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        User user = TestingSessionFactory.getUser();
        user.setGroupIds(Collections.singleton(Group.GRP_TESTING_SESSION_MANAGER));
        List<TestingSessionAdmin> sessionAdmins = new ArrayList<>();
        String userId = TestingSessionFactory.getUser().getId();
        sessionAdmins.add(new TestingSessionAdmin(userId));
        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(userId);
        testingSession.setTestingSessionAdmins(sessionAdmins);
        DatabaseManager.writeObject(testingSession);
        assertNotNull(testingSession.getTestingSessionAdmins());
        assertEquals(1, testingSession.getTestingSessionAdmins().size());

        for (TestingSessionAdmin testingSessionAdmin : sessionAdmins) {
            assertEquals(testingSessionAdmin.getUserId(), userId);
        }
    }

    @Test
    public void get_default_testing_session() {
        // Create a user and add it as a testing session admin
        TestingSession testingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        TestingSession defaultTestingSession = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France2",  provideNewUserWithMandatoryFields().getId());
        defaultTestingSession.setIsDefaultTestingSession(true);

        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(User.getId());
        TestingSession result = TestingSession.getDefault();
        assertEquals(defaultTestingSession.getName(), result.getName());
        assertNotEquals(testingSession.getName(), result.getName());
    }

    @Test
    public void get_default_testing_session_when_none_is_marked_as_default() {
        // Create a user and add it as a testing session admin
        TestingSession testingSession1 = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", provideNewUserWithMandatoryFields().getId());
        TestingSession testingSessionLatest = TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France2", provideNewUserWithMandatoryFields().getId());
        UserService userClient = new SSOGumUserClientCache(null);
        spyUserClient = Mockito.spy(userClient);
        User User = TestingSessionFactory.getUser();
        testingSession1.setUserClient(spyUserClient);
        doReturn(User).when(spyUserClient).getUserById(User.getId());
        TestingSession result = TestingSession.getDefault();
        assertEquals(testingSessionLatest.getId(), result.getId());
        assertNotEquals(testingSession1.getId(), result.getId());
    }

    private static User provideNewUserWithMandatoryFields() {
        User user = new User();
        user.setId("hardTesting");
        user.setFirstName("Hard");
        user.setLastName("Die");
        user.setActivated(true);
        user.setLastLoginTimestamp(new Timestamp(1601906470000L));
        user.setOrganizationId(InstitutionFactory.createInstitutionWithMandatoryFields().getName());
        user.setEmail("Die.Hard@kereval.com");
        return user;
    }
}
