package net.ihe.gazelle.tm.gazelletest.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AutomatedTestStepReportFileTest {

    @Test
    public void test_automated_test_step_report_file() {
        int id = 0;
        String reportName = "reportName";
        byte[] report = "pdf".getBytes();
        AutomatedTestStepReportFile reportFile1 = new AutomatedTestStepReportFile();
        reportFile1.setId(id);
        reportFile1.setReportName(reportName);
        reportFile1.setReport(report);

        AutomatedTestStepReportFile reportFile2 = new AutomatedTestStepReportFile(reportName, report);
        reportFile2.setId(id);
        assertEquals(reportFile1, reportFile2);
    }
}
