package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;

import java.util.List;

public class TestFactory {

    private static int number = 0;

    public static Test createPreConnectathonTest() {
        Test test = new Test();
        test.setTestType(TestTypeFactory.createTestType_TYPE_MESA());
        test = (Test) DatabaseManager.writeObject(test);
        return test;
    }

    public static Test createConnectathonTest() {
        Test test = provideConnectathonTest();
        test = (Test) DatabaseManager.writeObject(test);
        return test;
    }

    public static Test provideConnectathonTest() {
        Test test = new Test();
        test.setTestType(TestTypeFactory.createTestType_TYPE_CONNECTATHON());
        test.setKeyword("keyword" + number);
        number++;
        test.setName("name");
        test.setShortDescription("super short");
        return test;
    }

    public static void cleanTest() {
        TestQuery testQuery = new TestQuery();
        List<Test> test = testQuery.getListDistinct();
        for (Test item : test) {
            DatabaseManager.removeObject(item);
        }
        TestTypeFactory.cleanTestType();
    }
}
