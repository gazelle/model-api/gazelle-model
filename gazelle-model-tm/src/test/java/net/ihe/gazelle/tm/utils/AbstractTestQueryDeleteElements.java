package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.junit.Before;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;
@Ignore
public class AbstractTestQueryDeleteElements {

    private static Logger log = LoggerFactory.getLogger(AbstractTestQueryDeleteElements.class);

    private Map<String, Boolean> isAuditedMap;

    @Before
    public void setUp() throws Exception {
        isAuditedMap = new HashMap<String, Boolean>();
    }

    protected boolean isAudited(Connection connection, String tableName) throws SQLException {
        Boolean isAudited = isAuditedMap.get(tableName);
        if (isAudited == null) {
            ResultSet tables = connection.getMetaData().getTables(connection.getCatalog(), null, tableName + "_aud",
                    null);
            isAudited = tables.next();
            isAuditedMap.put(tableName, isAudited);
        }
        return isAudited;
    }

    private void executeQueries(Connection connection, List<String> queryCollector) {
        int errors = 0;
        for (String query : queryCollector) {
            try {
                executeUpdate(connection, query);
            } catch (SQLException e) {
                System.out.println("Failed");
                errors++;
            }
        }
        System.out.println("Got " + errors + " errors.");
        if (errors > 0) {
            System.out.println("Retrying");
            executeQueries(connection, queryCollector);
        }
    }

    private void executeUpdate(Connection connection, String query) throws SQLException {
        Statement statement = null;
        statement = connection.createStatement();
        System.out.println(query);
        // Try to execute the query
        statement.executeUpdate(query);
        statement.close();
    }

    protected void deleteElements(Connection connection, Set<Integer> idsToDelete, String tableName, String primaryKey) {
        for (Integer id : idsToDelete) {
            try {
                List<String> queryCollector = new ArrayList<String>();
                deleteCascade(connection, tableName, primaryKey, Integer.toString(id), queryCollector);
                executeQueries(connection, queryCollector);
            } catch (SQLException e) {
                log.error("Failed to delete " + id, e);
                return;
            }
        }
    }

    private void deleteCascade(Connection connection, String table, String primaryKey, String id,
                               List<String> queryCollector) throws SQLException {
        // table, list columns linking to table primarykey
        Map<String, List<String>> references = new HashMap<String, List<String>>();
        // table, single primary key
        Map<String, String> primaryKeys = new HashMap<String, String>();
        // table, ids to delete
        Map<String, List<String>> values = new HashMap<String, List<String>>();

        ResultSet exportedKeys = connection.getMetaData().getExportedKeys(connection.getCatalog(), "", table);

        while (exportedKeys.next()) {
            String table2 = exportedKeys.getString(7);
            String key2 = exportedKeys.getString(8);

            // referencing the column as remote reference
            List<String> columns = references.get(table2);
            if (columns == null) {
                columns = new ArrayList<String>();
                references.put(table2, columns);
            }
            columns.add(key2);

            // Shall we delete cascade?
            // Look for primary keys
            String primaryKey2 = primaryKeys.get(table2);
            if (primaryKey2 == null) {

                ResultSet primaryKeysRS = connection.getMetaData().getPrimaryKeys(connection.getCatalog(), "", table2);
                while (primaryKeysRS.next()) {
                    if (primaryKey2 == null) {
                        primaryKey2 = primaryKeysRS.getString(4);
                    } else {
                        primaryKey2 = "";
                    }
                }

                if (primaryKey2 != null && !"".equals(primaryKey2)) {
                    primaryKeys.put(table2, primaryKey2);

                    Statement statement = connection.createStatement();
                    String sql = "SELECT DISTINCT " + primaryKey2 + " from " + table2 + " WHERE " + key2 + "=" + id;
                    System.out.println("-- " + sql);
                    ResultSet remoteIds = statement.executeQuery(sql);
                    List<String> ids = new ArrayList<String>();
                    while (remoteIds.next()) {
                        String remoteId = remoteIds.getObject(1).toString();
                        ids.add(remoteId);
                    }
                    values.put(table2, ids);
                } else {
                    primaryKeys.put(table2, "");
                }
            }
        }

        Set<Entry<String, List<String>>> entrySet = values.entrySet();
        for (Entry<String, List<String>> entry : entrySet) {
            String table2 = entry.getKey();
            List<String> ids = entry.getValue();
            String primaryKey2 = primaryKeys.get(table2);
            if (!primaryKey2.equals("")) {
                for (String remoteId : ids) {
                    deleteCascade(connection, table2, primaryKey2, remoteId, queryCollector);
                }
            }
        }

        entrySet = references.entrySet();
        for (Entry<String, List<String>> entry : entrySet) {
            List<String> columns = entry.getValue();
            for (String column : columns) {
                String table2 = entry.getKey();
                queryCollector.add("DELETE FROM " + table2 + " WHERE " + column + " = " + id + ";");
                if (isAudited(connection, table2)) {
                    queryCollector.add("DELETE FROM " + table2 + "_aud WHERE " + column + " = " + id + ";");
                }
            }
        }

        queryCollector.add("DELETE FROM " + table + " WHERE " + primaryKey + " = " + id + ";");
        if (isAudited(connection, table)) {
            queryCollector.add("DELETE FROM " + table + "_aud WHERE " + primaryKey + " = " + id + ";");
        }
    }

}
