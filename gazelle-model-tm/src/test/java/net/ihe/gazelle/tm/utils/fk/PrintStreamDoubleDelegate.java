package net.ihe.gazelle.tm.utils.fk;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Locale;

public class PrintStreamDoubleDelegate extends PrintStream {

    private PrintStream delegate;
    private PrintStream delegate2;
    public PrintStreamDoubleDelegate(OutputStream os, PrintStream delegate2) {
        super(os);
        delegate = new PrintStream(os);
        this.delegate2 = delegate2;
    }

    public int hashCode() {
        return delegate.hashCode();
    }

    public void write(byte[] b) throws IOException {
        delegate.write(b);
        delegate2.write(b);
    }

    public boolean equals(Object obj) {
        return delegate.equals(obj);
    }

    public String toString() {
        return delegate.toString();
    }

    public void flush() {
        delegate.flush();
        delegate2.flush();
    }

    public void close() {
        delegate.close();
        delegate2.close();
    }

    public boolean checkError() {
        return delegate.checkError();
    }

    public void write(int b) {
        delegate.write(b);
        delegate2.write(b);
    }

    public void write(byte[] buf, int off, int len) {
        delegate.write(buf, off, len);
        delegate2.write(buf, off, len);
    }

    public void print(boolean b) {
        delegate.print(b);
        delegate2.print(b);
    }

    public void print(char c) {
        delegate.print(c);
        delegate2.print(c);
    }

    public void print(int i) {
        delegate.print(i);
        delegate2.print(i);
    }

    public void print(long l) {
        delegate.print(l);
        delegate2.print(l);
    }

    public void print(float f) {
        delegate.print(f);
        delegate2.print(f);
    }

    public void print(double d) {
        delegate.print(d);
        delegate2.print(d);
    }

    public void print(char[] s) {
        delegate.print(s);
        delegate2.print(s);
    }

    public void print(String s) {
        delegate.print(s);
        delegate2.print(s);
    }

    public void print(Object obj) {
        delegate.print(obj);
        delegate2.print(obj);
    }

    public void println() {
        delegate.println();
        delegate2.println();
    }

    public void println(boolean x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(char x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(int x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(long x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(float x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(double x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(char[] x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(String x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public void println(Object x) {
        delegate.println(x);
        delegate2.println(x);
    }

    public PrintStream printf(String format, Object... args) {
        return delegate.printf(format, args);
    }

    public PrintStream printf(Locale l, String format, Object... args) {
        return delegate.printf(l, format, args);
    }

    public PrintStream format(String format, Object... args) {
        return delegate.format(format, args);
    }

    public PrintStream format(Locale l, String format, Object... args) {
        return delegate.format(l, format, args);
    }

    public PrintStream append(CharSequence csq) {
        return delegate.append(csq);
    }

    public PrintStream append(CharSequence csq, int start, int end) {
        return delegate.append(csq, start, end);
    }

    public PrintStream append(char c) {
        return delegate.append(c);
    }

}
