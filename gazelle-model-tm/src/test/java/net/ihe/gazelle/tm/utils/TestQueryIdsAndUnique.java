package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.paths.HQLEntityInfo;
import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Ignore
public class TestQueryIdsAndUnique {

    public static Session getSession(EntityManager entityManager) {
        if (entityManager != null) {
            Object delegate = entityManager.getDelegate();
            if (delegate instanceof Session) {
                Session session = (Session) delegate;
                return session;
            }
        }
        return null;
    }

    public static SessionFactoryImplementor getFactory() {
        Session session = getSession(EntityManagerService.provideEntityManager());
        SessionFactory sessionFactory = session.getSessionFactory();
        SessionFactoryImplementor factory = null;
        if (sessionFactory instanceof SessionFactoryImplementor) {
            factory = (SessionFactoryImplementor) sessionFactory;
        }
        return factory;
    }

    public void testList() throws Exception {

        SessionFactoryImplementor factory = getFactory();
        Map<String, ClassMetadata> allClassMetadata = factory.getAllClassMetadata();
        List<ClassMetadata> classMetadatas = new ArrayList<ClassMetadata>(allClassMetadata.values());

        List<HQLSafePathEntity> entities = new ArrayList<HQLSafePathEntity>();
        for (ClassMetadata classMetadata : classMetadatas) {
            if (classMetadata instanceof AbstractEntityPersister) {
                AbstractEntityPersister aep = (AbstractEntityPersister) classMetadata;

                String entityName = aep.getEntityName();
                if (entityName.startsWith("net.") && !entityName.endsWith("_AUD")) {
                    Class<?> entityClass = Class.forName(entityName);
                    Exchanged dbSynchronized = entityClass.getAnnotation(Exchanged.class);
                    if (dbSynchronized != null) {
                        String entityClassName = aep.getEntityName() + "Query";
                        HQLSafePathEntity entity = (HQLSafePathEntity) Class.forName(entityClassName).newInstance();
                        entities.add(entity);
                    }
                }
            }
        }

        for (HQLSafePathEntity hqlSafePathEntity : entities) {
            System.out.println("* " + hqlSafePathEntity.getEntityClass().getCanonicalName());
            Set<HQLSafePath<?>> idAttributes = HQLEntityInfo.getIdAttributes(hqlSafePathEntity);
            // Set<HQLSafePath<?>> uniqueAttributes = hqlSafePathEntity.getUniqueAttributes();
            Set<HQLSafePath<?>> allAttributes = HQLEntityInfo.getAllAttributes(hqlSafePathEntity);
            for (HQLSafePath<?> path : allAttributes) {
                if (idAttributes.contains(path)) {
                    System.out.print(" I");
                } else {
                    System.out.print("  ");
                }
                // if (uniqueAttributes.contains(path)) {
                // System.out.print(" U ");
                // } else {
                // System.out.print("   ");
                // }
                System.out.println(path);
            }
        }

    }

}
