package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecutionStatus;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepReportFile;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AutomatedTestStepExecutionEntityTest {

    @Test
    public void test_automated_test_step_execution_entity() {
        Date date = new Date();
        AutomatedTestStepExecutionEntity entity1 = new AutomatedTestStepExecutionEntity();
        entity1.setTestStepsInstanceId(1);
        entity1.setUserId("userId");
        entity1.setStatus(AutomatedTestStepExecutionStatus.DONE_PASSED);
        entity1.setDate(date);
        entity1.setReport("value");

        AutomatedTestStepExecutionEntity entity2 = new AutomatedTestStepExecutionEntity(1, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, date, "value");
        assertEquals(entity1.getTestStepsInstanceId(), entity2.getTestStepsInstanceId());
        assertEquals(entity1.getUserId(), entity2.getUserId());
        assertEquals(entity1.getStatus(), entity2.getStatus());
        assertEquals(entity1.getDate(), entity2.getDate());
        assertEquals(entity1.getReport(), entity2.getReport());
    }

    @Test
    public void test_from_domain_constructor() {
        Date date = new Date();
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(1, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, date, "value");
        AutomatedTestStepExecutionEntity entity = new AutomatedTestStepExecutionEntity(execution);
        assertEquals(execution.getTestStepsInstanceId(), entity.getTestStepsInstanceId());
        assertEquals(execution.getUserId(), entity.getUserId());
        assertEquals(execution.getStatus(), entity.getStatus());
        assertEquals(execution.getDate(), entity.getDate());
        assertEquals(execution.getJsonReport(), entity.getReport());
    }

    @Test
    public void test_asDomain() {
        Date date = new Date();
        AutomatedTestStepExecutionEntity entity = new AutomatedTestStepExecutionEntity(1, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, date, "value");
        AutomatedTestStepReportFile report = new AutomatedTestStepReportFile("reportName", "pdf".getBytes());
        AutomatedTestStepReportFileEntity reportEntity = new AutomatedTestStepReportFileEntity(report);
        List<AutomatedTestStepReportFileEntity> files = new ArrayList<>();
        files.add(reportEntity);
        entity.setFiles(files);
        AutomatedTestStepExecution execution = entity.asDomain();
        assertEquals(entity.getTestStepsInstanceId(), execution.getTestStepsInstanceId());
        assertEquals(entity.getUserId(), execution.getUserId());
        assertEquals(entity.getStatus(), execution.getStatus());
        assertEquals(entity.getDate(), execution.getDate());
        assertEquals(entity.getReport(), execution.getJsonReport());
        assertEquals(entity.getFiles().size(), execution.getPdfFiles().size());

        AutomatedTestStepReportFile reportCopy = execution.getPdfFiles().get(0);
        AutomatedTestStepReportFileEntity reportCopyEntity = entity.getFiles().get(0);
        assertEquals(reportCopy.getReport(), reportCopyEntity.getReport());
        assertEquals(reportCopy.getReportName(), reportCopyEntity.getReportName());
    }
}
