package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.gazelletest.model.factories.SystemInSessionFactory;
import org.jboss.seam.security.Identity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PreferenceService.class})
@PowerMockIgnore("javax.management.*")
public class SystemInSessionTest {

    SystemInSessionRegistrationStatus[] expectedEmpty = {};
    SystemInSession systemInSession;
    List<SystemInSessionRegistrationStatus> possibleRegistrationStatus;
    SystemInSessionRegistrationStatus[] expectAllStatus = SystemInSessionRegistrationStatus.values();
    SystemInSessionRegistrationStatus[] userExpected = {SystemInSessionRegistrationStatus.IN_PROGRESS, SystemInSessionRegistrationStatus.DROPPED,
            SystemInSessionRegistrationStatus.COMPLETED};
    GazelleIdentity identity;

    @Before
    public void beforeTests() {
        Identity.setSecurityEnabled(false);
        identity = mock(GazelleIdentity.class);
        PowerMockito.mockStatic(PreferenceService.class);
        PowerMockito.mockStatic(GazelleIdentity.class);
        systemInSession = SystemInSessionFactory.createSystemInSessionWithMandatoryFields(identity.getUsername());
        systemInSession.setIdentity(identity);
        mockPreferenceService();
    }

    @Test
    public void user_can_change_registrationStatus_before_deadline() {
        systemInSession.getTestingSession().setAllowParticipantRegistration(true);
        systemInSession.getTestingSession().setRegistrationDeadlineDate(addHour(new Date(), 1));
        fakeAdminUser();
        assertEquals(Arrays.asList(userExpected), systemInSession.getPossibleRegistrationStatus());
    }

    @Test
    public void user_cannot_change_registrationStatus_after_deadline() {
        systemInSession.getTestingSession().setAllowParticipantRegistration(true);
        systemInSession.getTestingSession().setRegistrationDeadlineDate(substractHour(new Date(), 1));
        fakeAdminUser();
        assertNotEquals(Arrays.asList(expectedEmpty), systemInSession.getPossibleRegistrationStatus());
    }

    @Test
    public void admin_can_change_registrationStatus_before_deadline() {
        systemInSession.getTestingSession().setRegistrationDeadlineDate(addHour(new Date(), 1));
        fakeAdminUser();
        assertEquals(Arrays.asList(expectAllStatus), systemInSession.getPossibleRegistrationStatus());
    }

    @Test
    public void admin_can_change_registrationStatus_after_deadline() {
        systemInSession.getTestingSession().setRegistrationDeadlineDate(substractHour(new Date(), 1));
        fakeAdminUser();
        assertEquals(Arrays.asList(expectAllStatus), systemInSession.getPossibleRegistrationStatus());
    }

    private void fakeAdminUser() {
        Mockito.when(identity.hasRole(Role.ADMIN)).thenReturn(true);
    }

    private void mockPreferenceService() {
        Mockito.when(PreferenceService.getObject("user_testing_session")).thenReturn(
                systemInSession.getTestingSession());
    }

    public Date addHour(Date date, int nbHour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, nbHour);
        return cal.getTime();
    }

    public Date substractHour(Date date, int nbHour) {
        return addHour(date, nbHour * -1);
    }
}
