/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>TestingDepth<br>
 * <br>
 * This class describes the TestingDepth object, used by the Gazelle application. A testing type indicates a flag/tag on an IHE implementation, it means : If a system implements the IHE implementation
 * : (SWF, ADT, no option) then it will have the testing type : Supportive If a system implements the IHE implementation : (SWF, OF, no option) then it will have the testing type : Thorough
 * <p/>
 * Supportive and Thorough are testing types, we may have several others.
 * <p/>
 * This class belongs to the Systems module.
 * <p/>
 * TestingDepth possesses the following attributes :
 * <ul>
 * <li><b>id</b> : TestingDepth id</li>
 * <li><b>keyword</b> : Keyword of the TestingDepth (eg. T or S)</li>
 * <li><b>name</b> : Name of the TestingDepth (eg. Supportive, Thorough)</li>
 * <li><b>description</b> : Description of the TestingDepth</li>
 * </ul>
 * </br> <b>Example of TestingDepth</b> : ('S', 'Supportive', 'Indicates if a systems support an IHE implementation (Integration Profile / Actor/ Option - combination) <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, July 10th
 * @class TestingDepth.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@Entity
@Table(name = "tm_testing_depth")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_testing_depth_sequence", sequenceName = "tm_testing_depth_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class TestingDepth extends AuditedObject implements Serializable {
    // ~ Statics variables and Class initializer
    // ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1881413501542441951L;

    // ~ Attributes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * TestingDepth id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_testing_depth_sequence")
    private Integer id;

    /**
     * Keyword of the TestingDepth
     */
    @Column(name = "keyword", unique = true, nullable = false)
    @NotNull(message = "Please provide a keyword (eg. T or S)")
    @Size(min = 1)
    private String keyword;

    /**
     * Name of the TestingDepth
     */
    @Column(name = "name", unique = true, nullable = false)
    @NotNull(message = "Please provide a testing type name (eg. Thorough, or Supportive)")
    @Size(min = 2, max = 128)
    private String name;

    /**
     * Description of the TestingDepth
     */
    @Column(name = "description")
    private String description;

    /**
     * URL to the image/icon, representing this testing type
     */
    @Column(name = "image_url")
    private String imageUrl;

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new TestingDepth object.
     *
     * @param keyword     : Keyword of this TestingDepth
     * @param name        : Name of this TestingDepth
     * @param description : Description of this TestingDepth
     */
    public TestingDepth(final String name, final String description) {

        this.name = name;
        this.description = description;
    }

    /**
     * Creates a new TestingDepth object.
     */
    public TestingDepth() {
    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public TestingDepth(TestingDepth testingDepth) {

        if (testingDepth.keyword != null) {
            keyword = new String(testingDepth.keyword);
        }
        if (testingDepth.name != null) {
            name = new String(testingDepth.name);
        }
        if (testingDepth.description != null) {
            description = new String(testingDepth.description);
        }
        if (testingDepth.imageUrl != null) {
            imageUrl = new String(testingDepth.imageUrl);
        }
    }

    public static List<TestingDepth> getPossibleTestingDepths() {
        TestingDepthQuery q = new TestingDepthQuery();
        List<TestingDepth> possibleTestingDepths = q.getList();

        return possibleTestingDepths;
    }

    /**
     * Get the Id of this TestingDepth object
     *
     * @return TestingDepth id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Set the Id of this TestingDepth object
     *
     * @param id : TestingDepth id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Get the keyword of this TestingDepth object
     *
     * @return keyword : keyword of this TestingDepth
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Set the keyword of this TestingDepth object
     *
     * @param keyword : keyword of this TestingDepth
     */
    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }

    /**
     * Get the Name of this TestingDepth object
     *
     * @return name : Name of this TestingDepth
     */
    @FilterLabel
    public String getName() {
        return name;
    }

    /**
     * Set the Name of this TestingDepth object
     *
     * @param name : Name of this TestingDepth
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Get the description of this TestingDepth object
     *
     * @return description : description of this TestingDepth
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of this TestingDepth object
     *
     * @param description : description of this TestingDepth
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Get the image URL of this TestingDepth object
     *
     * @return imageUrl : imageUrl of this TestingDepth
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Set the image URL of this TestingDepth object
     *
     * @param imageUrl : imageUrl of this TestingDepth
     */
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Overriding method - It returns a string with the TestingDepth tag followed by the name
     *
     * @return string with the TestingDepth tag followed by the name
     */
    @Override
    public String toString() {
        return "TestingDepth(" + name + ")";
    }

}
