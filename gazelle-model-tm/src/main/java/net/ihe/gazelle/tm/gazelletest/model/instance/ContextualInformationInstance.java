package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.gazelletest.model.definition.ContextualInformation;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tm_contextual_information_instance", schema = "public")
@SequenceGenerator(name = "tm_contextual_information_instance_sequence", sequenceName = "tm_contextual_information_instance_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ContextualInformationInstance extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_contextual_information_instance_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "contextual_information", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ContextualInformation contextualInformation;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tm_test_steps_instance_input_ci_instance", joinColumns = @JoinColumn(name = "contextual_information_instance_id"), inverseJoinColumns = @JoinColumn(name = "test_steps_instance_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_instance_id", "contextual_information_instance_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestStepsInstance> testStepsInstancesAsInput;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "tm_test_steps_instance_output_ci_instance", joinColumns = @JoinColumn(name = "contextual_information_instance_id"), inverseJoinColumns = @JoinColumn(name = "test_steps_instance_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_instance_id", "contextual_information_instance_id"}))
    @Fetch(value = FetchMode.SELECT)
    private TestStepsInstance testStepsInstancesAsOutput;

    @Column(name = "value")
    private String value;

    public ContextualInformationInstance() {

    }

    public ContextualInformationInstance(ContextualInformation contextualInformation, String value) {
        this.contextualInformation = contextualInformation;
        this.value = value;
    }

    public ContextualInformationInstance(ContextualInformationInstance inContextualInformationInstance) {
        if (inContextualInformationInstance.getContextualInformation() != null) {
            this.contextualInformation = inContextualInformationInstance.getContextualInformation();
        }
        if (inContextualInformationInstance.getValue() != null) {
            this.value = inContextualInformationInstance.getValue();
        }
    }

    public static List<ContextualInformationInstance> getContextualInformationInstanceListForContextualInformation(
            ContextualInformation inContextualInformation) {
        if (inContextualInformation != null) {
            if (inContextualInformation.getId() != null) {
                ContextualInformationInstanceQuery q = new ContextualInformationInstanceQuery();
                q.contextualInformation().eq(inContextualInformation);
                List<ContextualInformationInstance> list = q.getListDistinct();
                if (list != null) {
                    if (list.size() > 0) {
                        return list;
                    }
                }
            }
        }
        return null;
    }

    public static void removeContextualInformationinstance(ContextualInformationInstance cii, EntityManager em) {
        if (cii != null) {
            em.remove(cii);
            em.flush();
        }
    }

    public Integer getId() {
        return id;
    }

    public List<TestStepsInstance> getTestStepsInstancesAsInput() {
        return HibernateHelper.getLazyValue(this, "testStepsInstancesAsInput", this.testStepsInstancesAsInput);
    }

    public void setTestStepsInstancesAsInput(List<TestStepsInstance> testStepsInstancesAsInput) {
        this.testStepsInstancesAsInput = testStepsInstancesAsInput;
    }

    public TestStepsInstance getTestStepsInstancesAsOutput() {
        return HibernateHelper.getLazyValue(this, "testStepsInstancesAsOutput", this.testStepsInstancesAsOutput);
    }

    public void setTestStepsInstancesAsOutput(TestStepsInstance testStepsInstancesAsOutput) {
        this.testStepsInstancesAsOutput = testStepsInstancesAsOutput;
    }

    public ContextualInformation getContextualInformation() {
        return contextualInformation;
    }

    public void setContextualInformation(ContextualInformation contextualInformation) {
        this.contextualInformation = contextualInformation;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ContextualInformationInstance [id=" + id + ", contextualInformation=" + contextualInformation
                + ", value=" + value + "]";
    }

    public void initContextualInformationInstance(EntityManager em) {
        this.value = this.contextualInformation.getValue();
        em.merge(this);
        em.flush();
    }

}
