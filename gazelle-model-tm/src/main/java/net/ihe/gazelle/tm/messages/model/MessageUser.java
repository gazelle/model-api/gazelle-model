package net.ihe.gazelle.tm.messages.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(MessageUserKey.class)
@Table(name = "usr_messages_usr_users")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class MessageUser implements Serializable {

    private static final long serialVersionUID = -6493247417882045002L;

    @Id
    @Column(name = "users_id")
    private String userId;
    @Id
    @Column(name = "usr_messages_id")
    private Integer messageId;

    public MessageUser() {
    }

    public MessageUser(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageUser)) return false;
        MessageUser that = (MessageUser) o;
        return Objects.equals(userId, that.userId) && Objects.equals(messageId, that.messageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, messageId);
    }
}
