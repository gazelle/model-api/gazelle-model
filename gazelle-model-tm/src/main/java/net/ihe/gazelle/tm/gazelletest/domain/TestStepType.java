package net.ihe.gazelle.tm.gazelletest.domain;

import java.util.ArrayList;
import java.util.List;

public enum TestStepType {
    BASIC("net.ihe.gazelle.tm.basicTestStep"),
    AUTOMATED("net.ihe.gazelle.tm.automatedStep");

    private final String key;

    TestStepType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public static TestStepType fromKey(String key) {
        for (TestStepType testStepType : TestStepType.values()) {
            if (testStepType.getKey().equals(key)) {
                return testStepType;
            }
        }
        return null;
    }

    public static List<String> getTestStepTypes() {
        List<String> testStepTypes = new ArrayList<>();
        for (TestStepType testStepType : TestStepType.values()) {
            testStepTypes.add(testStepType.getKey());
        }
        return testStepTypes;
    }
}

