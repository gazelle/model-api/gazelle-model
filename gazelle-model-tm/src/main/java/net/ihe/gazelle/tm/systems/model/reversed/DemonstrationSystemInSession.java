package net.ihe.gazelle.tm.systems.model.reversed;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.systems.model.Demonstration;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "tm_demonstration_system_in_session", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_in_session_id", "demonstration_id"}))
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DemonstrationSystemInSession {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "systemInSessionId", column = @Column(name = "system_in_session_id", nullable = false)),
            @AttributeOverride(name = "demonstrationId", column = @Column(name = "demonstration_id", nullable = false))})
    private DemonstrationSystemInSessionId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "demonstration_id", nullable = false, insertable = false, updatable = false)
    @Fetch(FetchMode.SELECT)
    private Demonstration demonstration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_in_session_id", nullable = false, insertable = false, updatable = false)
    @Fetch(FetchMode.SELECT)
    private SystemInSession systemInSession;

    public DemonstrationSystemInSession() {
    }

    public DemonstrationSystemInSession(DemonstrationSystemInSessionId id, Demonstration demonstration,
                                        SystemInSession systemInSession) {
        this.id = id;
        this.demonstration = demonstration;
        this.systemInSession = systemInSession;
    }

    public DemonstrationSystemInSessionId getId() {
        return this.id;
    }

    public void setId(DemonstrationSystemInSessionId id) {
        this.id = id;
    }

    public Demonstration getDemonstration() {
        return HibernateHelper.getLazyValue(this, "demonstration", this.demonstration);
    }

    public void setDemonstration(Demonstration demonstration) {
        this.demonstration = demonstration;
    }

    public SystemInSession getSystemInSession() {
        return HibernateHelper.getLazyValue(this, "systemInSession", this.systemInSession);
    }

    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
