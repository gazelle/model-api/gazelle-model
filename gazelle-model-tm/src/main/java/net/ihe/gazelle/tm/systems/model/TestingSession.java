/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.configurations.model.Host;
import net.ihe.gazelle.tm.configurations.model.NetworkConfigurationForTestingSession;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.users.model.Currency;
import net.ihe.gazelle.users.model.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.*;

/**
 * <b>Class Description : </b>TestingSession<br>
 * <br>
 * This class describes the TestingSession object, used by the Gazelle application. It corresponds to a testing event
 * (example : a connectathon) This class belongs to the Systems module.
 * <p/>
 * TestingSession possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Testing Session</li>
 * <li><b>type</b> : Type of event (example : Connectathon, or Show case, etc...)</li>
 * <li><b>zone</b> : Geographic Zone where this event is held (Example : North America)</li>
 * <li><b>description</b> : Description of this testing event</li>
 * <li><b>beginningSession</b> : Beginnning date of the event (example : 01/28/2008)</li>
 * <li><b>endingSession</b> : Ending date of the event (example : 02/01/2008)</li>
 * <li><b>addressSession</b> : Address where this event is held</li>
 * <li><b>isContractRequired</b> : Indicates whether a contract is required to attend to this testing session
 * <li><b>pathToContractTemplate</b>: Give the path to the Jasper Report used as a template for building contract
 * <li><b>nbOfTestInstancesClaimedByQRCode</b>: Counts the number of test instances which have been claimed by
 * flashing the QR code</li>
 * <li><b>nbOfTestInstancesVerifiedOnSmartphone</b>: Counts the number of test instances which have been verified
 * (status change) from Gazelle Monitor App</li>
 * <li><b>nbOfTestInstancesAccessedByQRCode</b>: Counts the number of accesses to Gazelle Monitor App using the QR
 * code</li>
 * <p/>
 * <p/>
 * </ul>
 * </br> <b>Example of TestingSession</b> : Connectathon, North America, 2008, Chicago Cat 2008 Hyatt Regency,
 * 01/28/08, 02/01/08, address of the event<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, December 21
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "TestingSession")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "tm_testing_session", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_testing_session_sequence", sequenceName = "tm_testing_session_id_seq", allocationSize = 1)
//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestingSession extends AuditedObject implements java.io.Serializable, Comparable<TestingSession> {

   /**
    * Serial ID version of this object
    */
   private static final long serialVersionUID = 832587470009418950L;

   private static Logger log = LoggerFactory.getLogger(TestingSession.class);

   // Attributes (existing in database as a column)
   @Id
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_testing_session_sequence")
   @XmlElement(name = "id")
   private Integer id;

   @Column(name = "name", unique=true)
   @NotNull
   private String name;

   @Column(name = "internetTesting")
   private Boolean internetTesting;

   @Column(name = "systemAutoAcceptance", nullable = false)
   private boolean systemAutoAcceptance;

   @Column(name = "allowParticipantRegistration")
   private Boolean allowParticipantRegistration;


   @XmlElement(name = "description")
   @Column(name = "description")
   private String description;

   @Column(name = "beginning_date")
   private Date beginningSession;

   @Column(name = "ending_date")
   private Date endingSession;

   @Column(name = "registration_deadline_date")
//    @NotNull(message = "You must fill the registration deadline date")
   private Date registrationDeadlineDate;

   @Column(name = "continuous_session")
   private Boolean continuousSession;

   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "currency_id")
   @Fetch(value = FetchMode.SELECT)
   private Currency currency;

   @Column(name = "invoice_payment_deadline_date")
   private Date invoicePaymentDeadline;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "address_id")
   private Address addressSession;

   @Column(name = "active")
   private Boolean activeSession;

   @Column(name = "hidden")
   private Boolean hiddenSession;

   @Column(name = "disable_auto_results")
   private Boolean disableAutoResults;

   @Column(name = "disable_patient_generation_and_sharing")
   private Boolean disablePatientGenerationAndSharing = false;

   @Column(name = "order_in_gui")
   private Integer orderInGUI;

   @Column(name = "mailing_list_url", length = 255)
   private String mailingListUrl;

   @Column(name = "wiki_url", length = 255)
   private String wikiUrl;

   @Column(name = "certificates_rul", length = 255)
   private String certificatesUrl;

   @Column(name = "display_certificates_menu")
   private boolean displayCertificatesMenu = true;

   @Column(name = "allow_one_company_play_several_role_in_p2p_tests", nullable = false)
   private boolean allowOneCompanyPlaySeveralRolesInP2PTests;

   @Column(name = "allow_one_company_play_several_role_in_group_tests", nullable = false)
   private boolean allowOneCompanyPlaySeveralRolesInGroupTests;

   @Column(name = "allow_participants_start_group_tests", nullable = false)
   private boolean allowParticipantsStartGroupTests;

   @Column(name = "hide_advanced_sample_search_to_vendors", nullable = false)
   private boolean hideAdvancedSampleSearchToVendors;

   @Column(name = "session_without_monitors", nullable = false)
   private boolean sessionWithoutMonitors;


   /* A demonstration is mapped with one or many testing sessions */
   @ManyToMany(fetch = FetchType.LAZY)
   @JoinTable(name = "tm_demonstrations_in_testing_sessions", joinColumns = @JoinColumn(name = "testing_session_id"),
         inverseJoinColumns = @JoinColumn(name = "demonstration_id"))
   @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   private List<Demonstration> demonstrations;

   /* A testing session is mapped with one or many domains */
   /* this list is now backed by integrationProfiles */
   @ManyToMany(fetch = FetchType.LAZY)
   @Fetch(FetchMode.SELECT)
   @JoinTable(name = "tm_domains_in_testing_sessions", joinColumns = @JoinColumn(name = "testing_session_id"),
         inverseJoinColumns = @JoinColumn(name = "domain_id"))
   @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   @Deprecated
   private List<Domain> domains;

   @ManyToMany(fetch = FetchType.EAGER)
   @Fetch(FetchMode.SELECT)
   @JoinTable(name = "tm_profiles_in_testing_sessions", joinColumns = @JoinColumn(name = "testing_session_id"),
         inverseJoinColumns = @JoinColumn(name = "integration_profile_id"))
   //@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   private List<IntegrationProfile> integrationProfiles;

   @ManyToMany(fetch = FetchType.EAGER)
   @Fetch(FetchMode.SELECT)
   @JoinTable(name = "tm_test_types_sessions", joinColumns = @JoinColumn(name = "testing_session_id"),
         inverseJoinColumns = @JoinColumn(name = "test_type_id"))
   @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   private List<TestType> testTypes;

   @Column(name = "is_critical_status_enabled")
   private boolean isCriticalStatusEnabled;

   @Column(name = "is_proxy_use_enabled")
   private boolean isProxyUseEnabled;

   @Column(name = "contact_lastname", nullable = false)
   @NotNull(message = "Please provide Last Name")
   @Size(max = 255)
   private String contactLastname;

   @Column(name = "contact_firstname", nullable = false)
   @NotNull(message = "Please provide First Name")
   @Size(max = 255)
   private String contactFirstname;

   @Column(name = "contact_email", nullable = false)
   @Pattern(regexp = "^.+@[^\\.].*\\.[a-z]{2,}$", message = "{gazelle.validator.email}")
   @Size(max = 255)
   private String contactEmail;

   @Column(name = "configuration_overview", length = 20000)
   private String configurationOverview;

   @Column(name = "fee_first_system")
   private BigDecimal feeFirstSystem;

   @Column(name = "fee_additional_system")
   private BigDecimal feeAdditionalSystem;

   @Column(name = "fee_participant")
   private BigDecimal feeParticipant;

   @Deprecated
   @Column(name = "domain_fee")
   private BigDecimal domainFee;

   @Column(name = "vat_percent")
   private BigDecimal vatPercent;

   @Column(name = "required_contract")
   private boolean contractRequired;

   @Column(name = "contract_template_path")
   private String pathToContractTemplate;

   @Column(name = "invoice_template_path")
   private String pathToInvoiceTemplate;

   @Column(name = "next_invoice_number")
   private Integer nextInvoiceNumber;

   @Column(name = "ti_claimed_by_qr")
   private Integer nbOfTestInstancesClaimedByQRCode;

   @Column(name = "ti_verified_on_smartphone")
   private Integer nbOfTestInstancesVerifiedOnSmartphone;

   @Column(name = "ti_accessed_by_qr")
   private Integer nbOfTestInstancesAccessedByQRCode;

   @Column(name = "logo_url")
   private String logoUrl;

   /**
    * Link to the project home page, available by clickink on the logo, for example http://gazelle.ihe.net
    */
   @Column(name = "logo_link_url")
   private String logoLinkUrl;

   @Column(name = "iso3_vat_countries")
   private String iso3VATCountries;

   @Column(name = "session_closed")
   private Boolean sessionClosed;

   @Column(name = "conformity_test_report_enabled")
   private boolean conformityTestReportEnabled;

   @Column(name = "results_compute")
   private Date resultsCompute;

   @Column(name = "enable_precat_tests")
   private Boolean enablePrecatTests;

   @Column(name = "target_color")
   private String targetColor;

   @Column(name = "nb_participants_included_in_system_fees", columnDefinition = "int default 2")
   private Integer nbParticipantsIncludedInSystemFees = 2;

   @Column(name = "allow_report_download")
   private Boolean allowReportDownload = true;

   // TODO add @PrimaryKeyJoinColumn, and add @OneToOnewith mappedBy attribute on
   // NetworkConfigurationForTestingSession class
   @OneToOne
   @Fetch(FetchMode.SELECT)
   private NetworkConfigurationForTestingSession networkConfiguration;

   @OneToMany(mappedBy = "testingSession", fetch = FetchType.LAZY)
   @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   private List<SystemInSession> systemInSessions;

   @OneToMany(mappedBy = "testingSession", fetch = FetchType.LAZY)
   @Fetch(FetchMode.SELECT)
   @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   private List<MonitorInSession> monitors;

   @OneToMany(mappedBy = "testingSessionId", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
   @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
   private List<TestingSessionAdmin> testingSessionAdmins;

   @Column(name = "default_testing_session")
   private Boolean isDefaultTestingSession = false;

   @Column(name = "send_notification_by_email")
   private Boolean sendNotificationByEmail = false;

   @OneToMany(mappedBy = "testingSession", fetch = FetchType.EAGER)
   private List<Host> hosts;

   @Transient
   private transient UserService userClient;

   @Transient
   private static final Logger LOGGER = LoggerFactory.getLogger(TestingSession.class);

   // Constructors
   public TestingSession() {
      this.contractRequired = true;
   }

   public TestingSession(Integer id, String name) {
      this.id = id;
      this.setName(name);
   }

   public TestingSession(Integer id, String name, String description) {
      this.id = id;
      this.setName(name);
      this.setDescription(description);
   }

   public TestingSession(Integer id, String name, String zone, Integer year, String description, Date beginningSession,
                         Date endingSession, Address addressSession) {
      this.id = id;
      this.setName(name);
      this.setDescription(description);
      this.setBeginningSession(beginningSession);
      this.setEndingSession(endingSession);
      this.setAddressSession(addressSession);
      networkConfiguration = new NetworkConfigurationForTestingSession();
   }

   /**
    * Getter of conformity TestReport Enabled
    *
    * @return boolean
    */
   public boolean getConformityTestReportEnabled() {
      return conformityTestReportEnabled;
   }

   /**
    * Setter of conformity TestReport Enabled
    *
    * @param conformityTestReportEnabled boolean
    */
   public void setConformityTestReportEnabled(boolean conformityTestReportEnabled) {
      this.conformityTestReportEnabled = conformityTestReportEnabled;
   }

   public static List<TestingSession> getTestingSessionWhereUserIsAdmin(String userId) {
      TestingSessionQuery query = new TestingSessionQuery();
      List<TestingSession> testingSessions = query.getList();
      List<TestingSession> results = new ArrayList<>();
      for (TestingSession session : testingSessions) {
         if(session.getTestingSessionAdmins().contains(userId)) {
            results.add(session);
         }
      }
      LOGGER.info(results.toString());
      return results;
   }

   public static List<Institution> getListOfInstitutionsParticipatingInSession(TestingSession ts) {
      if (ts == null) {
         return null;
      }
      SystemInSessionQuery query = new SystemInSessionQuery();
      query.testingSession().id().eq(ts.getId());
      return query.system().institutionSystems().institution().getListDistinct();
   }

   public static List<Institution> getListOfInstitutionsRegisterInSession(TestingSession ts) {

      if (ts == null) {
         return null;
      }

      SystemInSessionQuery query = new SystemInSessionQuery();
      query.testingSession().id().eq(ts.getId());
      return query.system().institutionSystems().institution().getListDistinct();
   }

   public static List<TestingSession> getPossibleTestingSessions() {
      TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
      testingSessionQuery.name().order(true);
      return testingSessionQuery.getList();
   }

   public static List<TestingSession> getTestingSessionsWhereCompanyWasHere(String companyName) {
      if ((companyName == null) || (companyName.length() == 0)) {
         log.error("getTestingSessionsWhereCompanyWasHere - Institution name given as parameter is null");
         return null;
      } else {
         SystemInSessionQuery q = new SystemInSessionQuery();
         InstitutionSystemQuery q1 = new InstitutionSystemQuery();
         q1.institution().name().eq(companyName);
         q.system().id().in(q1.system().id().getListDistinct());
         q.testingSession().name().order(false);
         return q.testingSession().getListDistinctOrdered();
      }
   }

   /**
    * Returns the list of all active sessions
    *
    * @return
    */
   public static List<TestingSession> GetAllActiveSessions() {
      TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
      testingSessionQuery.activeSession().eq(true);
      testingSessionQuery.orderInGUI().order(true);
      return testingSessionQuery.getList();
   }

   /**
    * Returns the list of all active sessions without hidden
    *
    * @return
    */
   public static List<TestingSession> GetAllActiveSessionsWithoutHidden() {
      TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
      testingSessionQuery.activeSession().eq(true);
      testingSessionQuery.hiddenSession().eq(false);
      testingSessionQuery.orderInGUI().order(true);
      return testingSessionQuery.getListDistinctOrdered();
   }

   /**
    * Returns the default testing session
    *
    * @return
    */
   public static TestingSession getDefaultTestingSession() {
      TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
      testingSessionQuery.isDefaultTestingSession().eq(true);
      return testingSessionQuery.getUniqueResult();
   }

   /**
    * Returns the list of all sessions
    *
    * @return
    */
   public static List<TestingSession> getAllSessions() {
      TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
      testingSessionQuery.orderInGUI().order(true);
      return testingSessionQuery.getList();
   }

   public static TestingSession getSessionById(Integer id) {
      EntityManager em = EntityManagerService.provideEntityManager();
      try {
         return em.find(TestingSession.class, id);
      } catch (Exception e) {
         log.error("could not find any session with id=" + id);
         return null;
      }
   }

   public static Integer getNextInvoiceNumberStatic(TestingSession ts) {
      EntityManager em = EntityManagerService.provideEntityManager();
      ts = em.find(TestingSession.class, ts.getId());
      if (ts.getNextInvoiceNumber() == null) {

         ts.setNextInvoiceNumber(1);
      } else {
         ts.setNextInvoiceNumber(ts.getNextInvoiceNumber() + 1);
      }
      em.merge(ts);
      em.flush();
      return ts.getNextInvoiceNumber();
   }

   public static TestingSession mergeTestingSession(TestingSession ts, EntityManager em) {
      ts = em.merge(ts);
      em.flush();
      return ts;
   }

   public static void removeTestingSession(TestingSession ts, EntityManager em) {
      if (ts.id != null) {
         String tsName = ts.name;
         em.remove(ts);
         try {
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, tsName + " is deleted !");
         } catch (PersistenceException e) {
            log.error("" + e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fail to delete " + tsName);
         }
      }
   }

   public static TestingSession getDefault() {
      TestingSessionQuery query = new TestingSessionQuery();
      query.isDefaultTestingSession().eq(true);
      TestingSession uniqueResult = query.getUniqueResult();
      if (uniqueResult == null) {
         uniqueResult = getLast();
      }
      return uniqueResult;
   }

   private static TestingSession getLast() {
      TestingSessionQuery query;
      TestingSession uniqueResult;
      query = new TestingSessionQuery();
      query.id().order(false);
      query.setMaxResults(1);
      uniqueResult = query.getUniqueResult();
      return uniqueResult;
   }

   public Boolean getIsDefaultTestingSession() {
      return isDefaultTestingSession;
   }

   public void setIsDefaultTestingSession(Boolean isDefaultTestingSession) {
      this.isDefaultTestingSession = isDefaultTestingSession;
   }

   public List<TestingSessionAdmin> getTestingSessionAdmins() {
      return testingSessionAdmins;
   }

   public void setTestingSessionAdmins(List<TestingSessionAdmin> testingSessionAdmins) {
         this.testingSessionAdmins = testingSessionAdmins;
   }

   public Boolean getSessionClosed() {
      return sessionClosed;
   }

   public void setSessionClosed(Boolean sessionClosed) {
      if (sessionClosed == null) {
         sessionClosed = false;
      }
      this.sessionClosed = sessionClosed;
   }

   public Integer getId() {
      return this.id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   @FilterLabel
   public String getName() {
      return this.name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Boolean getInternetTesting() {
      if (internetTesting == null) {
         return false;
      }
      return internetTesting;
   }

   public void setInternetTesting(Boolean internetTesting) {
      this.internetTesting = internetTesting;
   }

   public boolean isSystemAutoAcceptance() {
      return systemAutoAcceptance;
   }

   public void setSystemAutoAcceptance(boolean autoAcceptance) {
      this.systemAutoAcceptance = autoAcceptance;
   }


   public boolean isHideAdvancedSampleSearchToVendors() {
      return hideAdvancedSampleSearchToVendors;
   }

   public void setHideAdvancedSampleSearchToVendors(boolean hideSamples) {
      this.hideAdvancedSampleSearchToVendors = hideSamples;
   }


   public boolean isSessionWithoutMonitors() {
      return sessionWithoutMonitors;
   }

   public void setSessionWithoutMonitors(boolean allStatusEnable) {
      this.sessionWithoutMonitors = allStatusEnable;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Date getBeginningSession() {
      return this.beginningSession;
   }

   public void setBeginningSession(Date beginningSession) {
      this.beginningSession = beginningSession;
   }

   public Date getEndingSession() {
      return this.endingSession;
   }

   public void setEndingSession(Date endingSession) {
      this.endingSession = endingSession;
   }

   public Date getRegistrationDeadlineDate() {
      return registrationDeadlineDate;
   }

   public void setRegistrationDeadlineDate(Date registrationDeadlineDate) {
      this.registrationDeadlineDate = registrationDeadlineDate;
   }

   public Boolean getContinuousSession() {
      return continuousSession;
   }

   public void setContinuousSession(Boolean continuousSession) {
      this.continuousSession = continuousSession;
   }

   public Address getAddressSession() {
      return HibernateHelper.getLazyValue(this, "addressSession", this.addressSession);
   }

   public void setAddressSession(Address addressSession) {
      this.addressSession = addressSession;
   }

   public Boolean getActiveSession() {
      if (activeSession == null) {
         activeSession = false;
      }
      return activeSession;
   }

   public void setActiveSession(Boolean activeSession) {
      this.activeSession = activeSession;
   }

   public Currency getCurrency() {
      return currency;
   }

   public void setCurrency(Currency currency) {
      this.currency = currency;
   }

   public Date getInvoicePaymentDeadline() {
      return invoicePaymentDeadline;
   }

   public void setInvoicePaymentDeadline(Date invoicePaymentDeadline) {
      this.invoicePaymentDeadline = invoicePaymentDeadline;
   }

   public Boolean getHiddenSession() {
      if (hiddenSession == null) {
         hiddenSession = false;
      }
      return hiddenSession;
   }

   public void setHiddenSession(Boolean hiddenSession) {
      this.hiddenSession = hiddenSession;
   }

   public Integer getOrderInGUI() {
      return orderInGUI;
   }

   public void setOrderInGUI(Integer orderInGUI) {
      this.orderInGUI = orderInGUI;
   }

   public String getIso3VATCountries() {
      return iso3VATCountries;
   }

   public void setIso3VATCountries(String iso3vatCountries) {
      iso3VATCountries = iso3vatCountries;
   }

   public BigDecimal getFeeParticipant() {
      if (feeParticipant == null) {
         feeParticipant = BigDecimal.ZERO;
      }
      return feeParticipant;
   }

   public void setFeeParticipant(BigDecimal feeParticipant) {
      this.feeParticipant = feeParticipant;
   }

   public List<SystemInSession> getSystemInSessions() {
      return HibernateHelper.getLazyValue(this, "systemInSessions", this.systemInSessions);
   }

   public void setSystemInSessions(List<SystemInSession> systemInSessions) {
      this.systemInSessions = systemInSessions;
   }

   public List<MonitorInSession> getMonitors() {
      return HibernateHelper.getLazyValue(this, "monitors", this.monitors);
   }

   public List<Demonstration> getDemonstrations() {
      return HibernateHelper.getLazyValue(this, "demonstrations", this.demonstrations);
   }

   public void setDemonstrations(List<Demonstration> demonstrations) {
      this.demonstrations = demonstrations;
   }

   public boolean getIsCriticalStatusEnabled() {
      return isCriticalStatusEnabled;
   }

   public void setIsCriticalStatusEnabled(boolean isCriticalStatusEnabled) {
      this.isCriticalStatusEnabled = isCriticalStatusEnabled;
   }

   public boolean getIsProxyUseEnabled() {
      return isProxyUseEnabled;
   }

   public void setIsProxyUseEnabled(boolean isProxyUseEnabled) {
      this.isProxyUseEnabled = isProxyUseEnabled;
   }

   public String getConfigurationOverview() {
      return configurationOverview;
   }

   public void setConfigurationOverview(String configurationOverview) {
      this.configurationOverview = configurationOverview;
   }

   public BigDecimal getFeeFirstSystem() {
      if (feeFirstSystem == null) {
         feeFirstSystem = BigDecimal.ZERO;
      }
      return feeFirstSystem;
   }

   public void setFeeFirstSystem(BigDecimal feeFirstSystem) {
      this.feeFirstSystem = feeFirstSystem;
   }

   public BigDecimal getFeeAdditionalSystem() {
      if (feeAdditionalSystem == null) {
         feeAdditionalSystem = BigDecimal.ZERO;
      }
      return feeAdditionalSystem;
   }

   public void setFeeAdditionalSystem(BigDecimal feeAdditionalSystem) {
      this.feeAdditionalSystem = feeAdditionalSystem;
   }

   public BigDecimal getDomainFee() {
      if (domainFee == null) {
         domainFee = BigDecimal.ZERO;
      }
      return domainFee;
   }

   public void setDomainFee(BigDecimal domainFee) {
      this.domainFee = domainFee;
   }

   public BigDecimal getVatPercent() {
      return vatPercent;
   }

   public void setVatPercent(BigDecimal vatPercent) {
      this.vatPercent = vatPercent;
   }

   public String getContactLastname() {
      return contactLastname;
   }

   public void setContactLastname(String contactLastname) {
      this.contactLastname = contactLastname;
   }

   public String getContactFirstname() {
      return contactFirstname;
   }

   public void setContactFirstname(String contactFirstname) {
      this.contactFirstname = contactFirstname;
   }

   public String getContactEmail() {
      return contactEmail;
   }

   public void setContactEmail(String contactEmail) {
      this.contactEmail = contactEmail;
   }

   public Date getResultsCompute() {
      return resultsCompute;
   }

   public void setResultsCompute(Date resultsCompute) {
      this.resultsCompute = resultsCompute;
   }

   public String getLogoUrl() {
      return logoUrl;
   }

   public void setLogoUrl(String logoUrl) {
      this.logoUrl = logoUrl;
   }

   public String getLogoLinkUrl() {
      return logoLinkUrl;
   }

   public void setLogoLinkUrl(String logoLinkUrl) {
      this.logoLinkUrl = logoLinkUrl;
   }

   public String getTargetColor() {
      return targetColor;
   }

   public void setTargetColor(String targetColor) {
      this.targetColor = targetColor;
   }

   /**
    * Use this method to get the list of integration profiles. beware if you use collection.sort, you need to clone the
    * object before
    *
    * @return
    */
   public List<IntegrationProfile> getIntegrationProfilesUnsorted() {
      if ((integrationProfiles == null) || (integrationProfiles.size() == 0)) {
         log.info(
               "Object integrationProfiles is empty recreating it - Testing session is : " + this.getId() + ", " + this.getDescription());
         integrationProfiles = new ArrayList<IntegrationProfile>();
         if ((domains != null) && (domains.size() > 0)) {
            log.info("Reloading Object integrationProfiles for testing session from domainsProfile");
            IntegrationProfileQuery query = new IntegrationProfileQuery();
            query.domainsProfile().domain().in(domains);
            integrationProfiles = query.getList();
            // reset domains
            domains.clear();
         }
      }
      return integrationProfiles;
   }

   public List<IntegrationProfile> getIntegrationProfiles() {
      List<IntegrationProfile> integrationProfiles = getIntegrationProfilesUnsorted();
      ArrayList<IntegrationProfile> integrationProfilesCopy = new ArrayList<IntegrationProfile>(integrationProfiles);

      Collections.sort(integrationProfilesCopy);
      return integrationProfilesCopy;
   }

   public void setIntegrationProfiles(List<IntegrationProfile> integrationProfiles) {
      this.integrationProfiles = integrationProfiles;
   }

   public boolean isContractRequired() {
      return contractRequired;
   }

   public void setContractRequired(boolean contractRequired) {
      this.contractRequired = contractRequired;
   }

   public Integer getNbOfTestInstancesClaimedByQRCode() {
      return nbOfTestInstancesClaimedByQRCode;
   }

   public void setNbOfTestInstancesClaimedByQRCode(Integer nbOfTestInstancesClaimedByQRCode) {
      this.nbOfTestInstancesClaimedByQRCode = nbOfTestInstancesClaimedByQRCode;
   }

   public Integer getNbOfTestInstancesVerifiedOnSmartphone() {
      return nbOfTestInstancesVerifiedOnSmartphone;
   }

   public void setNbOfTestInstancesVerifiedOnSmartphone(Integer nbOfTestInstancesVerifiedOnSmartphone) {
      this.nbOfTestInstancesVerifiedOnSmartphone = nbOfTestInstancesVerifiedOnSmartphone;
   }

   public Integer getNbOfTestInstancesAccessedByQRCode() {
      return nbOfTestInstancesAccessedByQRCode;
   }

   public void setNbOfTestInstancesAccessedByQRCode(Integer nbOfTestInstancesAccessedByQRCode) {
      this.nbOfTestInstancesAccessedByQRCode = nbOfTestInstancesAccessedByQRCode;
   }

   public String getPathToContractTemplate() {
      return pathToContractTemplate;
   }

   public void setPathToContractTemplate(String pathToContractTemplate) {
      this.pathToContractTemplate = pathToContractTemplate;
   }

   public String getPathToInvoiceTemplate() {
      return pathToInvoiceTemplate;
   }

   public void setPathToInvoiceTemplate(String pathToInvoiceTemplate) {
      this.pathToInvoiceTemplate = pathToInvoiceTemplate;
   }

   public Integer getNextInvoiceNumber() {
      return nextInvoiceNumber;
   }

   public void setNextInvoiceNumber(Integer nextInvoiceNumber) {
      this.nextInvoiceNumber = nextInvoiceNumber;
   }

   public NetworkConfigurationForTestingSession getNetworkConfiguration() {
      return networkConfiguration;
   }

   public void setNetworkConfiguration(NetworkConfigurationForTestingSession networkConfiguration) {
      this.networkConfiguration = networkConfiguration;
   }

   public String getMailingListUrl() {
      return mailingListUrl;
   }

   public void setMailingListUrl(String mailingListUrl) {
      this.mailingListUrl = mailingListUrl;
   }

   public String getWikiUrl() {
      return wikiUrl;
   }

   public void setWikiUrl(String wikiUrl) {
      this.wikiUrl = wikiUrl;
   }

   public Boolean getDisableAutoResults() {
      return disableAutoResults;
   }

   public void setDisableAutoResults(Boolean disableAutoResults) {
      this.disableAutoResults = disableAutoResults;
   }

   public boolean isEnablePrecatTests() {
      if (enablePrecatTests == null) {
         return true;
      }
      return enablePrecatTests;
   }

   public void setEnablePrecatTests(boolean enablePrecatTests) {
      this.enablePrecatTests = enablePrecatTests;
   }

   public List<TestType> getTestTypes() {
      if ((testTypes == null) || (testTypes.size() == 0)) {
         testTypes = new ArrayList<TestType>();
         testTypes.add(TestType.getTYPE_CONNECTATHON());
      }
      return testTypes;
   }

   public void setTestTypes(List<TestType> testTypes) {
      this.testTypes = testTypes;
   }

   public boolean isRegistrationOpened() {
      return !isClosed() && (Boolean.TRUE.equals(continuousSession) || !isRegistratioDeadlineOver());
   }

   public boolean isRegistratioDeadlineOver() {
      return registrationDeadlineDate != null &&
            registrationDeadlineDate.before(new Date());
   }

   public List<Domain> getDomains() {
      List<IntegrationProfile> integrationProfiles = getIntegrationProfilesUnsorted();
      Set<Domain> domains = new TreeSet<Domain>();
      for (IntegrationProfile integrationProfile : integrationProfiles) {
         domains.addAll(integrationProfile.getDomainsForDP());
      }
      return new ArrayList<Domain>(domains);
   }

   public List<Actor> getActorList() {
      ActorQuery query = new ActorQuery();
      query.actorIntegrationProfiles().integrationProfile().in(getIntegrationProfilesUnsorted());
      query.keyword().order(true);
      return query.getList();
   }

   public void setUserClient(UserService userClient) {
      this.userClient = userClient;
   }

   @Override
   public int compareTo(TestingSession o) {
      if (o == null) {
         return 1;
      } else {
         return getId().compareTo(o.getId());
      }
   }

   @Override
   public String toString() {
      return "TestingSession [getDescription()=" + getDescription() + "]";
   }

   public List<Iso3166CountryCode> getIso3166CountryCodeFiltered(EntityManager em, String iso, String iso3,
                                                                 String name) {
      Iso3166CountryCodeQuery q = new Iso3166CountryCodeQuery();
      if (iso != null) {
         q.iso().eq(iso);
      }
      if (iso3 != null) {
         q.iso3().eq(iso3);
      }
      if (name != null) {
         q.name().eq(name);
      }
      return q.getList();
   }

   public List<Iso3166CountryCode> getListVATCountry(EntityManager em) {
      List<Iso3166CountryCode> res = new ArrayList<Iso3166CountryCode>();
      if (this.getIso3VATCountries() != null) {
         String[] sa = this.getIso3VATCountries().split(" ");
         for (String string : sa) {
            if ((string != null) && (!string.equals(""))) {
               List<Iso3166CountryCode> liso = getIso3166CountryCodeFiltered(em, null, string, null);
               res.addAll(liso);
            }
         }
      }
      if (res.size() == 0) {
         res = getIso3166CountryCodeFiltered(em, null, "BEL", null);
      }
      return res;
   }

   public boolean isClosed() {
      return Boolean.TRUE.equals(sessionClosed);
   }

   public boolean testingSessionClosedForUser() {
      if (this.isClosed()) {
         GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
         return !identity.hasRole(Role.ADMIN) &&
               !identity.hasRole(Role.MONITOR);
      }
      return false;
   }

   public Boolean getAllowParticipantRegistration() {
      if (allowParticipantRegistration == null) {
         return false;
      }
      return allowParticipantRegistration;
   }

   public void setAllowParticipantRegistration(Boolean allowParticipantRegistration) {
      this.allowParticipantRegistration = allowParticipantRegistration;
   }

   public boolean isTestingInteroperability() {
      if (getTestTypes() == null) {
         return false;
      }
      return getTestTypes().contains(TestType.getTYPE_INTEROPERABILITY());
   }

   public String getCertificatesUrl() {
      String certifUrl = ApplicationPreference.getCertificatesUrlFromPreferences();
      if ((certificatesUrl == null || certificatesUrl.isEmpty()) && certifUrl != null) {
         setCertificatesUrl(certifUrl);
      }
      return certificatesUrl;
   }

   public void setCertificatesUrl(String certificatesUrl) {
      this.certificatesUrl = certificatesUrl;
   }

   public boolean isDisplayCertificatesMenu() {
      return displayCertificatesMenu;
   }

   public void setDisplayCertificatesMenu(boolean displayCertificatesMenu) {
      this.displayCertificatesMenu = displayCertificatesMenu;
   }

   public boolean isAllowOneCompanyPlaySeveralRolesInP2PTests() {
      return allowOneCompanyPlaySeveralRolesInP2PTests;
   }

   public void setAllowOneCompanyPlaySeveralRolesInP2PTests(boolean allowOneCompanyPlaySeveralRolesInP2PTests) {
      this.allowOneCompanyPlaySeveralRolesInP2PTests = allowOneCompanyPlaySeveralRolesInP2PTests;
   }

   public boolean isAllowReportDownload() {
      return allowReportDownload;
   }

   public void setAllowReportDownload(boolean allowReportDownload) {
      this.allowReportDownload = allowReportDownload;
   }

   public boolean isAllowOneCompanyPlaySeveralRolesInGroupTests() {
      return allowOneCompanyPlaySeveralRolesInGroupTests;
   }

   public void setAllowOneCompanyPlaySeveralRolesInGroupTests(boolean allowOneCompanyPlaySeveralRolesInGroupTests) {
      this.allowOneCompanyPlaySeveralRolesInGroupTests = allowOneCompanyPlaySeveralRolesInGroupTests;
   }

   public boolean isAllowParticipantsStartGroupTests() {
      return allowParticipantsStartGroupTests;
   }

   public void setAllowParticipantsStartGroupTests(boolean allowParticipantsStartGroupTests) {
      this.allowParticipantsStartGroupTests = allowParticipantsStartGroupTests;
   }

   public Boolean getDisablePatientGenerationAndSharing() {
      if (disablePatientGenerationAndSharing == null) {
         return false;
      }
      return disablePatientGenerationAndSharing;
   }

   public void setDisablePatientGenerationAndSharing(Boolean disablePatientGenerationAndSharing) {
      this.disablePatientGenerationAndSharing = disablePatientGenerationAndSharing;
   }

   public Integer getNbParticipantsIncludedInSystemFees() {
      return nbParticipantsIncludedInSystemFees;
   }

   public void setNbParticipantsIncludedInSystemFees(Integer nbParticipantsIncludedInSystemFees) {
      this.nbParticipantsIncludedInSystemFees = nbParticipantsIncludedInSystemFees;
   }

   public Boolean getSendNotificationByEmail() {
      return sendNotificationByEmail;
   }

   public void setSendNotificationByEmail(Boolean sendNotificationByEmail) {
      this.sendNotificationByEmail = sendNotificationByEmail;
   }

   public boolean isLogoIsURL() {
      String path = getLogoUrl();
      if (path == null) {
         return true;
      }
      if (path.isEmpty()) {
         return true;
      }
      if (path.contains("http")) {
         return true;
      }
      /*This is an image resource from the ear*/
      return false;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      if (!super.equals(o)) {
         return false;
      }

      TestingSession that = (TestingSession) o;

      if (displayCertificatesMenu != that.displayCertificatesMenu) {
         return false;
      }
      if (allowOneCompanyPlaySeveralRolesInP2PTests != that.allowOneCompanyPlaySeveralRolesInP2PTests) {
         return false;
      }
      if (allowOneCompanyPlaySeveralRolesInGroupTests != that.allowOneCompanyPlaySeveralRolesInGroupTests) {
         return false;
      }
      if (allowReportDownload != that.allowReportDownload) {
         return false;
      }
      if (allowReportDownload != that.allowReportDownload) {
         return false;
      }
      if (allowParticipantsStartGroupTests != that.allowParticipantsStartGroupTests) {
         return false;
      }
      if (isCriticalStatusEnabled != that.isCriticalStatusEnabled) {
         return false;
      }
      if (isProxyUseEnabled != that.isProxyUseEnabled) {
         return false;
      }
      if (contractRequired != that.contractRequired) {
         return false;
      }
      if (name != null ? !name.equals(that.name) : that.name != null) {
         return false;
      }
      if (internetTesting != null ? !internetTesting.equals(that.internetTesting) : that.internetTesting != null) {
         return false;
      }
      if (allowParticipantRegistration != null ? !allowParticipantRegistration.equals(
            that.allowParticipantRegistration) : that
            .allowParticipantRegistration != null) {
         return false;
      }
      if (description != null ? !description.equals(that.description) : that.description != null) {
         return false;
      }
      if (beginningSession != null ? !beginningSession.equals(that.beginningSession) : that.beginningSession != null) {
         return false;
      }
      if (endingSession != null ? !endingSession.equals(that.endingSession) : that.endingSession != null) {
         return false;
      }
      if (registrationDeadlineDate != null ? !registrationDeadlineDate.equals(
            that.registrationDeadlineDate) : that.registrationDeadlineDate !=
            null) {
         return false;
      }
      if (currency != null ? !currency.equals(that.currency) : that.currency != null) {
         return false;
      }
      if (invoicePaymentDeadline != null ? !invoicePaymentDeadline.equals(
            that.invoicePaymentDeadline) : that.invoicePaymentDeadline != null) {
         return false;
      }
      if (activeSession != null ? !activeSession.equals(that.activeSession) : that.activeSession != null) {
         return false;
      }
      if (hiddenSession != null ? !hiddenSession.equals(that.hiddenSession) : that.hiddenSession != null) {
         return false;
      }
      if (disableAutoResults != null ? !disableAutoResults.equals(
            that.disableAutoResults) : that.disableAutoResults != null) {
         return false;
      }
      if (disablePatientGenerationAndSharing != null ? !disablePatientGenerationAndSharing.equals(
            that.disablePatientGenerationAndSharing) : that
            .disablePatientGenerationAndSharing != null) {
         return false;
      }
      if (orderInGUI != null ? !orderInGUI.equals(that.orderInGUI) : that.orderInGUI != null) {
         return false;
      }
      if (mailingListUrl != null ? !mailingListUrl.equals(that.mailingListUrl) : that.mailingListUrl != null) {
         return false;
      }
      if (wikiUrl != null ? !wikiUrl.equals(that.wikiUrl) : that.wikiUrl != null) {
         return false;
      }
      if (certificatesUrl != null ? !certificatesUrl.equals(that.certificatesUrl) : that.certificatesUrl != null) {
         return false;
      }
      if (contactLastname != null ? !contactLastname.equals(that.contactLastname) : that.contactLastname != null) {
         return false;
      }
      if (contactFirstname != null ? !contactFirstname.equals(that.contactFirstname) : that.contactFirstname != null) {
         return false;
      }
      if (contactEmail != null ? !contactEmail.equals(that.contactEmail) : that.contactEmail != null) {
         return false;
      }
      if (configurationOverview != null ? !configurationOverview.equals(
            that.configurationOverview) : that.configurationOverview != null) {
         return false;
      }
      if (feeFirstSystem != null ? !feeFirstSystem.equals(that.feeFirstSystem) : that.feeFirstSystem != null) {
         return false;
      }
      if (feeAdditionalSystem != null ? !feeAdditionalSystem.equals(
            that.feeAdditionalSystem) : that.feeAdditionalSystem != null) {
         return false;
      }
      if (feeParticipant != null ? !feeParticipant.equals(that.feeParticipant) : that.feeParticipant != null) {
         return false;
      }
      if (domainFee != null ? !domainFee.equals(that.domainFee) : that.domainFee != null) {
         return false;
      }
      if (vatPercent != null ? !vatPercent.equals(that.vatPercent) : that.vatPercent != null) {
         return false;
      }
      if (pathToContractTemplate != null ? !pathToContractTemplate.equals(
            that.pathToContractTemplate) : that.pathToContractTemplate != null) {
         return false;
      }
      if (pathToInvoiceTemplate != null ? !pathToInvoiceTemplate.equals(
            that.pathToInvoiceTemplate) : that.pathToInvoiceTemplate != null) {
         return false;
      }
      if (nextInvoiceNumber != null ? !nextInvoiceNumber.equals(
            that.nextInvoiceNumber) : that.nextInvoiceNumber != null) {
         return false;
      }
      if (nbOfTestInstancesClaimedByQRCode != null ? !nbOfTestInstancesClaimedByQRCode.equals(
            that.nbOfTestInstancesClaimedByQRCode) : that
            .nbOfTestInstancesClaimedByQRCode != null) {
         return false;
      }
      if (nbOfTestInstancesVerifiedOnSmartphone != null ? !nbOfTestInstancesVerifiedOnSmartphone.equals(that
            .nbOfTestInstancesVerifiedOnSmartphone) : that.nbOfTestInstancesVerifiedOnSmartphone != null) {
         return false;
      }
      if (nbOfTestInstancesAccessedByQRCode != null ? !nbOfTestInstancesAccessedByQRCode.equals(
            that.nbOfTestInstancesAccessedByQRCode) : that
            .nbOfTestInstancesAccessedByQRCode != null) {
         return false;
      }
      if (logoUrl != null ? !logoUrl.equals(that.logoUrl) : that.logoUrl != null) {
         return false;
      }
      if (logoLinkUrl != null ? !logoLinkUrl.equals(that.logoLinkUrl) : that.logoLinkUrl != null) {
         return false;
      }
      if (iso3VATCountries != null ? !iso3VATCountries.equals(that.iso3VATCountries) : that.iso3VATCountries != null) {
         return false;
      }
      if (sessionClosed != null ? !sessionClosed.equals(that.sessionClosed) : that.sessionClosed != null) {
         return false;
      }
      if (resultsCompute != null ? !resultsCompute.equals(that.resultsCompute) : that.resultsCompute != null) {
         return false;
      }
      if (enablePrecatTests != null ? !enablePrecatTests.equals(
            that.enablePrecatTests) : that.enablePrecatTests != null) {
         return false;
      }
      if (targetColor != null ? !targetColor.equals(that.targetColor) : that.targetColor != null) {
         return false;
      }
      if (nbParticipantsIncludedInSystemFees != null ? !nbParticipantsIncludedInSystemFees.equals(
            that.nbParticipantsIncludedInSystemFees) : that
            .nbParticipantsIncludedInSystemFees != null) {
         return false;
      }
      if (networkConfiguration != null ? !networkConfiguration.equals(
            that.networkConfiguration) : that.networkConfiguration != null) {
         return false;
      }
      if (lastChanged != null ? !lastChanged.equals(that.lastChanged) : that.lastChanged != null) {
         return false;
      }
      if (lastModifierId != null ? !lastModifierId.equals(that.lastModifierId) : that.lastModifierId != null) {
         return false;
      }
      return isDefaultTestingSession != null ? isDefaultTestingSession.equals(
            that.isDefaultTestingSession) : that.isDefaultTestingSession == null;
   }

   @Override
   public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + (name != null ? name.hashCode() : 0);
      result = 31 * result + (internetTesting != null ? internetTesting.hashCode() : 0);
      result = 31 * result + (allowParticipantRegistration != null ? allowParticipantRegistration.hashCode() : 0);
      result = 31 * result + (description != null ? description.hashCode() : 0);
      result = 31 * result + (beginningSession != null ? beginningSession.hashCode() : 0);
      result = 31 * result + (endingSession != null ? endingSession.hashCode() : 0);
      result = 31 * result + (registrationDeadlineDate != null ? registrationDeadlineDate.hashCode() : 0);
      result = 31 * result + (currency != null ? currency.hashCode() : 0);
      result = 31 * result + (invoicePaymentDeadline != null ? invoicePaymentDeadline.hashCode() : 0);
      result = 31 * result + (activeSession != null ? activeSession.hashCode() : 0);
      result = 31 * result + (hiddenSession != null ? hiddenSession.hashCode() : 0);
      result = 31 * result + (disableAutoResults != null ? disableAutoResults.hashCode() : 0);
      result = 31 * result + (disablePatientGenerationAndSharing != null ?
            disablePatientGenerationAndSharing.hashCode() : 0);
      result = 31 * result + (orderInGUI != null ? orderInGUI.hashCode() : 0);
      result = 31 * result + (mailingListUrl != null ? mailingListUrl.hashCode() : 0);
      result = 31 * result + (wikiUrl != null ? wikiUrl.hashCode() : 0);
      result = 31 * result + (certificatesUrl != null ? certificatesUrl.hashCode() : 0);
      result = 31 * result + (displayCertificatesMenu ? 1 : 0);
      result = 31 * result + (allowOneCompanyPlaySeveralRolesInP2PTests ? 1 : 0);
      result = 31 * result + (allowOneCompanyPlaySeveralRolesInGroupTests ? 1 : 0);
      result = 31 * result + (allowReportDownload ? 1 : 0);
      result = 31 * result + (allowParticipantsStartGroupTests ? 1 : 0);
      result = 31 * result + (isCriticalStatusEnabled ? 1 : 0);
      result = 31 * result + (isProxyUseEnabled ? 1 : 0);
      result = 31 * result + (contactLastname != null ? contactLastname.hashCode() : 0);
      result = 31 * result + (contactFirstname != null ? contactFirstname.hashCode() : 0);
      result = 31 * result + (contactEmail != null ? contactEmail.hashCode() : 0);
      result = 31 * result + (configurationOverview != null ? configurationOverview.hashCode() : 0);
      result = 31 * result + (feeFirstSystem != null ? feeFirstSystem.hashCode() : 0);
      result = 31 * result + (feeAdditionalSystem != null ? feeAdditionalSystem.hashCode() : 0);
      result = 31 * result + (feeParticipant != null ? feeParticipant.hashCode() : 0);
      result = 31 * result + (domainFee != null ? domainFee.hashCode() : 0);
      result = 31 * result + (vatPercent != null ? vatPercent.hashCode() : 0);
      result = 31 * result + (contractRequired ? 1 : 0);
      result = 31 * result + (pathToContractTemplate != null ? pathToContractTemplate.hashCode() : 0);
      result = 31 * result + (pathToInvoiceTemplate != null ? pathToInvoiceTemplate.hashCode() : 0);
      result = 31 * result + (nextInvoiceNumber != null ? nextInvoiceNumber.hashCode() : 0);
      result = 31 * result + (nbOfTestInstancesClaimedByQRCode != null ? nbOfTestInstancesClaimedByQRCode.hashCode()
            : 0);
      result = 31 * result + (nbOfTestInstancesVerifiedOnSmartphone != null ?
            nbOfTestInstancesVerifiedOnSmartphone.hashCode() : 0);
      result = 31 * result + (nbOfTestInstancesAccessedByQRCode != null ?
            nbOfTestInstancesAccessedByQRCode.hashCode() : 0);
      result = 31 * result + (logoUrl != null ? logoUrl.hashCode() : 0);
      result = 31 * result + (logoLinkUrl != null ? logoLinkUrl.hashCode() : 0);
      result = 31 * result + (iso3VATCountries != null ? iso3VATCountries.hashCode() : 0);
      result = 31 * result + (sessionClosed != null ? sessionClosed.hashCode() : 0);
      result = 31 * result + (resultsCompute != null ? resultsCompute.hashCode() : 0);
      result = 31 * result + (enablePrecatTests != null ? enablePrecatTests.hashCode() : 0);
      result = 31 * result + (targetColor != null ? targetColor.hashCode() : 0);
      result = 31 * result + (nbParticipantsIncludedInSystemFees != null ?
            nbParticipantsIncludedInSystemFees.hashCode() : 0);
      result = 31 * result + (networkConfiguration != null ? networkConfiguration.hashCode() : 0);
      result = 31 * result + (isDefaultTestingSession != null ? isDefaultTestingSession.hashCode() : 0);
      result = 31 * result + (lastChanged != null ? lastChanged.hashCode() : 0);
      result = 31 * result + (lastModifierId != null ? lastModifierId.hashCode() : 0);
      return result;
   }
}
