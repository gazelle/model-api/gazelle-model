/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.gazelletest.model.definition.MetaTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.systems.model.SystemActorProfiles;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.apache.commons.lang.WordUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

@Entity
@Table(name = "tm_system_aipo_result_for_a_testing_session", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_actor_profile_id", "testing_session_id"}))
@SequenceGenerator(name = "tm_system_aipo_result_for_a_testing_session_sequence", sequenceName = "tm_system_aipo_result_for_a_testing_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SystemAIPOResultForATestingSession extends AuditedObject implements Serializable {

    public static final int MAX_LENGHT_WORDWRAP = 60;
    private static final long serialVersionUID = -207532312821787L;
    private static Logger LOG = LoggerFactory.getLogger(SystemAIPOResultForATestingSession.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_aipo_result_for_a_testing_session_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "system_actor_profile_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private SystemActorProfiles systemActorProfile;

    @ManyToOne
    @JoinColumn(name = "testing_session_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testSession;

    @ManyToOne
    @JoinColumn(name = "status_id")
    @Fetch(value = FetchMode.SELECT)
    private StatusResults status;

    @Column(name = "comment")
    private String comment;

    @Column(name = "enabled")
    private Boolean enabled;

    @Lob
    @Type(type = "text")
    private String htmlCountVerified;

    @Lob
    @Type(type = "text")
    private String htmlCountWaiting;

    @Lob
    @Type(type = "text")
    private String htmlCountProgress;

    @Lob
    @Type(type = "text")
    private String htmlCountFailed;

    @Transient
    private boolean isDependencyMissing = false;

    private Integer testRolesCountR;

    private Integer testRolesCountO;

    private Integer indicatorVerified;

    private Integer indicatorProgress;

    private Integer indicatorWaiting;

    private Integer indicatorFailed;

    @OneToMany(mappedBy = "systemAIPOResultForATestingSession", fetch = FetchType.LAZY)
    private List<SAPResultTestRoleComment> comments;
    private transient Map<Object, Map<Status, Integer>> counters;

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public SystemAIPOResultForATestingSession() {
        super();
    }

    public static void deleteSystemAIPOResult(SystemActorProfiles inSystemActorProfiles) {
        EntityManager em = EntityManagerService.provideEntityManager();

        List<SystemAIPOResultForATestingSession> results = inSystemActorProfiles.getResults();
        for (SystemAIPOResultForATestingSession saipoResultForATestingSession : results) {
            try {
                List<SAPResultTestRoleComment> comments = saipoResultForATestingSession.getComments();
                for (SAPResultTestRoleComment sapResultTestRoleComment : comments) {
                    em.remove(sapResultTestRoleComment);
                }
                em.remove(saipoResultForATestingSession);
            } catch (Exception e) {
                LOG.error("deleteSystemAIPOResult", e);
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemActorProfiles getSystemActorProfile() {
        return systemActorProfile;
    }

    public void setSystemActorProfile(SystemActorProfiles systemActorProfile) {
        this.systemActorProfile = systemActorProfile;
    }

    public TestingSession getTestSession() {
        return testSession;
    }

    public void setTestSession(TestingSession testSession) {
        this.testSession = testSession;
    }

    public StatusResults getStatus() {
        return status;
    }

    public void setStatus(StatusResults status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getWordWrappedComment() {
        if (comment == null) {
            return "";
        }
        return WordUtils.wrap(comment, MAX_LENGHT_WORDWRAP);
    }

    public boolean getEnabled() {
        if (enabled == null) {
            return true;
        }
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getTestRolesCountR() {
        return testRolesCountR;
    }

    public void setTestRolesCountR(Integer testRolesCountR) {
        this.testRolesCountR = testRolesCountR;
    }

    public Integer getTestRolesCountO() {
        return testRolesCountO;
    }

    public void setTestRolesCountO(Integer testRolesCountO) {
        this.testRolesCountO = testRolesCountO;
    }

    public String getIndicatorVerified() {
        if (indicatorVerified == -1) {
            return "?";
        } else {
            return indicatorVerified + "%";
        }
    }

    public void setIndicatorVerified(Integer indicatorVerified) {
        this.indicatorVerified = indicatorVerified;
    }

    public String getIndicatorProgress() {
        if (indicatorProgress == -1) {
            return "?";
        } else {
            return indicatorProgress + "%";
        }
    }

    public void setIndicatorProgress(Integer indicatorProgress) {
        this.indicatorProgress = indicatorProgress;
    }

    public String getIndicatorWaiting() {
        if (indicatorWaiting == -1) {
            return "?";
        } else {
            return indicatorWaiting + "%";
        }
    }

    public void setIndicatorWaiting(Integer indicatorWaiting) {
        this.indicatorWaiting = indicatorWaiting;
    }

    public String getIndicatorFailed() {
        if (indicatorFailed == -1) {
            return "?";
        } else {
            return indicatorFailed + "%";
        }
    }

    public void setIndicatorFailed(Integer indicatorFailed) {
        this.indicatorFailed = indicatorFailed;
    }

    public String getHtmlCountVerified() {
        return htmlCountVerified;
    }

    public void setHtmlCountVerified(String htmlCountVerified) {
        this.htmlCountVerified = htmlCountVerified;
    }

    public String getHtmlCountWaiting() {
        return htmlCountWaiting;
    }

    public void setHtmlCountWaiting(String htmlCountWaiting) {
        this.htmlCountWaiting = htmlCountWaiting;
    }

    public String getHtmlCountProgress() {
        return htmlCountProgress;
    }

    public void setHtmlCountProgress(String htmlCountProgress) {
        this.htmlCountProgress = htmlCountProgress;
    }

    public String getHtmlCountFailed() {
        return htmlCountFailed;
    }

    public void setHtmlCountFailed(String htmlCountFailed) {
        this.htmlCountFailed = htmlCountFailed;
    }

    // ////////////////************* End of getters /Setters

    public List<SAPResultTestRoleComment> getComments() {
        return comments;
    }

    public void setComments(List<SAPResultTestRoleComment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SystemAIPOResultForATestingSession [systemActorProfile=");
        builder.append(systemActorProfile);
        builder.append(", status=");
        builder.append(status);
        builder.append(", indicatorVerified=");
        builder.append(indicatorVerified);
        builder.append(", indicatorProgress=");
        builder.append(indicatorProgress);
        builder.append(", indicatorWaiting=");
        builder.append(indicatorWaiting);
        builder.append(", indicatorFailed=");
        builder.append(indicatorFailed);
        builder.append("]");
        return builder.toString();
    }

    public void resetCounters() {
        counters = new HashMap<Object, Map<Status, Integer>>();
    }

    public void addCounterValues(TestRoles testRole, Map<Status, Integer> subCounters) {
        MetaTest metaTest = testRole.getMetaTest();
        Object key = null;
        if (metaTest != null) {
            key = metaTest;
        } else {
            key = testRole;
        }

        Map<Status, Integer> counter = counters.get(key);
        if (counter == null) {
            counter = new HashMap<Status, Integer>();
            counters.put(key, counter);
        }

        Set<Entry<Status, Integer>> entrySet = subCounters.entrySet();
        for (Entry<Status, Integer> entry : entrySet) {
            Integer value = counter.get(entry.getKey());
            if (value == null) {
                value = 0;
            }
            if (entry.getValue() != null) {
                value = value + entry.getValue();
            }
            counter.put(entry.getKey(), value);
        }
    }

    public boolean isDependencyMissing() {
        return isDependencyMissing;
    }

    public void setDependencyMissing(boolean dependencyMissing) {
        isDependencyMissing = dependencyMissing;
    }

    public void updateIndicators() {
        List<Fraction> fractionsVerified = new ArrayList<SystemAIPOResultForATestingSession.Fraction>();
        List<Fraction> fractionsWaiting = new ArrayList<SystemAIPOResultForATestingSession.Fraction>();
        List<Fraction> fractionsProgress = new ArrayList<SystemAIPOResultForATestingSession.Fraction>();
        List<Fraction> fractionsFailed = new ArrayList<SystemAIPOResultForATestingSession.Fraction>();
        List<Fraction>[] fractionsArray = new List[4];
        fractionsArray[0] = fractionsVerified;
        fractionsArray[1] = fractionsWaiting;
        fractionsArray[2] = fractionsProgress;
        fractionsArray[3] = fractionsFailed;

        int testRolesCountR = 0;
        int testRolesCountO = 0;

        float[][] sumTotal = new float[2][4];
        int[][] countTotal = new int[2][4];

        for (int i = 0; i < sumTotal.length; i++) {
            for (int j = 0; j < sumTotal[i].length; j++) {
                sumTotal[i][j] = 0.0f;
                countTotal[i][j] = 0;
            }
        }

        Set<Object> keySet = counters.keySet();
        for (Object key : keySet) {
            if (key != null) {
                float numberOfTestsToRealize = 0;
                // 0 = required , 1 = optional
                int index = 0;

                String name = "";
                if (key instanceof MetaTest) {
                    MetaTest metaTest = (MetaTest) key;
                    name = "MT " + metaTest.getKeyword();
                    List<TestRoles> testRoles = metaTest.getTestRolesList();
                    for (TestRoles testRole : testRoles) {
                        Integer numberOfTestsToRealize2 = testRole.getNumberOfTestsToRealize();
                        if (numberOfTestsToRealize2 == null) {
                            numberOfTestsToRealize2 = 0;
                        }
                        numberOfTestsToRealize = Math.max(numberOfTestsToRealize, numberOfTestsToRealize2);
                    }
                    index = 0;
                } else {
                    TestRoles testRole = (TestRoles) key;
                    name = testRole.getTest().getKeyword() + "/" + testRole.getRoleInTest().getKeyword();
                    if (testRole.getNumberOfTestsToRealize() == null) {
                        numberOfTestsToRealize = 0;
                    } else {
                        numberOfTestsToRealize = testRole.getNumberOfTestsToRealize();
                    }
                    if (!testRole.getTestOption().getKeyword().equals("O")) {
                        index = 0;
                    } else {
                        index = 1;
                    }
                }

                if (index == 0) {
                    testRolesCountR++;
                } else {
                    testRolesCountO++;
                }

                if (numberOfTestsToRealize > 0) {
                    Map<Status, Integer> counter = counters.get(key);

                    float started = counter.get(Status.STARTED) == null ? 0.0f : counter.get(Status.STARTED);
                    float completed = counter.get(Status.COMPLETED) == null ? 0.0f : counter.get(Status.COMPLETED);
                    float paused = counter.get(Status.PAUSED) == null ? 0.0f : counter.get(Status.PAUSED);
                    float verified = counter.get(Status.VERIFIED) == null ? 0.0f : counter.get(Status.VERIFIED);
                    float part_verified = counter.get(Status.PARTIALLY_VERIFIED) == null ? 0.0f : counter
                            .get(Status.PARTIALLY_VERIFIED);
                    float failed = counter.get(Status.FAILED) == null ? 0.0f : counter.get(Status.FAILED);
                    float critical = counter.get(Status.CRITICAL) == null ? 0.0f : counter.get(Status.CRITICAL);

                    fractionsVerified.add(new Fraction(name, index == 1, Math.round(verified), Math
                            .round(numberOfTestsToRealize)));

                    // 1 / Verified
                    float verifiedRatio = Math.max(0.0f, Math.min(1.0f, verified / numberOfTestsToRealize));
                    sumTotal[index][0] += verifiedRatio;
                    countTotal[index][0] += 1;

                    // 2 / Waiting
                    if (verified < numberOfTestsToRealize) {
                        fractionsWaiting.add(new Fraction(name, index == 1, Math.round(completed + critical), Math
                                .round(numberOfTestsToRealize - verified)));
                        float waitingRatio = Math.max(0.0f,
                                Math.min(1.0f, (completed + critical) / (numberOfTestsToRealize - verified)));
                        sumTotal[index][1] += waitingRatio;
                        countTotal[index][1] += 1;

                        // 3 / Progress
                        if ((verified + completed + critical) < numberOfTestsToRealize) {
                            fractionsProgress.add(new Fraction(name, index == 1, Math.round(started + paused
                                    + part_verified), Math.round(numberOfTestsToRealize - verified - completed
                                    - critical)));
                            float progressRatio = Math.max(
                                    0.0f,
                                    Math.min(1.0f, (started + paused + part_verified)
                                            / (numberOfTestsToRealize - verified - completed - critical)));
                            sumTotal[index][2] += progressRatio;
                            countTotal[index][2] += 1;
                        }
                    }

                    // 4 / Failed
                    fractionsFailed.add(new Fraction(name, index == 1, Math.round(failed), Math
                            .round(numberOfTestsToRealize)));
                    float failedRatio = failed / numberOfTestsToRealize;
                    sumTotal[index][3] += failedRatio;
                    countTotal[index][3] += 1;
                }
            }
        }

        for (int j = 0; j < 4; j++) {
            List<Fraction> fractions = fractionsArray[j];
            StringBuilder sb = new StringBuilder();
            Collections.sort(fractions);
            for (Fraction fraction : fractions) {
                fraction.append(sb);
            }

            switch (j) {
                case 0:
                    setHtmlCountVerified(sb.toString());
                    break;
                case 1:
                    setHtmlCountWaiting(sb.toString());
                    break;
                case 2:
                    setHtmlCountProgress(sb.toString());
                    break;
                case 3:
                    setHtmlCountFailed(sb.toString());
                    break;
                default:
                    break;
            }
        }

        setTestRolesCountR(testRolesCountR);
        setTestRolesCountO(testRolesCountO);

        for (int j = 0; j < 4; j++) {
            int count = countTotal[0][j];
            float value = sumTotal[0][j];
            if (testRolesCountR == 0) {
                count = countTotal[1][j];
                value = sumTotal[1][j];
            }

            int result = -1;
            if (count != 0) {
                result = Math.round((100.0f * value) / count);
            }

            switch (j) {
                case 0:
                    setIndicatorVerified(result);
                    break;
                case 1:
                    setIndicatorWaiting(result);
                    break;
                case 2:
                    setIndicatorProgress(result);
                    break;
                case 3:
                    setIndicatorFailed(result);
                    break;
                default:
                    break;
            }
        }

    }

    // ********///////////////////////////////

    private class Fraction implements Comparable<Fraction> {

        private boolean isOptionnal;

        private int verified;

        private int ntests;

        private float val;

        private String test;

        public Fraction(String test, boolean isOptionnal, int verified, int ntests) {
            super();
            this.isOptionnal = isOptionnal;
            this.verified = verified;
            this.ntests = ntests;
            this.test = test;
            if (ntests != 0) {
                val = ((float) verified) / ((float) ntests);
            } else {
                val = 2.0f;
            }
        }

        public void append(StringBuilder sb) {
            if (!isOptionnal) {
                sb.append("<b>");
            }
            sb.append("<sup>").append(Math.round(verified)).append("</sup>&frasl;<sub>").append(Math.round(ntests));
            if (!isOptionnal) {
                sb.append("</b>");
            }
            sb.append("</sub>&nbsp;");
            sb.append(test);
            sb.append("<br />");
        }

        @Override
        public int compareTo(Fraction o) {
            if (isOptionnal && !o.isOptionnal) {
                return 1;
            }
            if (!isOptionnal && o.isOptionnal) {
                return -1;
            }
            if (val < o.val) {
                return 1;
            } else {
                if (val > o.val) {
                    return -1;
                }
            }
            return 0;
        }

    }

}
