/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

/**
 * @class AbstractConfiguration.java
 * @package net.ihe.gazelle.tm.systems.model
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.csv.CSVExportable;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.configurations.model.DICOM.*;
import net.ihe.gazelle.tm.configurations.model.HL7.*;
import net.ihe.gazelle.tm.configurations.model.interfaces.HTTPConfiguration;
import net.ihe.gazelle.tm.configurations.model.interfaces.ServerConfiguration;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.translate.TranslateService;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.util.DatabaseUtil;
import net.ihe.gazelle.util.Pair;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "tm_abstract_configuration", schema = "public")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class AbstractConfiguration extends AuditedObject
        implements Serializable, CSVExportable, Comparable<AbstractConfiguration> {

    private static final long serialVersionUID = 8551333427051277205L;

    private static Logger log = LoggerFactory.getLogger(AbstractConfiguration.class);
    @Id
    @Column(name = "id", nullable = false, unique = true)
    protected Integer id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "configuration_id")
    @NotNull
    protected Configuration configuration;
    @Column(name = "comments", nullable = true)
    protected String comments;
    @Transient
    private int proxyPortsErrors = 0;

    public AbstractConfiguration() {
        super();
    }

    public AbstractConfiguration(Configuration inConfiguration) {
        configuration = inConfiguration;
        if (configuration == null) {
            configuration = new Configuration();
        }
        try {
            configuration.setConfigurationType(ConfigurationType.GetConfigurationTypeByClass(this.getClass()));
        } catch (ClassNotFoundException e) {
            log.error("ClassNotFoundException", e);
        }
    }

    public AbstractConfiguration(Configuration inConfiguration, String inComments) {
        comments = inComments;
        configuration = inConfiguration;
        try {
            configuration.setConfigurationType(ConfigurationType.GetConfigurationTypeByClass(this.getClass()));
        } catch (ClassNotFoundException e) {
            log.error("ClassNotFoundException", e);
        }
    }

    public AbstractConfiguration(AbstractConfiguration abstractConfiguration) {
        if (abstractConfiguration != null) {
            this.comments = abstractConfiguration.comments;
            this.configuration = new Configuration(abstractConfiguration.configuration);
        }
    }

    public static boolean classExtends(Class<?> testEntityClass, Class<?> entityClass) {
        if (testEntityClass == null) {
            return false;
        }
        if (testEntityClass.equals(entityClass)) {
            return true;
        }
        Class<?> superclass = testEntityClass.getSuperclass();
        return classExtends(superclass, entityClass);
    }

    public static boolean classImplements(Class<?> testEntityClass, Class<?> interfaceClass) {
        if (testEntityClass == null) {
            return false;
        }
        if (Arrays.asList(testEntityClass.getInterfaces()).contains(interfaceClass)) {
            return true;
        }
        Class<?> superclass = testEntityClass.getSuperclass();
        return classImplements(superclass, interfaceClass);
    }

    public static String getHeader1(Class<?> configurationClass) {
        String key = "";
        if (classExtends(configurationClass, AbstractDicomConfiguration.class)) {
            key = "AE Title";
        } else if (classExtends(configurationClass, HL7V2InitiatorConfiguration.class)) {
            key = "Sending application / facility";
        } else if (classExtends(configurationClass, HL7V2ResponderConfiguration.class)) {
            key = "Receiving application / facility";
        } else if (classExtends(configurationClass, HL7V3InitiatorConfiguration.class)) {
            key = "";
        } else if (classExtends(configurationClass, HL7V3ResponderConfiguration.class)) {
            key = "WSType";
        } else if (classExtends(configurationClass, SyslogConfiguration.class)) {
            key = "Transport";
        } else if (classExtends(configurationClass, WebServiceConfiguration.class)) {
            key = "WSType";
        } else if (classExtends(configurationClass, RawConfiguration.class)) {
            key = "Transaction description";
        }
        return TranslateService.getTranslation(key);
    }

    public static String getHeader2(Class<?> configurationClass) {
        String key = "";
        if (classExtends(configurationClass, AbstractDicomConfiguration.class)) {
            key = "SOP class";
        } else if (classExtends(configurationClass, AbstractHL7Configuration.class)) {
            key = "Namespace";
        } else if (classExtends(configurationClass, SyslogConfiguration.class)) {
            key = "Protocol";
        } else if (classExtends(configurationClass, WebServiceConfiguration.class)) {
            key = "Http rewrite";
        }

        return TranslateService.getTranslation(key);
    }

    public static String getHeaderUrl(Class<?> configurationClass) {
        String key = "";
        if (classExtends(configurationClass, HL7V3ResponderConfiguration.class)) {
            key = "URL";
        } else if (classExtends(configurationClass, WebServiceConfiguration.class)) {
            key = "URL";
        }
        return TranslateService.getTranslation(key);
    }

    public static String getTypeLabel(Class<?> configurationClass) {
        String key = "";
        if (classExtends(configurationClass, DicomSCUConfiguration.class)) {
            key = "Dicom SCU";
        } else if (classExtends(configurationClass, DicomSCPConfiguration.class)) {
            key = "Dicom SCP";
        } else if (classExtends(configurationClass, HL7V2InitiatorConfiguration.class)) {
            key = "HL7 V2 Initiator";
        } else if (classExtends(configurationClass, HL7V2ResponderConfiguration.class)) {
            key = "HL7 V2 Responder";
        } else if (classExtends(configurationClass, HL7V3InitiatorConfiguration.class)) {
            key = "HL7 V3 Initiator";
        } else if (classExtends(configurationClass, HL7V3ResponderConfiguration.class)) {
            key = "HL7 V3 Responder";
        } else if (classExtends(configurationClass, SyslogConfiguration.class)) {
            key = "Syslog";
        } else if (classExtends(configurationClass, WebServiceConfiguration.class)) {
            key = "Webservice";
        } else if (classExtends(configurationClass, RawConfiguration.class)) {
            key = "RAW";
        }
        return TranslateService.getTranslation(key);
    }

    /**
     * Allows to getAllConfiguration depending the parameterName given in parameter. For that we use the class Class ( use of reflection ) to get the name of the attribute. As the attribute is usually
     * private and in a Parent class, we look for the getter instead of the field
     *
     * @param inTestingSession
     * @param parameterName
     *
     * @return
     */
    private static List<AbstractConfiguration> GetListOfConfigurationWithSpecificParameter(
            TestingSession inTestingSession, String parameterName) {
        if ((parameterName == null) || (parameterName.trim().length() == 0)) {
            return null;
        }
        if (inTestingSession == null) {
            return null;
        }

        List<ConfigurationType> confTypeList = ConfigurationType.GetAllConfigurationType();

        List<AbstractConfiguration> listOfResult = new ArrayList<AbstractConfiguration>();
        for (ConfigurationType confType : confTypeList) {
            if (confType.getClassName().equals("net.ihe.gazelle.tm.configurations.model.DICOM.DicomSCUConfiguration")) {
                DicomSCUConfigurationQuery q = new DicomSCUConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            } else if (confType.getClassName()
                    .equals("net.ihe.gazelle.tm.configurations.model.DICOM.DicomSCPConfiguration")) {
                DicomSCPConfigurationQuery q = new DicomSCPConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            } else if (confType.getClassName()
                    .equals("net.ihe.gazelle.tm.configurations.model.WebServiceConfiguration")) {
                WebServiceConfigurationQuery q = new WebServiceConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            } else if (confType.getClassName()
                    .equals("net.ihe.gazelle.tm.configurations.model.HL7.HL7V3ResponderConfiguration")) {
                HL7V3ResponderConfigurationQuery q = new HL7V3ResponderConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            } else if (confType.getClassName()
                    .equals("net.ihe.gazelle.tm.configurations.model.HL7.HL7V2ResponderConfiguration")) {
                HL7V2ResponderConfigurationQuery q = new HL7V2ResponderConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            } else if (confType.getClassName().equals("net.ihe.gazelle.tm.configurations.model.SyslogConfiguration")) {
                SyslogConfigurationQuery q = new SyslogConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            } else if (confType.getClassName().equals("net.ihe.gazelle.tm.configurations.model.RawConfiguration")) {
                RawConfigurationQuery q = new RawConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfResult.addAll(q.getList());
            }
        }

        return listOfResult;
    }

    @SuppressWarnings("unchecked")
    private static List<Class<?>> GetListOfConfigurationClassWithSpecificParameter(String parameterName) {
        if ((parameterName == null) || (parameterName.trim().length() == 0)) {
            return null;
        }

        List<ConfigurationType> confTypeList = ConfigurationType.GetAllConfigurationType();
        String getterFromParameter = "get" + parameterName.substring(0, 1).toUpperCase(Locale.getDefault()) + (parameterName.length() > 1 ?
                parameterName.substring(1) :
                "");

        List<Class<?>> listOfResult = new ArrayList<Class<?>>();
        for (ConfigurationType confType : confTypeList) {
            try {
                new StringBuffer();
                Class<?> class_ = Class.forName(confType.getClassName());

                class_.getMethod(getterFromParameter);

                listOfResult.add(class_);
            } catch (ClassNotFoundException e) {

            } catch (SecurityException e) {

            } catch (NoSuchMethodException e) {

            }
        }

        return listOfResult;
    }

    public static List<AbstractConfiguration> GetListOfConfiguration(TestingSession inTestingSession,
                                                                     Class<?>... classes) {
        if ((classes == null) || (classes.length == 0)) {
            return null;
        }
        if (inTestingSession == null) {
            return null;
        }

        List<AbstractConfiguration> listOfConfigurations = new ArrayList<AbstractConfiguration>();
        for (Class<?> class_ : classes) {
            if (class_.equals(DicomSCUConfiguration.class)) {
                DicomSCUConfigurationQuery q = new DicomSCUConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(DicomSCPConfiguration.class)) {
                DicomSCPConfigurationQuery q = new DicomSCPConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(WebServiceConfiguration.class)) {
                WebServiceConfigurationQuery q = new WebServiceConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V3ResponderConfiguration.class)) {
                HL7V3ResponderConfigurationQuery q = new HL7V3ResponderConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V2ResponderConfiguration.class)) {
                HL7V2ResponderConfigurationQuery q = new HL7V2ResponderConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(SyslogConfiguration.class)) {
                SyslogConfigurationQuery q = new SyslogConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(RawConfiguration.class)) {
                RawConfigurationQuery q = new RawConfigurationQuery();
                q.configuration().systemInSession().testingSession().eq(inTestingSession);
                listOfConfigurations.addAll(q.getList());
            }
        }

        return listOfConfigurations;
    }

    public static List<AbstractConfiguration> listAllConfigurationsForAnActorAndASystem(Actor a,
                                                                                        SystemInSession inSystemInSession, Boolean isApproved) {

        if ((a == null) || (inSystemInSession == null)) {
            return null;
        }
        Class<?>[] configurationTypes = AbstractConfiguration.GetAllClassNameArrayOfConfiguration();

        List<AbstractConfiguration> listToUse = new ArrayList<AbstractConfiguration>();

        for (Class<?> clazz : configurationTypes) {
            List<AbstractConfiguration> tmpList = AbstractConfiguration
                    .getConfigurationsFiltered(clazz, inSystemInSession, null, null, null, null, a, null, null,
                            isApproved);
            if (tmpList != null) {
                listToUse.addAll(tmpList);
            }
        }

        return listToUse;
    }

    public static List<AbstractConfiguration> listAllConfigurationsForATestingSession(
            TestingSession selectedTestingSession) {
        Class<?>[] configurationTypes = AbstractConfiguration.GetAllClassNameArrayOfConfiguration();
        List<AbstractConfiguration> listToUse = new ArrayList<AbstractConfiguration>();

        for (Class<?> clazz : configurationTypes) {
            List<AbstractConfiguration> tmpList = AbstractConfiguration
                    .getConfigurationsFiltered(clazz, null, null, null, selectedTestingSession, null, null, null, null,
                            true);
            if (tmpList != null) {
                listToUse.addAll(tmpList);
            }
        }

        return listToUse;
    }

    public static List<AbstractConfiguration> GetListOfConfiguration(SystemInSession inSystemInSession,
                                                                     Class<?>... classes) {
        if ((classes == null) || (classes.length == 0)) {
            return null;
        }
        if (inSystemInSession == null) {
            return null;
        }

        List<AbstractConfiguration> listOfConfigurations = new ArrayList<AbstractConfiguration>();
        for (Class<?> class_ : classes) {
            if (class_.equals(DicomSCUConfiguration.class)) {
                DicomSCUConfigurationQuery q = new DicomSCUConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(DicomSCPConfiguration.class)) {
                DicomSCPConfigurationQuery q = new DicomSCPConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(WebServiceConfiguration.class)) {
                WebServiceConfigurationQuery q = new WebServiceConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V3ResponderConfiguration.class)) {
                HL7V3ResponderConfigurationQuery q = new HL7V3ResponderConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V2ResponderConfiguration.class)) {
                HL7V2ResponderConfigurationQuery q = new HL7V2ResponderConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(SyslogConfiguration.class)) {
                SyslogConfigurationQuery q = new SyslogConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(RawConfiguration.class)) {
                RawConfigurationQuery q = new RawConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            }
        }

        return listOfConfigurations;
    }

    public static List<AbstractConfiguration> GetListOfAllConfigurationForASystemInSession(
            SystemInSession inSystemInSession) {

        Class<?>[] listOfClasses = GetAllClassNameArrayOfConfiguration();

        List<AbstractConfiguration> listOfConfigurations = new ArrayList<AbstractConfiguration>();
        for (Class<?> class_ : listOfClasses) {
            if (class_.equals(DicomSCUConfiguration.class)) {
                DicomSCUConfigurationQuery q = new DicomSCUConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(DicomSCPConfiguration.class)) {
                DicomSCPConfigurationQuery q = new DicomSCPConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(WebServiceConfiguration.class)) {
                WebServiceConfigurationQuery q = new WebServiceConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V3ResponderConfiguration.class)) {
                HL7V3ResponderConfigurationQuery q = new HL7V3ResponderConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V2ResponderConfiguration.class)) {
                HL7V2ResponderConfigurationQuery q = new HL7V2ResponderConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V3InitiatorConfiguration.class)) {
                HL7V3InitiatorConfigurationQuery q = new HL7V3InitiatorConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(HL7V2InitiatorConfiguration.class)) {
                HL7V2InitiatorConfigurationQuery q = new HL7V2InitiatorConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(SyslogConfiguration.class)) {
                SyslogConfigurationQuery q = new SyslogConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            } else if (class_.equals(RawConfiguration.class)) {
                RawConfigurationQuery q = new RawConfigurationQuery();
                q.configuration().systemInSession().eq(inSystemInSession);
                listOfConfigurations.addAll(q.getList());
            }
        }

        return listOfConfigurations;
    }

    public static List<AbstractConfiguration> GetListOfConfiguration(TestingSession inTestingSession,
                                                                     List<Class<?>> listOfClass) {

        String[] res = listOfClass.toArray(new String[listOfClass.size()]);

        return GetListOfConfiguration(inTestingSession, res.getClass());
    }

    public static Class<?>[] GetAllClassNameArrayOfConfiguration() {

        List<ConfigurationType> confTypeList = ConfigurationType.GetAllConfigurationType();
        Class<?>[] newArrayOfClass = new Class<?>[confTypeList.size()];

        int i = 0;
        for (ConfigurationType confType : confTypeList) {
            try {
                newArrayOfClass[i++] = Class.forName(confType.getClassName());
            } catch (ClassNotFoundException e) {
                log.error("ClassNotFoundException", e);
            }
        }

        return newArrayOfClass;
    }

    public static List<AbstractConfiguration> GetListOfConfigurationWithOIDS(TestingSession inTestingSession) {
        return GetListOfConfigurationWithSpecificParameter(inTestingSession, "assigningAuthorityOID");
    }

    public static List<AbstractConfiguration> GetListOfConfigurationWithPortProxy(TestingSession inTestingSession) {
        return GetListOfConfigurationWithSpecificParameter(inTestingSession, "portProxy");
    }

    public static List<Class<?>> GetListOfClassWithOIDS() {
        return GetListOfConfigurationClassWithSpecificParameter("assigningAuthorityOID");
    }

    public static List<Class<?>> GetListOfClassWithPortProxy() {
        return GetListOfConfigurationClassWithSpecificParameter("portProxy");
    }

    public static List<AbstractConfiguration> getConfigurationsFiltered(Class<?> entityToUse,
                                                                        SystemInSession inSystemInSession, System inSystem, Institution inInstitution,
                                                                        TestingSession inTestingSession, Host inHost, Actor inActor, String inComment, Boolean inIsSecured,
                                                                        Boolean inIsApproved, Pair<String, Object>... parameterNamesWithValues) {
        return getConfigurationsFiltered(entityToUse, inSystemInSession, inSystem, inInstitution, inTestingSession,
                inHost, inActor, null, null, inComment, inIsSecured, inIsApproved, parameterNamesWithValues);
    }

    /**
     * This method SHALL be deleted from TM
     *
     * @param entityToUse
     * @param inSystemInSession
     * @param inSystem
     * @param inInstitution
     * @param inTestingSession
     * @param inHost
     * @param inActor
     * @param inIntegrationProfile
     * @param wsTrans
     * @param inComment
     * @param inIsSecured
     * @param inIsApproved
     * @param parameterNamesWithValues
     *
     * @return
     */
    // TODO replace this method with HQLQueryBuilder attributes
    @Deprecated
    public static List<AbstractConfiguration> getConfigurationsFiltered(Class<?> entityToUse,
                                                                        SystemInSession inSystemInSession, System inSystem, Institution inInstitution,
                                                                        TestingSession inTestingSession, Host inHost, Actor inActor, IntegrationProfile inIntegrationProfile,
                                                                        WSTransactionUsage wsTrans, String inComment, Boolean inIsSecured, Boolean inIsApproved,
                                                                        Pair<String, Object>... parameterNamesWithValues) {

        if (entityToUse == null) {
            return null;
        }

        EntityManager em = EntityManagerService.provideEntityManager();
        Query query;
        StringBuffer queryString = new StringBuffer();
        HashSet<String> prefixes = new HashSet<String>();
        HashSet<String> prefixesUsed = new HashSet<String>();
        HashMap<String, String> mapOfJoin = new HashMap<String, String>();
        HashMap<String, Object> mapOfParameters = new HashMap<String, Object>();

        if (inInstitution != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            prefixes.add("InstitutionSystem instSys");
            queryString
                    .append("value.configuration.systemInSession.system = instSys.system and instSys.institution = :choosenInst");
            mapOfParameters.put("choosenInst", inInstitution);
        }
        if (inSystemInSession != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.systemInSession = :inSystemInSession");
            mapOfParameters.put("inSystemInSession", inSystemInSession);
        }
        if (inSystem != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.systemInSession.system = :inSystem");
            mapOfParameters.put("inSystem", inSystem);
        }
        if (inTestingSession != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.systemInSession.testingSession =:inTestingSession");
            mapOfParameters.put("inTestingSession", inTestingSession);
        }
        if (inHost != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.host = :inHost  ");
            mapOfParameters.put("inHost", inHost);
        }
        if (inActor != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.actor = :inActor  ");
            mapOfParameters.put("inActor", inActor);
        }

        if (wsTrans != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            prefixes.add("ConfigurationTypeWithPortsWSTypeAndSopClass ctsc");
            queryString.append("value.configuration.configurationType=ctsc.configurationType "
                    + "AND ctsc.wsTRansactionUsage=:wsTrans");
            if (entityToUse.equals(HL7V3ResponderConfiguration.class) || entityToUse
                    .equals(HL7V3ResponderConfiguration.class)) {
                queryString.append(" AND value.wsTransactionUsage=:wsTrans");
            }
            mapOfParameters.put("wsTrans", wsTrans);
        }
        if (inIntegrationProfile != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            prefixes.add("ConfigurationTypeMappedWithAIPO ctmwa");
            mapOfJoin.put("ConfigurationTypeMappedWithAIPO ctmwa", "join ctmwa.listOfConfigurationTypes ctwp");
            prefixes.add("ConfigurationType ct");
            queryString.append("value.configuration.configurationType=ct AND ctwp.configurationType=ct "
                    + "AND ctmwa.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile=:inIntegrationProfile ");
//            if (entityToUse.getName().equals("WebServiceConfiguration")){
//			    queryString.append(" AND value.webServiceType.profile=:inIntegrationProfile");
//			}
            mapOfParameters.put("inIntegrationProfile", inIntegrationProfile);
        }
        if (inComment != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.comment = :inComment  ");
            mapOfParameters.put("inComment", inComment);
        }
        if (inIsSecured != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.isSecured = :inIsSecured  ");
            mapOfParameters.put("inIsSecured", inIsSecured);
        }
        if (inIsApproved != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.isApproved = :inIsApproved  ");
            mapOfParameters.put("inIsApproved", inIsApproved);
        }

        if (parameterNamesWithValues != null) {
            for (Pair<String, Object> parameterNameWithValue : parameterNamesWithValues) {
                if (parameterNameWithValue.getObject2() != null) {
                    if ((parameterNameWithValue.getObject1() != null) && (
                            parameterNameWithValue.getObject1().trim().length() > 0)) {
                        String getterFromParameter =
                                "get" + parameterNameWithValue.getObject1().substring(0, 1).toUpperCase(Locale.getDefault()) + (
                                        parameterNameWithValue.getObject1().length() > 1 ?
                                                parameterNameWithValue.getObject1().substring(1) :
                                                "");

                        try {
                            new StringBuffer();
                            Class<?> class_ = entityToUse;

                            class_.getMethod(getterFromParameter);

                            DatabaseUtil.addANDorWHERE(queryString);
                            prefixes.add(entityToUse.getName() + " value");
                            queryString.append("value." + parameterNameWithValue.getObject1() + " = :in"
                                    + parameterNameWithValue.getObject1());
                            mapOfParameters.put("in" + parameterNameWithValue.getObject1(),
                                    parameterNameWithValue.getObject2());
                        } catch (SecurityException e) {
                            log.error("getConfigurationsFiltered", e);
                        } catch (NoSuchMethodException e) {
                            log.error("getConfigurationsFiltered", e);
                        }
                    }
                }
            }
        }

        List<String> listOfPrefixes = new ArrayList<String>(prefixes);

        StringBuffer firstPartOfQuery = new StringBuffer();

        for (int i = 0; i < listOfPrefixes.size(); i++) {
            if (i == 0) {
                firstPartOfQuery.append("SELECT value FROM ");
            }

            if (!prefixesUsed.contains(listOfPrefixes.get(i))) {
                if (prefixesUsed.size() > 0) {
                    firstPartOfQuery.append(" , ");
                }

                firstPartOfQuery.append(listOfPrefixes.get(i));
                if (mapOfJoin.containsKey(listOfPrefixes.get(i))) {
                    firstPartOfQuery.append(" " + mapOfJoin.get(listOfPrefixes.get(i)) + " ");
                }

                prefixesUsed.add(listOfPrefixes.get(i));
            }
        }

        queryString.insert(0, firstPartOfQuery);

        if (queryString.toString().trim().length() == 0) {
            return null;
        }

        query = em.createQuery(queryString.toString());

        List<String> listOfParameters = new ArrayList<String>(mapOfParameters.keySet());
        for (String param : listOfParameters) {
            query.setParameter(param, mapOfParameters.get(param));
        }

        List<AbstractConfiguration> listOfResult = query.getResultList();
        return listOfResult;
    }

    //TODO
    public static List<AbstractConfiguration> getConfigurationsFiltered1(Class<?> entityToUse,
                                                                         SystemInSession inSystemInSession, System inSystem, Institution inInstitution,
                                                                         TestingSession inTestingSession, Host inHost, Actor inActor, IntegrationProfile inIntegrationProfile,
                                                                         WSTransactionUsage wsTrans, String inComment, Boolean inIsSecured, Boolean inIsApproved,
                                                                         Pair<String, Object>... parameterNamesWithValues) {

        if (entityToUse == null) {
            return null;
        }

        List<AbstractConfiguration> listOfResult = new ArrayList<AbstractConfiguration>();

        if (wsTrans != null) {
            ConfigurationTypeWithPortsWSTypeAndSopClassQuery query = new ConfigurationTypeWithPortsWSTypeAndSopClassQuery();
            query.wsTRansactionUsage().eq(wsTrans);
            WebServiceConfigurationQuery q = new WebServiceConfigurationQuery();
            q.wsTransactionUsage().eq(wsTrans);

            listOfResult.addAll(q.getList());
        }

        return listOfResult;
    }

    public static Long getConfigurationsFilteredCount(Class<?> entityToUse, SystemInSession inSystemInSession,
                                                      System inSystem, Institution inInstitution, TestingSession inTestingSession, Host inHost, Actor inActor,
                                                      String inComment, Boolean inIsSecured, Boolean inIsApproved,
                                                      Pair<String, Object>... parameterNamesWithValues) {

        if (entityToUse == null) {
            return 0L;
        }

        EntityManager em = EntityManagerService.provideEntityManager();
        Query query;
        StringBuffer queryString = new StringBuffer();
        HashSet<String> prefixes = new HashSet<String>();
        HashSet<String> prefixesUsed = new HashSet<String>();
        HashMap<String, String> mapOfJoin = new HashMap<String, String>();
        HashMap<String, Object> mapOfParameters = new HashMap<String, Object>();

        if (inInstitution != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            prefixes.add("InstitutionSystem instSys");
            queryString
                    .append("value.configuration.systemInSession.system = instSys.system and instSys.institution = :choosenInst");
            mapOfParameters.put("choosenInst", inInstitution);
        }
        if (inSystemInSession != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.systemInSession = :inSystemInSession");
            mapOfParameters.put("inSystemInSession", inSystemInSession);
        }
        if (inSystem != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.systemInSession.system = :inSystem");
            mapOfParameters.put("inSystem", inSystem);
        }
        if (inTestingSession != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.systemInSession.testingSession =:inTestingSession");
            mapOfParameters.put("inTestingSession", inTestingSession);
        }
        if (inHost != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.host = :inHost  ");
            mapOfParameters.put("inHost", inHost);
        }
        if (inActor != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.actor = :inActor  ");
            mapOfParameters.put("inActor", inActor);
        }
        if (inComment != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.comment = :inComment  ");
            mapOfParameters.put("inComment", inComment);
        }
        if (inIsSecured != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.isSecured = :inIsSecured  ");
            mapOfParameters.put("inIsSecured", inIsSecured);
        }
        if (inIsApproved != null) {
            DatabaseUtil.addANDorWHERE(queryString);
            prefixes.add(entityToUse.getName() + " value");
            queryString.append("value.configuration.isApproved = :inIsApproved  ");
            mapOfParameters.put("inIsApproved", inIsApproved);
        }

        for (Pair<String, Object> parameterNameWithValue : parameterNamesWithValues) {
            if (parameterNameWithValue.getObject2() != null) {
                if ((parameterNameWithValue.getObject1() != null) && (
                        parameterNameWithValue.getObject1().trim().length() > 0)) {
                    String getterFromParameter =
                            "get" + parameterNameWithValue.getObject1().substring(0, 1).toUpperCase(Locale.getDefault()) + (
                                    parameterNameWithValue.getObject1().length() > 1 ?
                                            parameterNameWithValue.getObject1().substring(1) :
                                            "");

                    try {
                        new StringBuffer();
                        Class<?> class_ = entityToUse;

                        class_.getMethod(getterFromParameter);

                        DatabaseUtil.addANDorWHERE(queryString);
                        prefixes.add(entityToUse.getName() + " value");
                        queryString.append("value." + parameterNameWithValue.getObject1() + " = :in"
                                + parameterNameWithValue.getObject1());
                        mapOfParameters
                                .put("in" + parameterNameWithValue.getObject1(), parameterNameWithValue.getObject2());
                    } catch (SecurityException e) {
                        log.error("getConfigurationsFilteredCount", e);
                    } catch (NoSuchMethodException e) {
                        log.error("getConfigurationsFilteredCount", e);
                    }
                }
            }
        }

        List<String> listOfPrefixes = new ArrayList<String>(prefixes);

        StringBuffer firstPartOfQuery = new StringBuffer();

        for (int i = 0; i < listOfPrefixes.size(); i++) {
            if (i == 0) {
                firstPartOfQuery.append("SELECT COUNT(*) FROM ");
            }

            if (!prefixesUsed.contains(listOfPrefixes.get(i))) {
                if (prefixesUsed.size() > 0) {
                    firstPartOfQuery.append(" , ");
                }

                firstPartOfQuery.append(listOfPrefixes.get(i));
                if (mapOfJoin.containsKey(listOfPrefixes.get(i))) {
                    firstPartOfQuery.append(" " + mapOfJoin.get(listOfPrefixes.get(i)) + " ");
                }

                prefixesUsed.add(listOfPrefixes.get(i));
            }
        }

        queryString.insert(0, firstPartOfQuery);

        if (queryString.toString().trim().length() == 0) {
            return 0L;
        }

        query = em.createQuery(queryString.toString());

        List<String> listOfParameters = new ArrayList<String>(mapOfParameters.keySet());
        for (String param : listOfParameters) {
            query.setParameter(param, mapOfParameters.get(param));
        }

        Long numberOfResult = (Long) query.getSingleResult();
        return numberOfResult;
    }

    @SuppressWarnings("unchecked")
    public static boolean AssigningAuthoritiesExist(String assignAuth) {

        List<AbstractConfiguration> listToReturn = new ArrayList<AbstractConfiguration>();
        EntityManagerService.provideEntityManager();
        List<AbstractConfiguration> tmp;

        tmp = AbstractConfiguration
                .getConfigurationsFiltered(HL7V2InitiatorConfiguration.class, null, null, null, null, null, null, null,
                        null, null, new Pair<String, Object>("assigningAuthority", assignAuth));
        listToReturn.addAll(tmp);

        tmp = AbstractConfiguration
                .getConfigurationsFiltered(HL7V2ResponderConfiguration.class, null, null, null, null, null, null, null,
                        null, null, new Pair<String, Object>("assigningAuthority", assignAuth));
        listToReturn.addAll(tmp);

        tmp = AbstractConfiguration
                .getConfigurationsFiltered(HL7V3InitiatorConfiguration.class, null, null, null, null, null, null, null,
                        null, null, new Pair<String, Object>("assigningAuthority", assignAuth));
        listToReturn.addAll(tmp);

        tmp = AbstractConfiguration
                .getConfigurationsFiltered(HL7V3ResponderConfiguration.class, null, null, null, null, null, null, null,
                        null, null, new Pair<String, Object>("assigningAuthority", assignAuth));
        listToReturn.addAll(tmp);

        tmp = AbstractConfiguration
                .getConfigurationsFiltered(WebServiceConfiguration.class, null, null, null, null, null, null, null,
                        null, null, new Pair<String, Object>("assigningAuthority", assignAuth));
        listToReturn.addAll(tmp);

        return (listToReturn.size() > 0);
    }

    public static String getAssigningAuthority(System inSystem) {
        List<Institution> institutions = System.getInstitutionsForASystem(inSystem);

        // Get the first institution
        if (institutions.size() > 0) {
            StringBuffer keyword = new StringBuffer(institutions.get(0).getKeyword().toUpperCase(Locale.getDefault()));
            if (keyword.length() > 2) {
                keyword.delete(2, keyword.length());
            } else {
                keyword.append("0");
            }

            keyword.append(inSystem.getKeyword().toUpperCase(Locale.getDefault()));
            keyword.delete(4, keyword.length());

            int i = 0;

            while (AbstractConfiguration.AssigningAuthoritiesExist(keyword.toString())) {
                keyword.deleteCharAt(keyword.length() - 1);
                keyword.append(i++);
            }

            return keyword.toString();
        } else {
            return null;
        }
    }

    public static void addCSVServerHeaders(ServerConfiguration serverConfiguration, List<String> headers) {
        headers.add("port");
        headers.add("port proxy");
        headers.add("port secured");
        headers.add("port proxy secured");
    }

    public static void addCSVHTTPHeaders(HTTPConfiguration serverConfiguration, List<String> headers) {
        headers.add("url");
        headers.add("assigningAuthority");
        headers.add("ws-type");
        addCSVServerHeaders(serverConfiguration, headers);
    }

    public static void addCSVServerValues(ServerConfiguration serverConfiguration, List<String> values) {
        AbstractConfiguration abstractConfiguration = (AbstractConfiguration) serverConfiguration;
        Boolean isSecured = abstractConfiguration.getConfiguration().getIsSecured();
        if (isSecured == null) {
            isSecured = false;
        }

        if ((serverConfiguration.getPort() != null) && !isSecured) {
            values.add(Integer.toString(serverConfiguration.getPort()));
        } else {
            values.add("");
        }
        if ((serverConfiguration.getPortProxy() != null) && (serverConfiguration.getPort() != null) && !isSecured) {
            values.add(Integer.toString(serverConfiguration.getPortProxy()));
        } else {
            values.add("");
        }
        if ((serverConfiguration.getPortSecured() != null) && isSecured) {
            values.add(Integer.toString(serverConfiguration.getPortSecured()));
        } else {
            values.add("");
        }
        if ((serverConfiguration.getPortProxy() != null) && (serverConfiguration.getPortSecured() != null) && isSecured) {
            values.add(Integer.toString(serverConfiguration.getPortProxy()));
        } else {
            values.add("");
        }
    }

    public static void addCSVHTTPValues(HTTPConfiguration serverConfiguration, List<String> values) {
        AbstractConfiguration abstractConfiguration = (AbstractConfiguration) serverConfiguration;

        if (abstractConfiguration.getFinalURL() != null) {
            values.add(abstractConfiguration.getFinalURL());
        } else {
            values.add("");
        }
        if (serverConfiguration.getAssigningAuthority() != null) {
            values.add(serverConfiguration.getAssigningAuthority());
        } else {
            values.add("");
        }
        if (serverConfiguration.getWsTransactionUsage() != null) {
            values.add(serverConfiguration.getWsTransactionUsage().getTransaction().getKeyword() + ":"
                    + serverConfiguration.getWsTransactionUsage().getUsage());
        } else {
            values.add("");
        }
        addCSVServerValues(serverConfiguration, values);
    }

    public static Set<Integer> getUsedProxyPorts(TestingSession testingSession) {
        AbstractConfigurationQuery query = new AbstractConfigurationQuery();
        query.configuration().systemInSession().testingSession().eq(testingSession);
        List<AbstractConfiguration> configurations = query.getList();
        Set<Integer> result = new HashSet<Integer>();
        for (AbstractConfiguration abstractConfiguration : configurations) {
            if (abstractConfiguration instanceof ServerConfiguration) {
                Integer portProxy = ((ServerConfiguration) abstractConfiguration).getPortProxy();
                if (portProxy != null) {
                    result.add(portProxy);
                }
            }
        }
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public boolean isServer() {
        return (this instanceof ServerConfiguration);
    }

    public String getDetails1() {
        if (this instanceof AbstractDicomConfiguration) {
            return ((AbstractDicomConfiguration) this).getAeTitle();
        } else if (this instanceof HL7V2InitiatorConfiguration) {
            return ((HL7V2InitiatorConfiguration) this).getSendingReceivingApplication() + " / "
                    + ((HL7V2InitiatorConfiguration) this).getSendingReceivingFacility();
        } else if (this instanceof HL7V2ResponderConfiguration) {
            return ((HL7V2ResponderConfiguration) this).getSendingReceivingApplication() + " / "
                    + ((HL7V2ResponderConfiguration) this).getSendingReceivingFacility();
        } else if (this instanceof HL7V3InitiatorConfiguration) {
            return "";
        } else if (this instanceof HL7V3ResponderConfiguration) {
            return ((HL7V3ResponderConfiguration) this).getWSTranactionUsageAsText();
        } else if (this instanceof SyslogConfiguration) {
            if (((SyslogConfiguration) this).getTransportLayer() != null) {
                return ((SyslogConfiguration) this).getTransportLayer().getName();
            } else {
                return "";
            }
        } else if (this instanceof RawConfiguration) {
            if (((RawConfiguration) this).getTransactionDescription() != null) {
                return ((RawConfiguration) this).getTransactionDescription();
            } else {
                return "";
            }
        }
        else if (this instanceof WebServiceConfiguration) {
            return ((WebServiceConfiguration) this).getWSTranactionUsageAsText();
        }
        return "";
    }
    public boolean details1IsEditable() {
        String res = getDetails1();
        return StringUtils.isNotEmpty(res);
    }

    public boolean details2IsEditable() {
        String res = getDetails2();
        return StringUtils.isNotEmpty(res);
    }

    public boolean finalUrlIsEditable() {
        String res = getFinalURL();
        return StringUtils.isNotEmpty(res);
    }

    public String getDetails2() {
        if (this instanceof AbstractDicomConfiguration) {
            SopClass sopClass = ((AbstractDicomConfiguration) this).getSopClass();
            if (sopClass != null) {
                return sopClass.getKeyword();
            } else {
                return "";
            }
        } else if (this instanceof AbstractHL7Configuration) {
            return ((AbstractHL7Configuration) this).getAssigningAuthority();
        } else if (this instanceof SyslogConfiguration) {
            return ((SyslogConfiguration) this).getProtocolVersion();
        } else if (this instanceof WebServiceConfiguration){
            return Boolean.TRUE.equals(((WebServiceConfiguration) this).getHttpRewrite()) ? "http rewrite enabled" : "http rewrite disabled";
        }
        return "";
    }

    public String getHeader1() {
        return getHeader1(this.getClass());
    }

    public String getHeader2() {
        return getHeader2(this.getClass());
    }

    public String getTypeLabel() {
        return getTypeLabel(this.getClass());
    }

    @Override
    public List<String> getCSVHeaders() {
        if (configuration != null) {
            return configuration.getCSVHeaders();
        } else {
            return new ArrayList<String>();
        }
    }

    @Override
    public List<String> getCSVValues() {
        if (configuration != null) {
            return configuration.getCSVValues();
        } else {
            return new ArrayList<String>();
        }
    }

    @Override
    public String toString() {
        return "AbstractConfiguration [id=" + id + ", configuration=" + configuration + ", comments=" + comments + "]";
    }

    public String getFinalURL() {
        if (this instanceof HTTPConfiguration) {
            HTTPConfiguration httpConfiguration = (HTTPConfiguration) this;
            StringBuffer sb = new StringBuffer();
            Configuration configuration = this.getConfiguration();
            if (configuration != null) {
                if ((configuration.getIsSecured() != null) && configuration.getIsSecured()) {
                    sb.append("https://");
                } else {
                    sb.append("http://");
                }
                boolean ignoreDomainName = false;
                if (configuration.getHost() != null) {
                    if (configuration.getHost().getHostname() != null) {
                        sb.append(configuration.getHost().getHostname());
                    }
                    if (configuration.getHost().getIgnoreDomainName() != null) {
                        ignoreDomainName = configuration.getHost().getIgnoreDomainName();
                    }
                }
                NetworkConfigurationForTestingSession networkConf = null;
                if ((configuration != null) && (configuration.getSystemInSession() != null) && (
                        configuration.getSystemInSession().getTestingSession() != null) && (
                        configuration.getSystemInSession().getTestingSession().getNetworkConfiguration() != null)) {
                    networkConf = configuration.getSystemInSession().getTestingSession().getNetworkConfiguration();
                }

                if ((networkConf != null) && !ignoreDomainName) {
                    if (networkConf.getDomainName() != null) {
                        sb.append("." + networkConf.getDomainName());
                    } else {
                        sb.append("");
                    }
                }
                Integer port = null;
                if ((configuration.getIsSecured() != null) && configuration.getIsSecured()) {
                    port = httpConfiguration.getPortSecured();
                } else {
                    port = httpConfiguration.getPort();
                }
                if (port != null) {
                    Integer defaultPort = null;
                    if ((configuration.getIsSecured() != null) && configuration.getIsSecured()) {
                        defaultPort = 443;
                    } else {
                        defaultPort = 80;
                    }
                    if (!port.equals(defaultPort)) {
                        sb.append(":");
                        sb.append(port);
                    }
                }
                sb.append("/");
                if (httpConfiguration.getUrl() != null) {
                    String url = httpConfiguration.getUrl();
                    if (url.startsWith("/")) {
                        url = url.substring(1);
                    }
                    sb.append(url);
                }
            }
            return sb.toString();
        } else {
            return null;
        }
    }

    public int getProxyPortsErrors() {
        return proxyPortsErrors;
    }

    public void setProxyPortsErrors(int proxyPortsErrors) {
        this.proxyPortsErrors = proxyPortsErrors;
    }

    public void getProxyPortIfNeeded(Set<Integer> usedPorts) {
        if (configuration != null) {
            SystemInSession systemInSession = configuration.getSystemInSession();
            if (systemInSession != null) {
                TestingSession testingSession = systemInSession.getTestingSession();
                if (testingSession != null) {
                    if (testingSession.getIsProxyUseEnabled()) {
                        if (this instanceof ServerConfiguration) {
                            ServerConfiguration serverConfiguration = (ServerConfiguration) this;

                            ProxyConfigurationForSession proxyConfig = ProxyConfigurationForSession
                                    .getProxyConfigurationForSession(testingSession);

                            if (proxyConfig != null) {
                                if (usedPorts == null) {
                                    usedPorts = getUsedProxyPorts(
                                            getConfiguration().getSystemInSession().getTestingSession());
                                }
                                Integer portRangeLow = proxyConfig.getPortRangeLow();
                                Integer portRangeHigh = proxyConfig.getPortRangeHigh();

                                if (serverConfiguration.getPortProxy() != null
                                        && serverConfiguration.getPortProxy() <= portRangeHigh
                                        && serverConfiguration.getPortProxy() >= portRangeLow) {

                                } else {
                                    Integer portProxy = null;
                                    for (int port = portRangeLow; port <= portRangeHigh; port++) {
                                        if (!usedPorts.contains(port)) {
                                            portProxy = port;
                                            break;
                                        }
                                    }
                                    if (portProxy != null) {
                                        usedPorts.add(portProxy);
                                        serverConfiguration.setPortProxy(portProxy);
                                    } else {
                                        log.error("No more proxy port available...");
                                        serverConfiguration.setPortProxy(null);
                                        setProxyPortsErrors(1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public int compareTo(AbstractConfiguration abstractConfiguration) {
        return this.getId().compareTo(abstractConfiguration.getId());
    }
}
