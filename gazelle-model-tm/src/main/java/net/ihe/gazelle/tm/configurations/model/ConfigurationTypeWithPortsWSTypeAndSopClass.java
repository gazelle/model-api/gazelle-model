/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, sep
 * @class ConfigurationTypeWithPortsWSTypeAndSopClass
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "configurationTypeWithPortsWSTypeAndSopClass")
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Audited
@Table(name = "tm_conftype_w_ports_wstype_and_sop_class", schema = "public")
@SequenceGenerator(name = "tm_conftype_w_ports_wstype_and_sop_class_sequence", sequenceName = "tm_conftype_w_ports_wstype_and_sop_class_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
// @Exchanged(set = SetTestDefinition.class)
public class ConfigurationTypeWithPortsWSTypeAndSopClass implements Serializable {

    private static final long serialVersionUID = 186149987619456701L;

    private static Logger log = LoggerFactory.getLogger(ConfigurationTypeWithPortsWSTypeAndSopClass.class);

    /**
     * Id attribute!
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_conftype_w_ports_wstype_and_sop_class_sequence")
    private Integer id;

    @XmlElementRef(name = "configurationType")
    @ManyToOne
    @JoinColumn(name = "configurationType_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    // @ExchangeUnique
    private ConfigurationType configurationType;

    @XmlElement(name = "portNonSecure")
    @Range(min = 80, max = 65535)
    @Column(name = "port_non_secure", nullable = true)
    // @ExchangeUnique
    private Integer portNonSecure;

    @XmlElement(name = "portSecure")
    @Range(min = 1024, max = 65535)
    @Column(name = "port_secure", nullable = true)
    // @ExchangeUnique
    private Integer portSecure;

    @XmlElementRef(name = "webServiceType")
    @Deprecated
    @ManyToOne
    @JoinColumn(name = "web_service_type_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private WebServiceType webServiceType;

    @XmlElementRef(name = "wsTRansactionUsage")
    @ManyToOne
    @JoinColumn(name = "ws_transaction_usage_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    // @ExchangeUnique
    private WSTransactionUsage wsTRansactionUsage;

    @XmlElementRef(name = "sopClass")
    @ManyToOne
    @JoinColumn(name = "sop_class_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    // @ExchangeUnique
    private SopClass sopClass;

    @XmlElementRef(name = "transportLayer")
    @ManyToOne
    @JoinColumn(name = "transport_layer_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    // @ExchangeUnique
    private TransportLayer transportLayer;

    @XmlElement(name = "transactionDescription")
    @Column(name = "transaction_description", nullable = true)
    private String transactionDescription;

    @XmlElement(name = "comment")
    @Column(name = "comment", nullable = true)
    private String comment;

    public ConfigurationTypeWithPortsWSTypeAndSopClass() {

    }

    public ConfigurationTypeWithPortsWSTypeAndSopClass(ConfigurationTypeWithPortsWSTypeAndSopClass inConfiguration) {
        if (inConfiguration == null) {
            return;
        }

        this.configurationType = inConfiguration.configurationType;
        this.webServiceType = inConfiguration.webServiceType;
        this.sopClass = inConfiguration.sopClass;
        this.transportLayer = inConfiguration.transportLayer;
        this.transactionDescription = inConfiguration.transactionDescription;
        this.comment = inConfiguration.comment;
        if (inConfiguration.portSecure != null) {
            portSecure =  Integer.valueOf(inConfiguration.portSecure);
        }
        if (inConfiguration.portNonSecure != null) {
            portNonSecure =  Integer.valueOf(inConfiguration.portNonSecure);
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ConfigurationType getConfigurationType() {
        return configurationType;
    }

    public void setConfigurationType(ConfigurationType configurationType) {
        this.configurationType = configurationType;
    }

    public Integer getPortNonSecure() {
        return portNonSecure;
    }

    public void setPortNonSecure(Integer portNonSecure) {
        this.portNonSecure = portNonSecure;
    }

    public Integer getPortSecure() {
        return portSecure;
    }

    public void setPortSecure(Integer portSecure) {
        this.portSecure = portSecure;
    }

    public WebServiceType getWebServiceType() {
        return webServiceType;
    }

    public void setWebServiceType(WebServiceType webServiceType) {
        this.webServiceType = webServiceType;
    }

    public SopClass getSopClass() {
        return sopClass;
    }

    public void setSopClass(SopClass sopClass) {
        this.sopClass = sopClass;
    }

    public TransportLayer getTransportLayer() {
        return transportLayer;
    }

    public void setTransportLayer(TransportLayer transportLayer) {
        this.transportLayer = transportLayer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public WSTransactionUsage getWsTRansactionUsage() {
        return wsTRansactionUsage;
    }

    public void setWsTRansactionUsage(WSTransactionUsage wsTRansactionUsage) {
        this.wsTRansactionUsage = wsTRansactionUsage;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }


    // public ConfigurationTypeMappedWithAIPO getConfigurationTypeMappedWithAIPO() {
    // return configurationTypeMappedWithAIPO;
    // }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        return "ConfigurationTypeWithPortsWSTypeAndSopClass [id=" + id + ", configurationType=" + configurationType
                + ", portNonSecure=" + portNonSecure + ", portSecure=" + portSecure + ", webServiceType="
                + webServiceType + ", sopClass=" + sopClass + ", transportLayer=" + transportLayer + ", comment="
                + comment + "]";
    }
}