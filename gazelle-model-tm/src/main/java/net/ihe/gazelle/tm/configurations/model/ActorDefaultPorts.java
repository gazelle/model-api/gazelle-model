/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import net.ihe.gazelle.tf.model.Actor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * <b>Class Description : </b>ActorDefaultsPorts<br>
 * <br>
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class ActorDefaultsPorts.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tf_actor_default_ports", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "actor_id", "config_name", "type_of_config"}))
@SequenceGenerator(name = "tf_actor_default_ports_sequence", sequenceName = "tf_actor_default_ports_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class ActorDefaultPorts extends AuditedObject implements java.io.Serializable {

    public static final String TYPE_CONFIG_HL7 = "HL7";
    public static final String TYPE_CONFIG_HL7_V3 = "HL7V3";
    public static final String TYPE_CONFIG_DICOM_SCU = "DICOM-SCU";
    public static final String TYPE_CONFIG_DICOM_SCP = "DICOM-SCP";
    public static final String TYPE_CONFIG_WEBSERVICE = "WS";
    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450931131541283760L;
    public static EntityManager entityManager;

    // Attributes (existing in database as a column)

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_actor_default_ports_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "actor_id")
    @Fetch(value = FetchMode.SELECT)
    private Actor actor;

    @Column(name = "type_of_config", nullable = false)
    private String typeOfConfig;

    @Column(name = "port_non_secure", nullable = true)
    @Min(value = 0)
    @Max(value = 65635)
    private Integer portNonSecure;

    @Column(name = "port_secure", nullable = true)
    @Min(value = 0)
    @Max(value = 65635)
    private Integer portSecure;

    @Column(name = "config_name")
    private String configurationName;

    public ActorDefaultPorts() {
    }

    public ActorDefaultPorts(Integer inPortNonSecure, Integer inPortSecure) {
        portNonSecure = inPortNonSecure;
        portSecure = inPortSecure;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public String getTypeOfConfig() {
        return typeOfConfig;
    }

    public void setTypeOfConfig(String typeOfConfig) {
        this.typeOfConfig = typeOfConfig;
    }

    public Integer getPortNonSecure() {
        return portNonSecure;
    }

    public void setPortNonSecure(Integer portNonSecure) {
        this.portNonSecure = portNonSecure;
    }

    public Integer getPortSecure() {
        return portSecure;
    }

    public void setPortSecure(Integer portSecure) {
        this.portSecure = portSecure;
    }

    public String getConfigurationName() {
        return configurationName;
    }

    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }

}
