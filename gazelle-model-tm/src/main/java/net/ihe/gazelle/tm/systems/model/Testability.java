package net.ihe.gazelle.tm.systems.model;

public enum Testability {

    TESTABLE("testable", "net.ihe.gazelle.tm.Testability.testable", 1),
    FEW_PARTNERS("few_partners", "net.ihe.gazelle.tm.Testability.fewPartners", 2),
    DROPPED("dropped", "net.ihe.gazelle.tm.Testability.dropped", 3),
    REMOVED_FROM_SESSION("removed_from_session", "net.ihe.gazelle.tm.Testability.removedFromSession", 4),
    DECISION_PENDING("decision_pending", "net.ihe.gazelle.tm.Testability.decisionPending", 5);

    private final String keyword;
    private final String labelToDisplay;
    private final int rank;

    Testability(String keyword, String labelToDisplay, int rank) {
        this.keyword = keyword;
        this.labelToDisplay = labelToDisplay;
        this.rank = rank;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public int getRank() {
        return rank;
    }
}
