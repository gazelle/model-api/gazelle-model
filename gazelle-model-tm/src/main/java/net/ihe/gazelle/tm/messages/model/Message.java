package net.ihe.gazelle.tm.messages.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

/**
 * A message related to an event linked to multiple users
 */
@Entity
@Table(name = "usr_messages")
@SequenceGenerator(name = "usr_messages_sequence", sequenceName = "usr_messages_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Message implements Serializable {

    private static final long serialVersionUID = 5753212182315533124L;

    @Transient
    private TestingSession testingSession;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_messages_sequence")
    private Integer id;

    private Date date;

    private String type;

    private String link;

    private String author;

    private String image;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<MessageParameter> messageParameters;

    @OneToMany(mappedBy = "messageId", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<MessageUser> users;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<MessageParameter> getMessageParameters() {
        ArrayList<MessageParameter> res = new ArrayList<>(messageParameters);
        if (!res.isEmpty() && res.get(0).getId() != null) {
            Collections.sort(res);
        }
        return res;
    }

    public void setMessageParameters(List<MessageParameter> messageParameters) {
        this.messageParameters = messageParameters;
    }

    public Set<MessageUser> getUsers() {
        return users;
    }

    public void setUsers(Set<MessageUser> users) {
        this.users = users;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFormattedImage() {
        return PreferenceService.getString("application_url") + image;
    }


    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Message [id=");
        builder.append(id);
        builder.append(", date=");
        builder.append(date);
        builder.append(", type=");
        builder.append(type);
        builder.append(", link=");
        builder.append(link);
        builder.append(", messageParameters=");
        builder.append(messageParameters);
        builder.append(", userIDList=");
        builder.append(users);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
