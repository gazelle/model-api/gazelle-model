/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * <b>Class Description : </b>InstitutionSystem<br>
 * <br>
 * This class describes the relation between an Institution and a System. This relation corresponds to an object, it is used by the Gazelle application.
 * <p/>
 * InstitutionSystem possesses the following attributes :
 * <ul>
 * <li><b>systemId</b> : System Id corresponding to the System</li>
 * <li><b>institutionId</b> : Institution Id associated to the System</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, February 14
 * @class InstitutionSystem.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_institution_system", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_id", "institution_id"}))
@SequenceGenerator(name = "tm_institution_system_sequence", sequenceName = "tm_institution_system_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class InstitutionSystem extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911163531283760L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_institution_system_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "system_id")
    @Fetch(value = FetchMode.SELECT)
    private System system;

    @ManyToOne
    @JoinColumn(name = "institution_id")
    @Fetch(value = FetchMode.SELECT)
    private Institution institution;

    @Column
    private String comment;

    // Constructors

    public InstitutionSystem() {
    }

    public InstitutionSystem(Integer id) {

        this.id = id;
    }

    public InstitutionSystem(System system, Institution institution) {

        this.system = system;
        this.institution = institution;
    }

    public InstitutionSystem(System system, Institution institution, String comment) {

        this.system = system;
        this.institution = institution;
        this.comment = comment;
    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy an InstitutionSystem and to persist this new object, it is necessary it
     * doesn't have the same identifier.
     *
     * @param institutionSystem
     */
    public InstitutionSystem(InstitutionSystem institutionSystem) {
        this.system = institutionSystem.getSystem();
        this.institution = institutionSystem.getInstitution();
        if (institutionSystem.getComment() != null) {
            this.comment = new String(institutionSystem.getComment());
        }

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public System getSystem() {
        return this.system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public Institution getInstitution() {
        return this.institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
