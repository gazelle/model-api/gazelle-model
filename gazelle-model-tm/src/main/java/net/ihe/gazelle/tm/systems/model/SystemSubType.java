/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.FilterLabelProvider;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * <b>Class Description : </b>SystemSubType<br>
 * <br>
 * This class describes the SystemSubType object, used by the Gazelle application. It corresponds to a subtype of system (eg. PACS CARDIOLOGY). There is a difference between SystemType and
 * SystemSubType : Here is an example to explain it : - SystemType is for instance : PACS - SystemSubType is for instance : PACS Cardiology or PACS Radiology
 * <p/>
 * This class belongs to the Systems module.
 * <p/>
 * SystemSubType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the SystemSubType</li>
 * <li><b>systemSubTypeKeyword</b> : Keyword of the System SubType (example : PACS-CARD)</li>
 * <li><b>systemSubTypeDescription</b> : Description of the System SubType (example : PACS Cardiology)</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 23
 * @class SystemSubType.java
 * @package net.ihe.gazelle.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "sys_system_subtype", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "sys_system_subtype_sequence", sequenceName = "sys_system_subtype_id_seq", allocationSize = 1)
@Exchanged(set = SetRegistration.class)
public class SystemSubType extends AuditedObject implements java.io.Serializable, FilterLabelProvider {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5993534457358192088L;

    private static Logger log = LoggerFactory.getLogger(SystemSubType.class);

    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sys_system_subtype_sequence")
    private Integer id;

    @Column(name = "keyword")
    @NotNull
    private String systemSubTypeKeyword;

    @Column(name = "description")
    private String systemSubTypeDescription;

    // If this attribute is true, it means that the entry is in the shortlist
    // used for quick search (and not in whole list containing too many entries)
    @Column(name = "subtype_for_quick_search")
    private Boolean subTypeForQuickSearch;

    @Transient
    private Boolean isSelectedInTree;

    // Constructors
    public SystemSubType() {
    }

    public SystemSubType(Integer id, String systemSubTypeKeyword, String systemSubTypeDescription,
                         Boolean subTypeForQuickSearch) {
        this.id = id;
        this.systemSubTypeKeyword = systemSubTypeKeyword;
        this.systemSubTypeDescription = systemSubTypeDescription;
        this.subTypeForQuickSearch = subTypeForQuickSearch;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSystemSubTypeKeyword() {
        return systemSubTypeKeyword;
    }

    public void setSystemSubTypeKeyword(String systemSubTypeKeyword) {
        this.systemSubTypeKeyword = systemSubTypeKeyword;
    }

    public String getSystemSubTypeDescription() {
        return systemSubTypeDescription;
    }

    public void setSystemSubTypeDescription(String systemSubTypeDescription) {
        this.systemSubTypeDescription = systemSubTypeDescription;
    }

    public Boolean getSubTypeForQuickSearch() {
        return subTypeForQuickSearch;
    }

    public void setSubTypeForQuickSearch(Boolean subTypeForQuickSearch) {
        this.subTypeForQuickSearch = subTypeForQuickSearch;
    }

    public Boolean getIsSelectedInTree() {
        return isSelectedInTree;
    }

    public void setIsSelectedInTree(Boolean isSelectedInTree) {
        this.isSelectedInTree = isSelectedInTree;
    }

    // *********************************************************
    // Hashcode and equals
    // *********************************************************

    @Override
    @FilterLabel
    public String getSelectableLabel() {
        return getSystemSubTypeKeyword() + " - " + getSystemSubTypeDescription();
    }

    @Override
    public String toString() {

        if (this == null) {
            return null;
        }

        return "SystemSubType [id=" + id + ", systemSubTypeKeyword=" + systemSubTypeKeyword + "]";
    }

}
