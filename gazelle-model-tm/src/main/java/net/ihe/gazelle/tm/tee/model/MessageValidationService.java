package net.ihe.gazelle.tm.tee.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * The persistent class for the message_validation_service database table.
 *
 * @author tnabeel
 */
@XmlRootElement(name = "messageValidationService")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "message_validation_service")
@SequenceGenerator(name = "message_validation_service_id_generator", sequenceName = "message_validation_service_sequence", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class MessageValidationService implements Serializable {

    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(MessageValidationService.class);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "message_validation_service_id_generator")
    @XmlTransient
    private int id;

    @ManyToOne
    @JoinColumn(name = "validation_service_id")
    private ValidationService validationService;

    @OneToMany(mappedBy = "messageValidationService")
    @XmlTransient
    private Set<TmStepInstanceMsgValidation> tmStepInstanceMsgValidations;

    @ManyToOne
    @JoinColumn(name = "tm_test_step_message_profile_id")
    @XmlTransient
    private TmTestStepMessageProfile tmTestStepMessageProfile;

    public MessageValidationService() {
    }

    public static void deleteMessageValidationService(EntityManager em, MessageValidationService srv) {

        if (srv == null) {

            return;
        }

        if (srv.getTmStepInstanceMsgValidations() != null) {
            if (srv.getTmStepInstanceMsgValidations().size() > 0) {

                return;
            }
        }

        srv = em.find(MessageValidationService.class, srv.getId());
        em.remove(srv);
        em.flush();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<TmStepInstanceMsgValidation> getTmStepInstanceMsgValidations() {
        return this.tmStepInstanceMsgValidations;
    }

    public void setTmStepInstanceMsgValidations(Set<TmStepInstanceMsgValidation> tmStepInstanceMsgValidations) {
        this.tmStepInstanceMsgValidations = tmStepInstanceMsgValidations;
    }

    public TmStepInstanceMsgValidation addTmStepInstanceMsgValidation(
            TmStepInstanceMsgValidation tmStepInstanceMsgValidation) {
        getTmStepInstanceMsgValidations().add(tmStepInstanceMsgValidation);
        tmStepInstanceMsgValidation.setMessageValidationService(this);

        return tmStepInstanceMsgValidation;
    }

    public TmStepInstanceMsgValidation removeTmStepInstanceMsgValidation(
            TmStepInstanceMsgValidation tmStepInstanceMsgValidation) {
        getTmStepInstanceMsgValidations().remove(tmStepInstanceMsgValidation);
        tmStepInstanceMsgValidation.setMessageValidationService(null);

        return tmStepInstanceMsgValidation;
    }

    public ValidationService getValidationService() {
        return validationService;
    }

    public void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }

    public TmTestStepMessageProfile getTmTestStepMessageProfile() {
        return tmTestStepMessageProfile;
    }

    public void setTmTestStepMessageProfile(TmTestStepMessageProfile tmTestStepMessageProfile) {
        this.tmTestStepMessageProfile = tmTestStepMessageProfile;
    }

    public void saveOrMerge(EntityManager entityManager, TmTestStepMessageProfile tmTestStepMessageProfile) {
        this.tmTestStepMessageProfile = tmTestStepMessageProfile;
        validationService.saveOrMerge(entityManager);

        MessageValidationServiceQuery query = new MessageValidationServiceQuery(entityManager);
        query.validationService().eq(validationService);
        query.tmTestStepMessageProfile().eq(tmTestStepMessageProfile);

        List<MessageValidationService> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
    }

}