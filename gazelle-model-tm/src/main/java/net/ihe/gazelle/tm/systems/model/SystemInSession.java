/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.dates.DateDisplayer;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceFile;
import net.ihe.gazelle.objects.model.ObjectType;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.OIDSystemAssignment;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemInSessionUser;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;
import net.ihe.gazelle.tm.systems.model.reversed.DemonstrationSystemInSession;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.Person;
import net.ihe.gazelle.util.Pair;
import org.apache.commons.lang.SerializationUtils;
import org.hibernate.Criteria;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <b>Class Description : </b>SystemInSession<br>
 * <br>
 * This class describes the SystemInSession object, used by the Gazelle application. It corresponds to a medical System associated to ONE
 * event/session. This class belongs to the Systems module.
 * <p/>
 * SystemInSession possesses the following attributes :
 * <ul>
 * <li><b>systemId</b> : id corresponding to the System</li>
 * <li><b>testingSessionId</b> : id corresponding to session (example : Connectathon Chicago 2008)</li>
 * <li><b>tableSession</b> : Table during the Session (example : Table A6)</li>
 * <li><b>acceptedToSession</b> : is this system accepted to this session ?</li>
 * <li><b>extraIPAddress</b> : The number of extra IP addresses requested for this event</li>
 * <li><b>isCopy</b> : is this system copied ?</li>
 * </ul>
 * </br> <b>Example of SystemInSession</b> : PACS_AGFA, Connectathon_Chicago_2008, Table A6<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, December 21
 * @class SystemInSession.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "SystemInSession")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "tm_system_in_session", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_id", "testing_session_id"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_system_in_session_sequence", sequenceName = "tm_system_in_session_id_seq", allocationSize = 1)
// @Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("SYS")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SystemInSession extends AuditedObject implements java.io.Serializable, Comparable<SystemInSession> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911131554283760L;
    private static Logger log = LoggerFactory.getLogger(SystemInSession.class);

    // Id Attribute : do not exist in database as a column ( combination system_id & testing_session_id )
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_in_session_sequence")
    private Integer id;

    // Attributes (existing in database as a column)

    @ManyToOne
    @JoinColumn(name = "table_session_id")
    @XmlElement(name = "Table")
    @Fetch(value = FetchMode.SELECT)
    private TableSession tableSession;

    @Column(name = "accepted_to_session", nullable = false)
    private Boolean acceptedToSession = false;

    @Column(name = "extra_ip_address")
    private Integer extraIPAddress;

    @Column(name = "is_copy")
    private Boolean isCopy;

    @XmlElement(name = "System")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "system_id")
    @Fetch(value = FetchMode.SELECT)
    private System system;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "testing_session_id")
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    @Column(name = "is_ihe_is_required")
    private Boolean isIHEISRequired;

    @Column(name = "power")
    private Integer power;

    @Column(name = "voltage")
    private Integer voltage;

    @Column(name = "is_conf_stat_provided")
    private Boolean isConfStatProvided;

    @ManyToOne
    @JoinColumn(name = "person_servicing")
    @Fetch(value = FetchMode.SELECT)
    private Person personServicing;

    @Column(name = "storage_volume")
    private Float storageVolume;

    @Column(name = "ampere")
    private Float ampere;

    @Column(name = "table_label")
    private String tableLabel;

    @Column(name = "is_ihe_is_provided")
    private Boolean isIHEISProvided;

    @Column(name = "skype")
    private String skype;

    @Column(name = "blog")
    private String blog;

    @Column(name = "notesList", length = Integer.MAX_VALUE - 1)
    private byte[] notesList;

    @ManyToOne
    @JoinColumn(name = "system_in_session_status_id")
    @Fetch(value = FetchMode.SELECT)
    private SystemInSessionStatus systemInSessionStatus;

    @ManyToMany
    @JoinTable(name = "tm_demonstration_system_in_session", joinColumns = @JoinColumn(name = "system_in_session_id"), inverseJoinColumns =
    @JoinColumn(name = "demonstration_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_in_session_id", "demonstration_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<Demonstration> demonstrations;

    // Computed set, do not update that one
    @OneToMany(mappedBy = "systemInSession", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<DemonstrationSystemInSession> demonstrationsSystemInSession;

    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private Set<ObjectInstance> objectInstances;

    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectInstanceFile> objectInstanceFiles;

    @OneToMany(mappedBy = "systemInSession", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<Configuration> configurations;

    @OneToMany(mappedBy = "systemInSession", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<SystemInSessionUser> systemInSessionUsers;

    @Column(name = "registration_status")
    private SystemInSessionRegistrationStatus registrationStatus;

    @Transient
    private String notes;
    @Transient
    private UserService userClient;
    @Transient
    private GazelleIdentity identity;

    @Transient
    private boolean addCommentInNotes = false;


    // Constructors

    public SystemInSession() {
    }

    public SystemInSession(System system, TestingSession testingSession) {
        this.system = system;
        this.testingSession = testingSession;
    }

    public SystemInSession(TableSession tableSession, System system, TestingSession testingSession,
                           Person personServicing, SystemInSessionStatus systemInSessionStatus) {
        this.tableSession = tableSession;
        this.system = system;
        this.testingSession = testingSession;
        this.personServicing = personServicing;
        this.systemInSessionStatus = systemInSessionStatus;
    }

    public SystemInSession(SystemInSession sIs) {
        if (sIs.getTableSession() != null) {
            this.tableSession = sIs.tableSession;
        }
        if (sIs.getAcceptedToSession() != null) {
            this.acceptedToSession = Boolean.valueOf(sIs.getAcceptedToSession());
        }
        if (sIs.getExtraIPAddress() != null) {
            this.extraIPAddress = Integer.valueOf(sIs.getExtraIPAddress());
        }
        if (sIs.getIsIHEISRequired() != null) {
            this.isIHEISRequired = Boolean.valueOf(sIs.getIsIHEISRequired());
        }
        if (sIs.getPower() != null) {
            this.power = Integer.valueOf(sIs.getPower());
        }
        if (sIs.getVoltage() != null) {
            this.voltage = Integer.valueOf(sIs.getVoltage());
        }
        if (sIs.getIsConfStatProvided() != null) {
            this.isConfStatProvided = Boolean.valueOf(sIs.getIsConfStatProvided());
        }
        if (sIs.getStorageVolume() != null) {
            this.storageVolume = Float.valueOf(sIs.getStorageVolume());
        }
        if (sIs.getAmpere() != null) {
            this.ampere = Float.valueOf(sIs.getAmpere());
        }
        if (sIs.getTableLabel() != null) {
            this.tableLabel = String.valueOf(sIs.getTableLabel());
        }
        if (sIs.getIsIHEISProvided() != null) {
            this.isIHEISProvided = Boolean.valueOf(sIs.getIsIHEISProvided());
        }
        if (sIs.getSkype() != null) {
            this.skype = String.valueOf(sIs.getSkype());
        }
        if (sIs.getBlog() != null) {
            this.blog = String.valueOf(sIs.getBlog());
        }
        if (sIs.getIsCopy() != null) {
            this.isCopy = Boolean.valueOf(true);
        }
        if (sIs.getSystem() != null) {
            this.system = sIs.getSystem();
        }
        if (sIs.getTestingSession() != null) {
            this.testingSession = sIs.getTestingSession();
        }
        if (sIs.getSystemInSessionStatus() != null) {
            this.systemInSessionStatus = sIs.getSystemInSessionStatus();
        }
        if (sIs.getDemonstrations() != null) {
            this.demonstrations = sIs.getDemonstrations();
        }

    }

    public static List<SystemInSession> findSystemInSessionsWithSystem(System inSystem) {
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.system().id().eq(inSystem.getId());
        return q.getList();
    }

    public static List<SystemInSession> getSystemsInSessionForCompanyNameForSession(String inCompanyNameToSearch,
                                                                                    TestingSession testingSession) {

        if (testingSession == null) {
            log.error("testingSession is null");
            return null;
        }
        if ((inCompanyNameToSearch == null) || (inCompanyNameToSearch.length() == 0)) {
            log.error("inCompanyNameToSearch is null");
            return null;
        }

        InstitutionSystemQuery q1 = new InstitutionSystemQuery();
        q1.institution().name().eq(inCompanyNameToSearch);
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.testingSession().id().eq(testingSession.getId());
        q.system().id().in(q1.system().id().getListDistinct());

        return q.getListDistinct();
    }

    public static List<SystemInSession> getSystemsInSessionForCompanyForSession(EntityManager em,
                                                                                Institution inCompanyToSearch, TestingSession testingSession) {
        return getSystemInSessionFiltered(em, null, testingSession, inCompanyToSearch, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null);
    }

    public static List<SystemInSession> getNotDroppedSystemsInSessionForCompanyForSession(EntityManager em,
                                                                                          Institution inCompanyToSearch, TestingSession
                                                                                                  testingSession) {
        return getSystemInSessionFiltered(em, null, testingSession, inCompanyToSearch, null, null, null, null, null,
                null, null, null, null, null, null, null, null, true);
    }

    public static List<SystemInSession> getSystemsInSessionForCompanyForSessionForActorForIP(EntityManager em,
                                                                                             Institution inCompanyToSearch, TestingSession
                                                                                                     testingSession, Actor inActor,
                                                                                             IntegrationProfile inIntegrationProfile) {
        return getSystemInSessionFiltered(em, null, testingSession, inCompanyToSearch, null, null, null, null, null,
                inActor, inIntegrationProfile, null, null, null, null, null, null, null);
    }

    public static List<SystemInSession> getAcceptedSystemsInSessionForCompanyForSession(EntityManager em,
                                                                                        Institution inCompanyToSearch, TestingSession
                                                                                                testingSession) {
        return getSystemInSessionFiltered(em, null, testingSession, inCompanyToSearch, null, null, null, null, null,
                null, null, null, null, null, null, null, true, null);
    }

    public static List<SystemInSession> getAcceptedSystemsInSessionForSession(EntityManager em,
                                                                              TestingSession testingSession) {
        return getSystemInSessionFiltered(em, null, testingSession, null, null, null, null, null, null, null, null,
                null, null, null, null, null, true, null);
    }

    public static List<SystemInSession> getNotAcceptedSystemsInSessionForCompanyForSession(EntityManager em,
                                                                                           Institution inCompanyToSearch, TestingSession
                                                                                                   testingSession) {
        return getSystemInSessionFiltered(em, null, testingSession, inCompanyToSearch, null, null, null, null, null,
                null, null, null, null, null, null, null, false, null);
    }



    public static List<SystemInSession> getSystemsInSessionForLoggedCompanyForSession(TestingSession selectedTestingSession) {
        EntityManager em = EntityManagerService.provideEntityManager();
        return SystemInSession.getSystemsInSessionForLoggedCompanyForSession(em, selectedTestingSession);
    }

    public static List<SystemInSession> getSystemsInSessionForLoggedCompanyForSession(EntityManager em, TestingSession selectedTestingSession) {
        Institution institutionUsed = Institution.getLoggedInInstitution();
        return getSystemsInSessionForCompanyForSession(em, institutionUsed, selectedTestingSession);
    }

    public static List<SystemInSession> getNotDroppedSystemsInSessionForLoggedCompanyForSession(TestingSession selectedTestingSession) {
        EntityManager em = EntityManagerService.provideEntityManager();
        return SystemInSession.getNotDroppedSystemsInSessionForLoggedCompanyForSession(em, selectedTestingSession);
    }

    public static List<SystemInSession> getNotDroppedSystemsInSessionForLoggedCompanyForSession(EntityManager em, TestingSession selectedTestingSession) {
        Institution institutionUsed = Institution.getLoggedInInstitution();
        return getNotDroppedSystemsInSessionForCompanyForSession(em, institutionUsed, selectedTestingSession);
    }

    public static List<Pair<Boolean, SystemInSession>> getPairBoolSystemsInSessionForCompanyForSession(
            String inCompanyToSearch, TestingSession testingSession) {
        if (testingSession == null) {
            log.error("testingSession is null");
            return null;
        }
        if ((inCompanyToSearch == null) || (inCompanyToSearch.length() == 0)) {
            log.error("inCompanyNameToSearch is null");
            return null;
        }

        List<Pair<Boolean, SystemInSession>> ret = new LinkedList<Pair<Boolean, SystemInSession>>();
        List<SystemInSession> systemsList = getSystemsInSessionForCompanyNameForSession(inCompanyToSearch,
                testingSession);
        for (int i = 0; i < systemsList.size(); i++) {
            ret.add(new Pair<Boolean, SystemInSession>(false, systemsList.get(i)));

        }

        return ret;
    }

    public static List<Pair<Boolean, System>> getPairBoolSystemsForCompanyForSession(Institution institution) {
        if ((institution.getName() == null) || (institution.getName().length() == 0)) {
            log.error("inCompanyNameToSearch is null");
            return null;
        }

        List<Pair<Boolean, System>> ret = new LinkedList<Pair<Boolean, System>>();
        List<System> systemsList = System.getSystemsForAnInstitution(institution);
        for (int i = 0; i < systemsList.size(); i++) {
            ret.add(new Pair<Boolean, System>(false, systemsList.get(i)));

        }

        return ret;
    }

    public static List<SystemInSession> getSystemsInSessionForCompanyLikeForSession(String inSysKeywordOrName,
                                                                                    TestingSession testingSession) {
        InstitutionSystemQuery q = new InstitutionSystemQuery();
        q.addRestriction(HQLRestrictions
                .or(HQLRestrictions.like("system.name", inSysKeywordOrName, HQLRestrictionLikeMatchMode.ANYWHERE),
                        HQLRestrictions
                                .like("system.keyword", inSysKeywordOrName, HQLRestrictionLikeMatchMode.ANYWHERE)));

        List<Integer> listOfSystemID = q.system().id().getListDistinct();
        if (listOfSystemID.size() > 0) {
            return getSystemInSessions(testingSession, q);
        }
        return null;
    }

    @Deprecated
    public static List<SystemInSession> getSystemInSessions(TestingSession testingSession, EntityManager em,
                                                            Criteria c) {
        Query query = em.createQuery("SELECT distinct systInSession FROM SystemInSession systInSession "
                + "where systInSession.testingSession.id = :testingSessionId and systInSession.system.id "
                + "IN (:sysId)");
        query.setParameter("testingSessionId", testingSession.getId());
        query.setParameter("sysId", c.list());
        return query.getResultList();
    }

    public static List<SystemInSession> getSystemInSessions(TestingSession testingSession,
                                                            InstitutionSystemQuery query) {
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.testingSession().id().eq(testingSession.getId());
        q.system().id().in(query.system().id().getListDistinct());
        return q.getListDistinct();
    }

    /**
     * Allow to retrieve a list of system for a session using the SystemInSession Class
     *
     * @param testingSession
     * @return List of system
     */
    public static List<SystemInSession> getSystemsInSessionForSession(TestingSession testingSession) {
        if (testingSession == null) {
            log.error("testingSession is null");
            return null;
        }
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.testingSession().id().eq(testingSession.getId());
        q.system().name().order(true);
        return q.getList();
    }


    public static List<SystemInSession> getSystemsInSessionForCompany(String companyName) {
        InstitutionSystemQuery q1 = new InstitutionSystemQuery();
        q1.institution().name().eq(companyName);
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.system().id().in(q1.system().id().getListDistinct());

        return q.getListDistinct();
    }

    public static List<SystemInSession> getSystemsInSessionOnlyOwnedBySingleCompany(String companyName) {
        InstitutionSystemQuery q1 = new InstitutionSystemQuery();
        q1.institution().name().eq(companyName);
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.system().id().in(q1.system().id().getListDistinct());

        return q.getListDistinct();
    }

    public static List<SystemInSession> getAllSystemsInSession() {
        SystemInSessionQuery q = new SystemInSessionQuery();
        return q.getList();
    }

    /**
     * Allow to delete a systemInSession.Delete all certificates then Delete just the systemInSession and not the system attached for that call
     * public static void deleteSelectedSystem(final System
     * currentSystem, EntityManager entityManager)
     *
     * @param systemInSessionToDelete
     */
    public static void deleteSelectedSystemInSessionWithDependencies(SystemInSession systemInSessionToDelete) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        // Removing all Configuration objects associated to this SystemInSession
        Configuration.deleteConfigurationsForSystemInSession(systemInSessionToDelete);

        // Removing all TestInstanceParticipants objects associated to this SystemInSession
        TestInstanceParticipants.deleteTestParticipantForSystemInSession(systemInSessionToDelete);

        // Removing all OIDs associated to this SystemInSession
        OIDSystemAssignment.deleteAllOIDSystemAssignment(systemInSessionToDelete, null,
                systemInSessionToDelete.getTestingSession(), null, entityManager);

        // We retrieve the SystemInSession object to delete from the entityManager
        SystemInSession systemInSessionToBeDeleted = entityManager
                .find(SystemInSession.class, systemInSessionToDelete.getId());
        entityManager.remove(systemInSessionToBeDeleted);
        entityManager.flush();
        //

    }

    public static List<SystemInSession> getSystemInSessionFiltered(EntityManager em, System inSystem,
                                                                   TestingSession inTestingSession, Institution inInstitution,
                                                                   SystemInSessionStatus inStatus,
                                                                   Demonstration inDemonstration, ActorIntegrationProfile inActorIntegrationProfile,
                                                                   IntegrationProfileOption inIntegrationProfileOption,
                                                                   ActorIntegrationProfileOption inActorIntegrationProfileOption, Actor inActor,
                                                                   IntegrationProfile inIntegrationProfile, String inActorKeyword, String
                                                                           inIntegrationProfileKeyword) {
        return getSystemInSessionFiltered(em, inSystem, inTestingSession, inInstitution, inStatus, inDemonstration,
                inActorIntegrationProfile, inIntegrationProfileOption, inActorIntegrationProfileOption, inActor,
                inIntegrationProfile, inActorKeyword, inIntegrationProfileKeyword, null, null, null, null, null);
    }

    public static List<SystemInSession> getSystemInSessionFiltered(EntityManager entityManager, System inSystem,
                                                                   TestingSession inTestingSession, Institution inInstitution,
                                                                   SystemInSessionStatus inStatus,
                                                                   Demonstration inDemonstration, ActorIntegrationProfile inActorIntegrationProfile,
                                                                   IntegrationProfileOption inIntegrationProfileOption,
                                                                   ActorIntegrationProfileOption inActorIntegrationProfileOption, Actor inActor,
                                                                   IntegrationProfile inIntegrationProfile, String inActorKeyword, String
                                                                           inIntegrationProfileKeyword,
                                                                   Domain inDomain, ObjectType inObjectTypeCreator, ObjectType inObjectTypeReader,
                                                                   Boolean acceptedToSession,
                                                                   Boolean isNotDroppedSystem) {

        SystemInSessionQuery query = new SystemInSessionQuery();

        query.testingSession().eqIfValueNotNull(inTestingSession);
        query.system().institutionSystems().institution().eqIfValueNotNull(inInstitution);
        query.system().eqIfValueNotNull(inSystem);
        query.systemInSessionStatus().eqIfValueNotNull(inStatus);
        query.demonstrationsSystemInSession().demonstration().eqIfValueNotNull(inDemonstration);
        ActorIntegrationProfileOptionEntity<ActorIntegrationProfileOption> aipoPath = query.system()
                .systemActorProfiles().actorIntegrationProfileOption();

        aipoPath.actorIntegrationProfile().integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        aipoPath.actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        aipoPath.integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);
        aipoPath.actorIntegrationProfile().eqIfValueNotNull(inActorIntegrationProfile);
        aipoPath.eqIfValueNotNull(inActorIntegrationProfileOption);

        aipoPath.actorIntegrationProfile().integrationProfile().keyword().eqIfValueNotNull(inIntegrationProfileKeyword);
        aipoPath.actorIntegrationProfile().actor().keyword().eqIfValueNotNull(inActorKeyword);

        if (inDomain != null) {
            aipoPath.actorIntegrationProfile().integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }

        if (inObjectTypeCreator != null) {
            query.system().systemActorProfiles().aipo().objectCreators().object().id().eq(inObjectTypeCreator.getId());
        }
        if (inObjectTypeReader != null) {
            query.system().systemActorProfiles().aipo().objectReaders().object().id().eq(inObjectTypeReader.getId());
        }
        if (acceptedToSession != null) {
            query.system().systemsInSession().acceptedToSession().eq(acceptedToSession);
        }
        if ((isNotDroppedSystem != null) && (isNotDroppedSystem == true)) {
            query.addRestriction(HQLRestrictions.or(query.system().systemsInSession().registrationStatus()
                            .neqRestriction(SystemInSessionRegistrationStatus.DROPPED),
                    query.system().systemsInSession().registrationStatus().isNullRestriction()));
        }

        return query.getList();
    }

    public static SystemInSession getSystemInSessionForSession(System inSystem, TestingSession inTestingSession) {
        if ((inTestingSession == null) || (inSystem == null)) {
            log.error("getSystemInSessionForSession - System or TestingSession is null !!!    System = " + inSystem
                    + "   - TestingSession = " + inTestingSession);
            return null;
        }
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.testingSession().id().eq(inTestingSession.getId());
        q.system().id().eq(inSystem.getId());

        List<SystemInSession> systemInSessionsList = q.getList();
        if (systemInSessionsList.size() == 0) {
            log.error("getSystemInSessionForSession - no results");
            return null;
        } else if (systemInSessionsList.size() == 1) {
            return systemInSessionsList.get(0);
        } else {
            log.error("getSystemInSessionForSession - returns null");
            return null;
        }
    }

    public static List<SystemInSession> getSystemInSessionByUserByTestingSession(String username,
                                                                                 TestingSession inTestingSession) {
        if ((username != null) && (inTestingSession != null)) {
            SystemInSessionQuery q = new SystemInSessionQuery();
            q.testingSession().eq(inTestingSession);
            List<SystemInSession> list = q.getListDistinct();
            if (list.size() > 0) {
                return list;
            }
        }
        return null;
    }

    public boolean isAddCommentInNotes() {
        return addCommentInNotes;
    }

    public void setAddCommentInNotes(boolean com) {
        this.addCommentInNotes = com;
    }

    //TODO Systems getNotesList
    public ArrayList<SystemNotes> getNotesList() {
        //Left due to migration error on notesList
        //We now need to deserialize the object manually
        if (notesList == null) {
            setNotesList(new ArrayList<SystemNotes>());
        }
        if (0 == notesList.length) {
            setNotesList(new ArrayList<SystemNotes>());
            return new ArrayList<SystemNotes>();
        }
        Object res = SerializationUtils.deserialize(notesList);
        ArrayList<SystemNotes> res2 = (ArrayList<SystemNotes>) res;
        return res2;
    }

    public void setNotesList(ArrayList<SystemNotes> notes) {
        this.notesList = SerializationUtils.serialize(notes);
    }

    public String getNotes() {
        userClient = (UserService) Component.getInstance("gumUserService");
        if ((notes == null) || notes.isEmpty()) {
            List<SystemNotes> listNote = getNotesList();
            Collections.sort(listNote);

            StringBuilder commentsSB = new StringBuilder();
            for (SystemNotes systemNotes : listNote) {
                commentsSB.append("<p class=\"comment-item\"><b>");
                String userId = systemNotes.getUsername();
                commentsSB.append(userClient.getUserDisplayNameWithoutException(userId));
                commentsSB.append("</b> ");

                try {
                    User user = userClient.getUserById(userId);
                    commentsSB.append("<i>(").append(user.getOrganizationId()).append(")</i> ");
                } catch (NoSuchElementException e) {
                    log.info("User with id {} not found", userId);
                }

                String tiPattern = "TI-([0-9]{5})";
                Pattern regex = Pattern.compile(tiPattern, Pattern.DOTALL);
                Matcher regexMatcher = regex.matcher(systemNotes.toStringForComments());
                if (regexMatcher.find()) {
                    String tiNum = regexMatcher.group(1);
                    String notesWithLink = regexMatcher.replaceAll("<a href=\"" + PreferenceService.getString("application_url")
                            + "testing/test/test/TestInstance.seam?id=" + tiNum + "\">TI-" + tiNum + "</a>");
                    commentsSB.append(notesWithLink);
                } else {
                    commentsSB.append(systemNotes.toStringForComments());
                }

                commentsSB.append("<i class=\"comment-meta\">");
                commentsSB.append(GenericServiceLoader.getService(DateDisplayer.class)
                        .displayDateTime(systemNotes.getDateOfEvent()));
                commentsSB.append("</i>");
                commentsSB.append("</p>\r\n");
            }
            return commentsSB.toString();
        } else {
            return notes;
        }
    }

    public SystemInSessionRegistrationStatus getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(SystemInSessionRegistrationStatus registrationStatus) {
        if (getAcceptedToSession() && registrationStatus.equals(SystemInSessionRegistrationStatus.DROPPED)) {
            setAcceptedToSession(false);
        }
        this.registrationStatus = registrationStatus;
    }

    public System getSystem() {
        return this.system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public TestingSession getTestingSession() {
        return this.testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public TableSession getTableSession() {
        return this.tableSession;
    }

    public void setTableSession(TableSession tableSession) {
        this.tableSession = tableSession;
    }

    public Boolean getAcceptedToSession() {
        if (this.acceptedToSession == null) {
            this.acceptedToSession = false;
        }
        return this.acceptedToSession;
    }

    public void setAcceptedToSession(Boolean acceptedToSession) {
        if ((getRegistrationStatus() == null) || (getRegistrationStatus().getWeight() != 1)) {
            this.acceptedToSession = acceptedToSession;
        } else {
            this.acceptedToSession = false;
        }
    }

    public Integer getExtraIPAddress() {
        return this.extraIPAddress;
    }

    public void setExtraIPAddress(Integer extraIPAddress) {
        this.extraIPAddress = extraIPAddress;
    }

    public Boolean getIsCopy() {
        return this.isCopy;
    }

    public void setIsCopy(Boolean isCopy) {
        this.isCopy = isCopy;
    }

    public Boolean getIsIHEISRequired() {
        return this.isIHEISRequired;
    }

    public void setIsIHEISRequired(Boolean isIHEISRequired) {
        this.isIHEISRequired = isIHEISRequired;
    }

    public Integer getPower() {
        return this.power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getVoltage() {
        return this.voltage;
    }

    public void setVoltage(Integer voltage) {
        this.voltage = voltage;
    }

    public Boolean getIsConfStatProvided() {
        return this.isConfStatProvided;
    }

    public void setIsConfStatProvided(Boolean isConfStatProvided) {
        this.isConfStatProvided = isConfStatProvided;
    }

    public Person getPersonServicing() {
        return this.personServicing;
    }

    public void setPersonServicing(Person personServicing) {
        this.personServicing = personServicing;
    }

    public Float getStorageVolume() {
        return this.storageVolume;
    }

    public void setStorageVolume(Float storageVolume) {
        this.storageVolume = storageVolume;
    }

    public Float getAmpere() {
        return this.ampere;
    }

    public void setAmpere(Float ampere) {
        this.ampere = ampere;
    }

    public String getTableLabel() {
        return this.tableLabel;
    }

    public void setTableLabel(String tableLabel) {
        this.tableLabel = tableLabel;
    }

    public Boolean getIsIHEISProvided() {
        return this.isIHEISProvided;
    }

    public void setIsIHEISProvided(Boolean isIHEISProvided) {
        this.isIHEISProvided = isIHEISProvided;
    }

    public String getSkype() {
        return this.skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getBlog() {
        return this.blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public SystemInSessionStatus getSystemInSessionStatus() {
        return this.systemInSessionStatus;
    }

    public void setSystemInSessionStatus(SystemInSessionStatus systemInSessionStatus) {
        this.systemInSessionStatus = systemInSessionStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<ObjectInstance> getObjectInstances() {
        return HibernateHelper.getLazyValue(this, "objectInstances", this.objectInstances);
    }

    public void setObjectInstances(Set<ObjectInstance> objectInstances) {
        this.objectInstances = objectInstances;
    }

    public Set<Configuration> getConfigurations() {
        return HibernateHelper.getLazyValue(this, "configurations", this.configurations);
    }

    public void setConfigurations(Set<Configuration> configurations) {
        this.configurations = configurations;
    }

    public Set<ObjectInstanceFile> getObjectInstanceFiles() {
        return HibernateHelper.getLazyValue(this, "objectInstanceFiles", this.objectInstanceFiles);
    }

    public void setObjectInstanceFiles(Set<ObjectInstanceFile> objectInstanceFiles) {
        this.objectInstanceFiles = objectInstanceFiles;
    }

    public Set<SystemInSessionUser> getSystemInSessionUsers() {
        return HibernateHelper.getLazyValue(this, "systemInSessionUsers", this.systemInSessionUsers);
    }

    public Set<DemonstrationSystemInSession> getDemonstrationsSystemInSession() {
        return HibernateHelper.getLazyValue(this, "demonstrationsSystemInSession", this.demonstrationsSystemInSession);
    }

    public List<Demonstration> getDemonstrations() {
        return demonstrations;
    }

    public void setDemonstrations(List<Demonstration> demonstrations) {
        this.demonstrations = demonstrations;
    }

    public boolean isAuthorizedUser(String username, SystemInSession currentSis, TestingSession selectedTestingSession) {
        List<SystemInSession> listSis = getSystemInSessionByUserByTestingSession(username, selectedTestingSession);
        if (listSis != null) {
            for (SystemInSession sis : listSis) {
                if (sis.equals(currentSis)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int compareTo(SystemInSession o) {
        if (o == null) {
            return 1;
        }
        if (o.getSystem() == null) {
            return 1;
        }
        if (getSystem() == null) {
            return 1;
        }
        if (o.getSystem().getKeyword() == null) {
            return 1;
        }
        if (getSystem().getKeyword() == null) {
            return 1;
        }

        return this.getSystem().getKeyword().compareToIgnoreCase(o.getSystem().getKeyword());
    }

    @FilterLabel
    public String getLabel() {
        return system.getKeywordVersion() + " / " + system.getName();
    }

    @Override
    public String toString() {
        return "SystemInSession [system=" + system + ", testingSession=" + testingSession + "]";
    }

    public void addNote(String note) {
        SystemNotes sn = new SystemNotes(new Date(), note);
        ArrayList<SystemNotes> currentNotes = getNotesList();
        currentNotes.add(sn);
        setNotesList(currentNotes);
    }

    public int getNotesNumber() {
        return getNotesList().size();
    }

    public List<SystemInSessionRegistrationStatus> getPossibleRegistrationStatus() {
        if (identity == null) {
            identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        }
        SystemInSessionRegistrationStatus[] values = {};
        if (identity.hasRole(Role.ADMIN)) {
            values = SystemInSessionRegistrationStatus.values();
        } else {
            if (isRegistrationOpened()) {
                values = SystemInSessionRegistrationStatus.getPossibleRegistrationStatusBeforeDeadLine();
            }
        }
        return Arrays.asList(values);
    }

    public void setIdentity(GazelleIdentity identity) {
        this.identity = identity;
    }

    public boolean isRegistrationOpened() {
        return testingSession.isRegistrationOpened();
    }

}
