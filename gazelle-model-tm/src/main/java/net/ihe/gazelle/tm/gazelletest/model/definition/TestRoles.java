/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemActorProfiles;
import net.ihe.gazelle.tm.systems.model.SystemActorProfilesQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Link a test to a Role in test
 * <br />
 * <br />
 * TestRoles possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestRoles</li>
 * <li><b>test</b> : test corresponding to the TestRoles</li>
 * <li><b>RoleInTest</b> : RoleInTest corresponding to the TestRoles</li>
 * <li><b>testOption</b> : testOption corresponding to the TestRoles</li>
 * <li><b>cardMin</b> : cardMin corresponding to the TestRoles</li>
 * <li><b>cardMax</b> : cardMax corresponding to the TestRoles</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                         http://ihe.irisa.fr
 *                         </pre>
 *         <p/>
 *         <pre>
 *                         amiladi@irisa.fr
 *                         </pre>
 * @version 1.0 , 27 janv. 2009
 * @class TestRoles.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

// We need to have test_id and test_participants_id needs to be unique. We cannot have the same test included twice for a given participants_id
@XmlRootElement(name = "testRoles")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_Roles", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"test_id",
        "role_in_test_id"}))
@SequenceGenerator(name = "tm_test_Roles_sequence", sequenceName = "tm_test_Roles_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestRoles extends AuditedObject implements Serializable, Comparable<TestRoles> {


    private static final long serialVersionUID = -2075371831998021787L;
    private static Logger log = LoggerFactory.getLogger(TestRoles.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_Roles_sequence")
    @XmlTransient
    private Integer id;

    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "test_id" , nullable = false ) to be able to sync TestRoles object with GMM
    @JoinColumn(name = "test_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private Test test;

    @ManyToOne
    @JoinColumn(name = "role_in_test_id")
    @Fetch(value = FetchMode.SELECT)
    private RoleInTest roleInTest;

    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "test_option_id" , nullable = false ) to be able to sync TestRoles object with GMM
    @JoinColumn(name = "test_option_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestOption testOption;

    @Column(name = "card_min")
    private Integer cardMin;

    @Column(name = "card_max")
    private Integer cardMax;

    @Column(name = "url")
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    private String url;

    @Column(name = "url_doc")
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    private String urlDocumentation;

    @Column(name = "number_of_tests_to_realize")
    @Min(value = 1)
    private Integer numberOfTestsToRealize;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinTable(name = "tm_meta_test_test_roles", joinColumns = @JoinColumn(name = "test_roles_id"), inverseJoinColumns = @JoinColumn(name = "meta_test_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_roles_id", "meta_test_id"}))
    @Fetch(value = FetchMode.SELECT)
    private MetaTest metaTest;

    @Column(name = "role_rank")
    private Integer roleRank;

    public TestRoles() {

    }

    public TestRoles(Integer cardMin, Integer cardMax) {
        this(null, null, null, cardMin, cardMax, null, null);
        this.numberOfTestsToRealize = 1;
    }

    public TestRoles(Test test, RoleInTest roleInTest, TestOption testOption, Integer cardMin, Integer cardMax,
                     String inURL, String inURLDocumentation) {
        this.test = test;
        this.roleInTest = roleInTest;
        this.testOption = testOption;
        this.cardMin = cardMin;
        this.cardMax = cardMax;
        this.url = inURL;
        this.urlDocumentation = inURLDocumentation;
        this.numberOfTestsToRealize = 1;
    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy a TestRoles and to persist this new object, it is necessary it doesn't have
     * the same identifier.
     *
     * @param inTestRoles
     */

    public TestRoles(TestRoles inTestRoles) {
        if (inTestRoles.getTest() != null) {
            this.test = new Test(inTestRoles.getTest());
        }
        if (inTestRoles.getRoleInTest() != null) {
            this.roleInTest = new RoleInTest(inTestRoles.getRoleInTest());
        }
        if (inTestRoles.getTestOption() != null) {
            this.testOption = inTestRoles.getTestOption();
        }
        if (inTestRoles.getCardMin() != null) {
            this.cardMin = Integer.valueOf(inTestRoles.getCardMin());
        }
        if (inTestRoles.getCardMax() != null) {
            this.cardMax = Integer.valueOf(inTestRoles.getCardMax());
        }
        if (inTestRoles.getUrl() != null) {
            this.url = new String(inTestRoles.getUrl());
        }
        if (inTestRoles.getUrlDocumentation() != null) {
            this.urlDocumentation = new String(inTestRoles.getUrlDocumentation());
        }
        if (inTestRoles.getNumberOfTestsToRealize() != null) {
            this.numberOfTestsToRealize = Integer.valueOf(inTestRoles.getNumberOfTestsToRealize());
        }

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<TestRoles> getTestRolesListForATest(Test test) {
        // Used in JSF
        if (test == null) {
            return null;
        }
        TestRolesQuery query = new TestRolesQuery();
        query.test().eq(test);
        query.roleRank().order(true);
        return query.getListNullIfEmpty();
    }

    public static Integer maxRankForTest(Test test){
        TestRolesQuery query = new TestRolesQuery();
        query.test().eq(test);
        query.roleRank().order(false);
        TestRoles testRoles = query.getUniqueResult();
        if (testRoles != null){
            return testRoles.getRoleRank();
        } else {
            return 0;
        }
    }

    public static void deleteTestRolesWithFind(TestRoles testRoles) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();
        try {
            TestRoles selectedTestRoles = em.find(TestRoles.class, testRoles.getId());
            em.remove(selectedTestRoles);
        } catch (Exception e) {
            log.error("deleteTestRolesWithFind", e);
            throw new Exception("A testRoles cannot be deleted -  id = " + testRoles.getId(), e);
        }
    }

    public static List<TestRoles> getListOfTestsPerSystem(List<TestType> inTestTypes, System sys) {
        return TestRoles.getTestsFiltered(sys, null, null, null, null, inTestTypes, null, null, null, true);
    }

    protected static TestRolesQuery getTestRolesQuery(List<TestType> inTestTypes, TestStatus inTestStatus, Test inTest,
                                                      TestOption inTestOption, System inSystem, Domain inDomain, IntegrationProfile inIntegrationProfile,
                                                      Actor inActor, IntegrationProfileOption inIntegrationProfileOption, Boolean isTested,
                                                      TestPeerType inTestPeerType) {
        TestRolesQuery query = new TestRolesQuery();

        if ((inTestTypes != null) && !inTestTypes.isEmpty()) {
            query.test().testType().in(inTestTypes);
        }

        query.test().testStatus().eqIfValueNotNull(inTestStatus);
        if (inTest != null) {
            query.test().id().eq(inTest.getId());
        }
        query.testOption().eqIfValueNotNull(inTestOption);

        query.roleInTest().testParticipantsList().aipo().systemActorProfiles().system().eqIfValueNotNull(inSystem);

        ActorIntegrationProfileOptionEntity<ActorIntegrationProfileOption> aipoPath = query.roleInTest()
                .testParticipantsList().actorIntegrationProfileOption();
        if (inDomain != null) {
            aipoPath.actorIntegrationProfile().integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }
        aipoPath.actorIntegrationProfile().integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        aipoPath.actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        aipoPath.integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);

        query.roleInTest().testParticipantsList().tested().eqIfValueNotNull(isTested);
        query.test().testPeerType().eqIfValueNotNull(inTestPeerType);
        return query;
    }

    public static List<Test> getTestFiltered(List<TestType> inTestTypes, TestStatus inTestStatus, Test inTest,
                                             TestOption inTestOption, System inSystem, Domain inDomain, IntegrationProfile inIntegrationProfile,
                                             Actor inActor, IntegrationProfileOption inIntegrationProfileOption, Boolean isTested,
                                             TestPeerType inTestPeerType) {
        TestRolesQuery query = getTestRolesQuery(inTestTypes, inTestStatus, inTest, inTestOption, inSystem, inDomain,
                inIntegrationProfile, inActor, inIntegrationProfileOption, isTested, inTestPeerType);
        return query.test().getListDistinct();
    }

    public static List<TestRoles> getTestsFiltered(System inSystem, IntegrationProfile inIntegrationProfile,
                                                   IntegrationProfileOption inIntegrationProfileOption, Actor inActor, Test inTest,
                                                   List<TestType> inTestTypes, TestPeerType inTestPeerType, TestOption inTestOption, TestStatus inTestStatus,
                                                   Boolean inTested) {
        return getTestsFiltered(inSystem, null, inIntegrationProfile, inIntegrationProfileOption, inActor, inTest,
                inTestTypes, inTestPeerType, inTestOption, inTestStatus, inTested);
    }

    public static List<TestRoles> getTestsFiltered(System inSystem, Domain inDomain,
                                                   IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption,
                                                   Actor inActor, Test inTest, List<TestType> inTestTypes, TestPeerType inTestPeerType,
                                                   TestOption inTestOption, TestStatus inTestStatus, Boolean inTested) {
        TestRolesQuery query = getTestRolesQuery(inTestTypes, inTestStatus, inTest, inTestOption, inSystem, inDomain,
                inIntegrationProfile, inActor, inIntegrationProfileOption, inTested, inTestPeerType);
        return query.getList();
    }

    public static List<Actor> getDistinctActors(List<TestRoles> inList) {
        HashSet<Actor> hashSetOfTests = new HashSet<Actor>();
        if (inList == null) {
            return null;
        }

        for (TestRoles tRoles : inList) {
            for (TestParticipants tp : tRoles.getRoleInTest().getTestParticipantsList()) {
                hashSetOfTests.add(tp.getActorIntegrationProfileOption().getActorIntegrationProfile().getActor());
            }
        }

        return new ArrayList<Actor>(hashSetOfTests);
    }

    public static boolean testRolesContainsKeyword(String inString, List<TestRoles> inListRoles) {
        boolean toReturn = false;
        for (TestRoles tr : inListRoles) {
            if (tr.getRoleInTest().getKeyword().equals(inString)) {
                toReturn = true;
                break;
            }
        }

        return toReturn;
    }

    private static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionByTestRolesBySystemInSession(
            SystemInSession inSystemInSession, TestRoles inTestRoles, boolean onlyTestedTestParticipantsIncluded) {
        if (inTestRoles != null) {
            SystemActorProfilesQuery query = new SystemActorProfilesQuery();
            query.system().systemsInSession().id().eq(inSystemInSession.getId());
            query.aipo().testParticipants().roleInTest().testRoles().id().eq(inTestRoles.getId());
            if (onlyTestedTestParticipantsIncluded) {
                query.aipo().testParticipants().tested().eq(true);
            }
            List<ActorIntegrationProfileOption> list = query.actorIntegrationProfileOption().getListDistinct();
            if (list.size() > 0) {
                return list;
            }
        }
        return null;
    }

    public static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionByTestRolesBySystemInSession(
            SystemInSession inSystemInSession, TestRoles inTestRoles) {
        return getActorIntegrationProfileOptionByTestRolesBySystemInSession(inSystemInSession, inTestRoles, true);
    }

    public static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionByTestRolesBySystemInSessionForAllTestParticipants(
            SystemInSession inSystemInSession, TestRoles inTestRoles) {
        return getActorIntegrationProfileOptionByTestRolesBySystemInSession(inSystemInSession, inTestRoles, false);
    }

    private static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionByRoleInTestBySystemInSession(
            SystemInSession inSystemInSession, RoleInTest inRoleInTest, boolean onlyTestedTestParticipantsIncluded) {
        if (inRoleInTest != null) {
            SystemActorProfilesQuery query = new SystemActorProfilesQuery();
            query.system().systemsInSession().id().eq(inSystemInSession.getId());
            query.aipo().testParticipants().roleInTest().id().eq(inRoleInTest.getId());
            if (onlyTestedTestParticipantsIncluded) {
                query.aipo().testParticipants().tested().eq(true);
            }
            List<ActorIntegrationProfileOption> list = query.actorIntegrationProfileOption().getListDistinct();
            if (list.size() > 0) {
                return list;
            }
        }
        return null;
    }

    public static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionByRoleInTestBySystemInSession(
            SystemInSession inSystemInSession, RoleInTest inRoleInTest) {
        return getActorIntegrationProfileOptionByRoleInTestBySystemInSession(inSystemInSession, inRoleInTest, true);
    }

    public static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionByRoleInTestBySystemInSessionForAllTestParticipants(
            SystemInSession inSystemInSession, RoleInTest inRoleInTest) {
        return getActorIntegrationProfileOptionByRoleInTestBySystemInSession(inSystemInSession, inRoleInTest, false);
    }

    public static List<SystemActorProfiles> getSystemActorProfilesByTestRolesBySystemInSession(
            SystemInSession inSystemInSession, TestRoles inTestRoles) {
        if (inTestRoles != null) {
            SystemActorProfilesQuery query = new SystemActorProfilesQuery();
            query.system().systemsInSession().id().eq(inSystemInSession.getId());
            query.aipo().testParticipants().roleInTest().testRoles().id().eq(inTestRoles.getId());
            query.aipo().testParticipants().tested().eq(true);
            List<SystemActorProfiles> list = query.getList();
            if (list.size() > 0) {
                return list;
            }
        }
        return null;
    }

    public static TestRoles getTestRolesByRoleInTestByTest(Test inTest, RoleInTest inRoleInTest) {
        if ((inTest != null) && (inRoleInTest != null)) {
            TestRolesQuery query = new TestRolesQuery();
            query.test().eq(inTest);
            query.roleInTest().eq(inRoleInTest);
            return query.getUniqueResult();
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public RoleInTest getRoleInTest() {
        return roleInTest;
    }

    public void setRoleInTest(RoleInTest roleInTest) {
        this.roleInTest = roleInTest;
    }

    public TestOption getTestOption() {
        return testOption;
    }

    public void setTestOption(TestOption testOption) {
        this.testOption = testOption;
    }

    public Integer getCardMin() {
        if (cardMin == null) {
            return 0;
        }
        return cardMin;
    }

    public void setCardMin(Integer cardMin) {
        this.cardMin = cardMin;
    }

    public Integer getCardMax() {
        if (cardMax == null) {
            return Math.max(1, getCardMin());
        }
        return cardMax;
    }

    public void setCardMax(Integer cardMax) {
        this.cardMax = cardMax;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlDocumentation() {
        return urlDocumentation;
    }

    public void setUrlDocumentation(String urlDocumentation) {
        this.urlDocumentation = urlDocumentation;
    }

    public Integer getNumberOfTestsToRealize() {
        if (numberOfTestsToRealize == null) {
            return 0;
        }
        return numberOfTestsToRealize;
    }

    public void setNumberOfTestsToRealize(Integer numberOfTestsToRealize) {
        this.numberOfTestsToRealize = numberOfTestsToRealize;
    }

    public MetaTest getMetaTest() {
        return HibernateHelper.getLazyValue(this, "metaTest", this.metaTest);
    }

    public void setMetaTest(MetaTest metaTest) {
        this.metaTest = metaTest;
    }

    public List<TestSteps> getTestStepsList() {
        TestStepsQuery query = new TestStepsQuery();
        query.testRolesInitiator().id().eq(this.getId());
        return query.getListNullIfEmpty();
    }

    public Integer getRoleRank(){
        return this.roleRank;
    }

    public void setRoleRank(Integer roleRank){
        this.roleRank = roleRank;
    }

    public TestRoles getTestRolesAtPreviousRank(){
        TestRolesQuery testRolesQuery = new TestRolesQuery();
        testRolesQuery.test().id().eq(this.test.getId());
        testRolesQuery.roleRank().lt(this.roleRank);
        testRolesQuery.roleRank().order(false);
        return testRolesQuery.getUniqueResult();
    }

    public TestRoles getTestRolesAtNextRank(){
        TestRolesQuery testRolesQuery = new TestRolesQuery();
        testRolesQuery.test().id().eq(this.test.getId());
        testRolesQuery.roleRank().gt(this.roleRank);
        testRolesQuery.roleRank().order(true);
        return testRolesQuery.getUniqueResult();
    }


    @Override
    public int compareTo(TestRoles o) {
        return this.getTest().compareTo(o.getTest());
    }

    @Override
    public String toString() {
        return "TestRoles [id=" + id + ", test=" + test + ", roleInTest=" + roleInTest + "]";
    }

    public void saveOrMerge(EntityManager entityManager, Test test) {
        this.id = null;
        this.test = test;
        testOption.setLastModifierId(this.lastModifierId);
        testOption = testOption.saveOrMerge(entityManager);
        roleInTest.setLastModifierId(this.lastModifierId);
        roleInTest.saveOrMerge(entityManager);
        if (metaTest != null) {
            metaTest.setLastModifierId(this.lastModifierId);
            metaTest.saveOrMerge(entityManager);
            if (metaTest.getId() == null) {
                metaTest = null;
            }
        }
        TestRolesQuery query = new TestRolesQuery();
        query.roleInTest().eq(this.roleInTest);
        query.test().eq(this.test);

        List<TestRoles> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

    public static class Comparators {

        public static Comparator<TestRoles> KEYWORD = new Comparator<TestRoles>() {
            @Override
            public int compare(TestRoles tr1, TestRoles tr2) {
                return tr1.getTest().getKeyword().compareTo(tr2.getTest().getKeyword());
            }
        };
    }

}
