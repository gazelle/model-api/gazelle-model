/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

/**
 * @class AbstractConfiguration.java
 * @package net.ihe.gazelle.tm.systems.model
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.configurations.model.interfaces.HTTPConfiguration;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tm_web_service_configuration", schema = "public")
@SequenceGenerator(name = "tm_web_service_configuration_sequence", sequenceName = "tm_web_service_configuration_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class WebServiceConfiguration extends AbstractConfiguration implements Serializable, HTTPConfiguration {

    private static final long serialVersionUID = 1711863131367532507L;

    private static Logger log = LoggerFactory.getLogger(WebServiceConfiguration.class);

    @Column(name = "url", nullable = false)
    private String url;

    @Range(min = 0, max = 65635)
    @Column(name = "port")
    private Integer port;

    @Range(min = 0, max = 65635)
    @Column(name = "port_secured")
    private Integer portSecured;

    @Range(min = 1024, max = 65635)
    @Column(name = "port_proxy")
    private Integer portProxy;

    @XmlElement(name = "httpRewrite")
    @Column(name = "http_rewrite")
    private Boolean httpRewrite = false;

    @Deprecated
    @Column(name = "usage")
    private String usage;

    /**
     * assigningAuthority attribute
     */
    @Column(name = "assigning_authority")
    private String assigningAuthority;

    /**
     * assigningAuthorityOID attribute
     */
    @Deprecated
    @Column(name = "ass_auth_oid")
    private String assigningAuthorityOID;

    /**
     * webServiceDetail attribute
     */
    @Deprecated
    @Column(name = "web_service_detail")
    private String webServiceDetail;

    @Deprecated
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "webservice_type", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private WebServiceType webServiceType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ws_transaction_usage", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private WSTransactionUsage wsTransactionUsage;

    public WebServiceConfiguration() {
        super();
    }

    public WebServiceConfiguration(boolean httpRewrite) {
        super();
        this.httpRewrite = httpRewrite;
    }

    public WebServiceConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public WebServiceConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public WebServiceConfiguration(WebServiceConfiguration ws) {
        super(ws);
        if (ws != null) {
            this.url = ws.url;
            this.port = ws.port;
            this.portSecured = ws.portSecured;
            this.assigningAuthority = ws.assigningAuthority;
            this.wsTransactionUsage = ws.wsTransactionUsage;
            this.httpRewrite = ws.httpRewrite;
            getProxyPortIfNeeded(null);
        }
    }

    public WebServiceConfiguration(Configuration inConfiguration, String url, Integer port, Integer portSecured,
                                   Integer inPortProxy, String usage, String comment, String inAssigningAuthority,
                                   WSTransactionUsage wsTransactionUsage, Boolean hTTPRewrite) {
        super(inConfiguration);
        this.url = url;
        this.port = port;
        this.portSecured = portSecured;
        this.portProxy = inPortProxy;
        this.usage = usage;
        this.comments = comment;
        assigningAuthority = inAssigningAuthority;
        this.wsTransactionUsage = wsTransactionUsage;
        this.httpRewrite = hTTPRewrite;
    }

    @SuppressWarnings("unchecked")
    public static List<AbstractConfiguration> GetListOfConfigurations(TestingSession inTestingSession,
                                                                      GenericOIDSConfigurationForSession inGenericConf) {

        if (inTestingSession == null) {
            return null;
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        List<AbstractConfiguration> listOfConfigurations = null;

        try {
            StringBuffer sb = new StringBuffer();

            String className = WebServiceConfiguration.class.getCanonicalName();
            String stringAliasOfClass = className.substring(className.lastIndexOf(".") + 1, className.length())
                    .toLowerCase();
            sb.append("select "
                    + stringAliasOfClass
                    + " FROM "
                    + className
                    + " "
                    + stringAliasOfClass
                    + " WHERE "
                    + stringAliasOfClass
                    + ".configuration.systemInSession.testingSession=:inTestingSession and webServiceType.profile= :integrationProfile");
            Query q = entityManager.createQuery(sb.toString());

            q.setParameter("inTestingSession", inTestingSession);
            q.setParameter("integrationProfile", inGenericConf.getProfileFor());
            listOfConfigurations = q.getResultList();
        } catch (Exception e) {
            log.error("GetListOfConfigurations", e);
        }

        return listOfConfigurations;
    }

    //TODO we need to return a List<AbstractConfiguration> instead of List<WebServiceConfiguration>
    public static List<WebServiceConfiguration> GetListOfConfigurations1(TestingSession inTestingSession,
                                                                         GenericOIDSConfigurationForSession inGenericConf) {

        if (inTestingSession == null) {
            return null;
        }

        List<WebServiceConfiguration> listOfConfigurations = null;

        try {

            AbstractConfigurationQuery q1 = new AbstractConfigurationQuery();
            q1.configuration().systemInSession().testingSession().eq(inTestingSession);

            WebServiceConfigurationQuery q = new WebServiceConfigurationQuery();
            q.configuration().systemInSession().testingSession().eq(inTestingSession);
            q.webServiceType().profile().eq(inGenericConf.getProfileFor());


            listOfConfigurations = q.getList();
        } catch (Exception e) {
            log.error("GetListOfConfigurations", e);
        }

        return listOfConfigurations;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public Integer getPortSecured() {
        return portSecured;
    }

    @Override
    public void setPortSecured(Integer portSecured) {
        this.portSecured = portSecured;
    }

    public Boolean getHttpRewrite() {
        return httpRewrite;
    }

    public void setHttpRewrite(Boolean httpRewrite) {
        this.httpRewrite = httpRewrite;
    }

    public Integer getPortIfConfNotSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == false)) {
            return port;
        }
        return null;
    }

    public Integer getPortSecuredIfConfSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == true)) {
            return portSecured;
        }
        return null;
    }

    @Override
    public Integer getPortProxy() {
        return portProxy;
    }

    @Override
    public void setPortProxy(Integer portProxy) {
        this.portProxy = portProxy;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    @Override
    public String getAssigningAuthority() {
        return assigningAuthority;
    }

    @Override
    public void setAssigningAuthority(String assigningAuthority) {
        this.assigningAuthority = assigningAuthority;
    }

    public String getAssigningAuthorityOID() {
        return assigningAuthorityOID;
    }

    public void setAssigningAuthorityOID(String inassigningAuthorityOID) {
        this.assigningAuthorityOID = inassigningAuthorityOID;
    }

    public String getWebServiceDetail() {
        return webServiceDetail;
    }

    public void setWebServiceDetail(String webServiceDetail) {
        this.webServiceDetail = webServiceDetail;
    }

    public String getWebServiceDetailTransaction() {
        if (wsTransactionUsage != null) {
            if (wsTransactionUsage.getTransaction() != null) {
                return wsTransactionUsage.getTransaction().getKeyword();
            }
        }
        return null;
    }

    public String getWebServiceDetailCode() {
        if (wsTransactionUsage != null) {
            if (wsTransactionUsage.getUsage() != null) {
                if (!wsTransactionUsage.getUsage().equals("")) {
                    String k[] = wsTransactionUsage.getUsage().split(":");
                    if (k.length == 2) {
                        return k[1];
                    }
                }
            }
        }
        return null;
    }

    public String getWebServiceDetailTitle() {
        if (wsTransactionUsage != null) {
            if (wsTransactionUsage.getUsage() != null) {
                if (!wsTransactionUsage.getUsage().equals("")) {
                    String k[] = wsTransactionUsage.getUsage().split(":");
                    if (k.length == 1) {
                        return k[0];
                    }
                }
            }
        }
        return null;
    }

    public String getUsageFromWSTransactionUsage() {
        if (wsTransactionUsage != null) {
            return wsTransactionUsage.getUsage();
        }
        return null;
    }

    public String getUsageTransFromWSTransactionUsage() {
        if (wsTransactionUsage != null) {
            return wsTransactionUsage.getTransaction().getKeyword() + ":" + wsTransactionUsage.getUsage();
        }
        return null;
    }

    public WebServiceType getWebServiceType() {
        return webServiceType;
    }

    public void setWebServiceType(WebServiceType webServiceType) {
        this.webServiceType = webServiceType;
    }

    @Override
    public WSTransactionUsage getWsTransactionUsage() {
        return wsTransactionUsage;
    }

    @Override
    public void setWsTransactionUsage(WSTransactionUsage wsTransactionUsage) {
        this.wsTransactionUsage = wsTransactionUsage;
    }

    public String getWSTranactionUsageAsText() {
        if (this.wsTransactionUsage != null) {
            return this.wsTransactionUsage.getTransaction().getKeyword() + ":"
                    + this.getWsTransactionUsage().getUsage();
        }
        return null;
    }

    public String generateAssigningAuthorityOID(GenericOIDSConfigurationForSession config, TestingSession selectedTestingSession) {

        if (selectedTestingSession == null) {
            return assigningAuthorityOID;
        }

        if (config == null) {
            return assigningAuthorityOID;
        }

        if (webServiceType.getNeedsOID()) {
            assigningAuthorityOID = config.getOidForSession() + "." + config.getOidRootValue() + "."
                    + config.getCurrentOIDValue();
            config.incrementOID();
        }

        return assigningAuthorityOID;

    }

    public void generateAssigningAuthorityOID(TestingSession selectedTestingSession) {
        if (this.getWebServiceType() != null) {
            IntegrationProfile profile = this.getWebServiceType().getProfile();
            if (profile != null) {

                GenericOIDSConfigurationForSession oidConfigurationsForWS = GenericOIDSConfigurationForSession
                        .getConfigurationParameterForSessionForProfile(selectedTestingSession, profile);
                if (oidConfigurationsForWS != null) {
                    generateAssigningAuthorityOID(oidConfigurationsForWS, selectedTestingSession);
                }
            }
        }

    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> headers = super.getCSVHeaders();
        addCSVHTTPHeaders(this, headers);
        return headers;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> values = super.getCSVValues();
        addCSVHTTPValues(this, values);
        return values;
    }

}
