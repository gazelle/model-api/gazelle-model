/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.gazelletest.model.reversed.AIPO;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * <b>Class Description : </b>TestInstanceParticipants<br>
 * <br>
 * This class describes the TestInstanceParticipants object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * TestInstanceParticipants possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestInstanceParticipants</li>
 * <li><b>test</b> : test corresponding to the TestInstanceParticipants</li>
 * <li><b>systemInSession</b> : systemInSession corresponding to the TestInstanceParticipants</li>
 * <li><b>status</b> : status corresponding to the TestInstanceParticipants</li>
 * <li><b>actor</b> : actor corresponding to the TestInstanceParticipants</li>
 * <li><b>integrationProfile</b> : integrationProfile corresponding to the TestInstanceParticipants</li>
 * <li><b>IntegrationProfileOption</b> : IntegrationProfileOption corresponding to the TestInstanceParticipants</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                                                 http://ihe.irisa.fr
 *                                                 </pre>
 *
 *         <pre>
 *                                                 amiladi@irisa.fr
 *                                                 </pre>
 * @version 1.0 , 27 janv. 2009
 * @class TestInstanceParticipants.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "TestInstanceParticipant")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "tm_test_instance_participants", schema = "public")
@SequenceGenerator(name = "tm_test_instance_participants_sequence", sequenceName = "tm_test_instance_participants_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestInstanceParticipants extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 2562301184472924420L;

    private static Logger log = LoggerFactory.getLogger(TestInstanceParticipants.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_instance_participants_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "test_instance_id")
    @Fetch(FetchMode.SELECT)
    private TestInstance testInstance;

    @ManyToOne
    @JoinColumn(name = "system_in_session_user_id")
    @Fetch(FetchMode.SELECT)
    @XmlElement(name = "systemInSessionUser")
    private SystemInSessionUser systemInSessionUser;

    @ManyToOne
    @JoinColumn(name = "status_id")
    @Fetch(FetchMode.SELECT)
    private TestInstanceParticipantsStatus status;

    @ManyToOne
    @JoinColumn(name = "role_in_test_id")
    @Fetch(FetchMode.SELECT)
    @XmlElement(name = "roleInTest")
    private RoleInTest roleInTest;

    @ManyToOne
    @JoinColumn(name = "actor_integration_profile_option_id")
    @Fetch(FetchMode.SELECT)
    private ActorIntegrationProfileOption actorIntegrationProfileOption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actor_integration_profile_option_id", insertable = false, updatable = false)
    @Fetch(FetchMode.SELECT)
    private AIPO aipo;

    public TestInstanceParticipants() {

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<Test> getListOfTestsForInstances(TestType inTestType) {
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.testInstance().test().testType().eq(inTestType);
        List<Test> res = q.testInstance().test().getListDistinct();
        Collections.sort(res);
        return res;
    }

    //TODO
    public static List<Institution> getListOfInstitutionsForInstances(TestType inTestType) {
        EntityManager em = EntityManagerService.provideEntityManager();

        Query q = em.createQuery(
                "select distinct instSys.institution from TestInstanceParticipants tip, InstitutionSystem instSys "
                        + "where tip.systemInSessionUser.systemInSession.system = instSys.system  "
                        + "and tip.testInstance.test.testType = :inTestType order by instSys.institution.keyword asc");
        q.setParameter("inTestType", inTestType);

        return q.getResultList();
    }

    public static List<TestInstanceParticipants> getTestInstanceParticipantsListForATest(TestInstance testInstance) {
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.testInstance().eq(testInstance);
        List<TestInstanceParticipants> list = q.getList();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static void deleteTestParticipantsWithFind(TestInstanceParticipants testInstanceParticipants)
            throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();
        try {
            TestInstanceParticipants selectedTestInstanceParticipants = em
                    .find(TestInstanceParticipants.class, testInstanceParticipants.getId());
            em.remove(selectedTestInstanceParticipants);
        } catch (Exception e) {
            log.error("deleteTestParticipantsWithFind", e);
            throw new Exception(
                    "A testInstanceParticipants cannot be deleted -  id = " + testInstanceParticipants.getId(), e);
        }
    }

    public static List<TestInstanceParticipants> getTestInstancesParticipantsForATest(TestRoles t,
                                                                                      SystemInSession sis) {
        if ((t == null) || (sis == null)) {
            return null;
        }
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.testInstance().test().eq(t.getTest());
        q.roleInTest().eq(t.getRoleInTest());
        q.systemInSessionUser().systemInSession().eq(sis);

        List<TestInstanceParticipants> list = q.getList();

        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static List<TestInstanceParticipants> getTestInstancesParticipantsForATestWithStatus(TestRoles t,
                                                                                                SystemInSession sis, Status status) {
        if ((sis == null) || (status == null)) {
            return null;
        }
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.systemInSessionUser().systemInSession().eq(sis);
        q.testInstance().lastStatus().eq(status);
        if (t != null) {
            q.roleInTest().eq(t.getRoleInTest());
            q.testInstance().test().eq(t.getTest());
        }

        List<TestInstanceParticipants> list = q.getList();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static long getTestInstancesParticipantsNumberForATestWithStatus(TestRoles t, SystemInSession sis,
                                                                            Status status) {
        if ((sis == null) || (status == null)) {
            return 0;
        }

        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.systemInSessionUser().systemInSession().eq(sis);
        q.testInstance().lastStatus().eq(status);
        if (t != null) {
            q.roleInTest().eq(t.getRoleInTest());
            q.testInstance().test().eq(t.getTest());
        }

        return q.getCount();
    }

    @Deprecated
    public static List<Status> getStatusOfTestParticipantsForTest(TestRoles t, SystemInSession sis) {
        if ((t == null) || (sis == null)) {
            return null;
        }

        EntityManager em = EntityManagerService.provideEntityManager();

        Query query = em.createQuery(
                "SELECT tip.testInstance.lastStatus FROM TestInstanceParticipants tip WHERE tip.testInstance.test=:test and tip.roleInTest=:rit  and tip.systemInSessionUser.systemInSession=:sis");

        query.setParameter("test", t.getTest());
        query.setParameter("rit", t.getRoleInTest());
        query.setParameter("sis", sis);

        List<Status> list = query.getResultList();

        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    //TODO We need to replace getListDistinct() by getList()
    public static List<Status> getStatusOfTestParticipantsForTest1(TestRoles t, SystemInSession sis) {
        if ((t == null) || (sis == null)) {
            return null;
        }
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.systemInSessionUser().systemInSession().eq(sis);
        q.roleInTest().eq(t.getRoleInTest());
        q.testInstance().test().eq(t.getTest());

        List<Status> list = q.testInstance().lastStatus().getListDistinct();

        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    /**
     * Return the list of instances depending of the session/ Institution / Test / Status TestingSession inSession , Institution inInstitution , System inSystem , Test inTest , IntegrationProfile
     * inIntegrationProfile , IntegrationProfileOption inIntegrationProfileOption , Actor inActor , TestOption inTestOption , Status inTestStatus
     *
     * @param inSession
     * @return
     */
    //TODO we need to rewrite HQLQueryBuilder
    static List<TestInstanceParticipants> getTestInstancesParticipantsFiltered(boolean distinct,
                                                                               TestingSession inSession, Institution inInstitution, System inSystem, Test inTest, List<TestType> testTypes,
                                                                               IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption, Actor inActor,
                                                                               Status inTestStatus, TestInstance inTestInstance) {

        TestInstanceParticipantsQuery query = new TestInstanceParticipantsQuery();

        query.systemInSessionUser().systemInSession().testingSession().eqIfValueNotNull(inSession);
        query.systemInSessionUser().systemInSession().system().institutionSystems().institution()
                .eqIfValueNotNull(inInstitution);
        query.systemInSessionUser().systemInSession().system().eqIfValueNotNull(inSystem);
        query.testInstance().test().eqIfValueNotNull(inTest);
        if ((testTypes != null) && !testTypes.isEmpty()) {
            query.testInstance().test().testType().in(testTypes);
        }

        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile()
                .eqIfValueNotNull(inIntegrationProfile);
        query.actorIntegrationProfileOption().integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().eqIfValueNotNull(inActor);

        query.testInstance().lastStatus().eqIfValueNotNull(inTestStatus);
        query.testInstance().eqIfValueNotNull(inTestInstance);

        if (!distinct) {
            HQLQueryBuilder<TestInstanceParticipants> queryBuilder = (HQLQueryBuilder<TestInstanceParticipants>) query
                    .getQueryBuilder();
            StringBuilder sb = new StringBuilder("select ").append(queryBuilder.getShortProperty("this_"));
            return queryBuilder.getListWithProvidedFrom(sb);
        } else {
            return query.getList();
        }
    }

    public static List<TestInstanceParticipants> getTestInstancesParticipantsFiltered(TestingSession inSession,
                                                                                      Institution inInstitution, System inSystem, Test inTest, List<TestType> testTypes,
                                                                                      IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption, Actor inActor,
                                                                                      Status inTestStatus) {
        return getTestInstancesParticipantsFiltered(true, inSession, inInstitution, inSystem, inTest, testTypes,
                inIntegrationProfile, inIntegrationProfileOption, inActor, inTestStatus, null);
    }

    public static List<TestInstanceParticipants> getTestInstancesParticipantsFilteredNotDistinct(
            TestingSession inSession, Institution inInstitution, System inSystem, Test inTest,
            List<TestType> inTestTypes, IntegrationProfile inIntegrationProfile,
            IntegrationProfileOption inIntegrationProfileOption, Actor inActor, Status inTestStatus) {
        return getTestInstancesParticipantsFiltered(false, inSession, inInstitution, inSystem, inTest, inTestTypes,
                inIntegrationProfile, inIntegrationProfileOption, inActor, inTestStatus, null);
    }

    public static void deleteTestParticipantForSystemInSession(SystemInSession sis) {
        EntityManager em = EntityManagerService.provideEntityManager();

        List<TestInstanceParticipants> list = getTestInstancesParticipantsFiltered(true, sis.getTestingSession(), null,
                sis.getSystem(), null, null, null, null, null, null, null);

        for (TestInstanceParticipants tip : list) {
            try {
                em.remove(tip);

                em.remove(tip.systemInSessionUser);

                tip.getTestInstance().deleteTestInstance();

                em.flush();

            } catch (ConstraintViolationException e) {
                log.error("deleteTestParticipantForSystemInSession", e);
                log.warn("[deleteTestParticipantForSystemInSession]Impossible to delete " + e.getMessage());
            }
        }
    }

    public static List<SystemInSession> getSystemInSessionListByTestInstanceByRoleInTest(TestInstance inTestInstance,
                                                                                         RoleInTest inRoleInTest) {
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        if (inTestInstance != null) {
            q.testInstance().eq(inTestInstance);
        }
        if (inRoleInTest != null) {
            q.roleInTest().eq(inRoleInTest);
        }
        List<SystemInSession> list = q.systemInSessionUser().systemInSession().getListDistinct();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static List<TestInstanceParticipants> getTestInstanceParticipantsListByTestInstanceByRoleInTest(
            TestInstance inTestInstance, RoleInTest inRoleInTest) {
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        if (inTestInstance != null) {
            q.testInstance().eq(inTestInstance);
        }
        if (inRoleInTest != null) {
            q.roleInTest().eq(inRoleInTest);
        }
        List<TestInstanceParticipants> list = q.getListDistinct();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static void deleteTestInstanceParticipantsByRoleInTestByTestInstance(TestInstance inTestInstance,
                                                                                RoleInTest inRoleInTest) {
        EntityManager em = EntityManagerService.provideEntityManager();
        if ((inTestInstance != null) && (inRoleInTest != null)) {
            List<TestInstanceParticipants> testInstanceParticipantsList = TestInstanceParticipants
                    .getTestInstanceParticipantsListByTestInstanceByRoleInTest(inTestInstance, inRoleInTest);
            if (testInstanceParticipantsList != null) {
                for (TestInstanceParticipants testInstanceParticipants : testInstanceParticipantsList) {
                    em.remove(testInstanceParticipants);
                }
            }
        }
    }

    public static Integer getTestInstanceListSizeForSystemInSessionByStatus(SystemInSession inSystemInSession,
                                                                            Status inStatus) {
        if (inSystemInSession == null) {
            return 0;
        } else {
            TestInstanceQuery query = new TestInstanceQuery();
            query.lastStatus().eq(inStatus);
            query.test().testType().in(inSystemInSession.getTestingSession().getTestTypes());
            query.testInstanceParticipants().systemInSessionUser().systemInSession().eq(inSystemInSession);
            return query.getCount();
        }
    }

    public static TestInstanceParticipants getTestInstanceParticipantsByTestInstanceBySiSByRoleInTest(
            TestInstance inTestInstance, SystemInSession inSystemInSession, RoleInTest inRoleTest) {
        try {
            TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
            q.testInstance().eq(inTestInstance);
            q.roleInTest().eq(inRoleTest);
            q.systemInSessionUser().systemInSession().eq(inSystemInSession);
            List<TestInstanceParticipants> listTIP = q.getListDistinct();
            if (listTIP.size() == 1) {
                return listTIP.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Error getTestInstanceParticipantsByTestInstanceBySiSByRoleInTest ! ");
            log.error("inTestInstance  = " + inTestInstance.getId());
            log.error("inSystemInSession  = " + inSystemInSession.getId());
            log.error("inRoleTest  = " + inRoleTest.getId());
            log.error("", e);
        }
        return null;

    }

    @Deprecated
    public static List<TestInstanceParticipants> getTestInstanceParticipantsBySiSByRoleInTest(
            SystemInSession inSystemInSession, TestRoles inTestRoles, RoleInTest systemRoleInTest) {
        try {
            EntityManager em = EntityManagerService.provideEntityManager();
            Query query = em.createQuery(
                    "SELECT DISTINCT tip FROM TestInstanceParticipants tip,TestInstanceParticipants tip2 "
                            + "WHERE tip.roleInTest=:inRoleInTest " + "AND tip2.testInstance=tip.testInstance "
                            + "AND tip.testInstance.test=:inTest " + "AND tip2 !=tip "
                            + "AND tip2.roleInTest=:systemRoleInTest "
                            + "AND tip2.systemInSessionUser.systemInSession=:inSessionInSession");
            query.setParameter("inSessionInSession", inSystemInSession);
            query.setParameter("inRoleInTest", inTestRoles.getRoleInTest());
            query.setParameter("systemRoleInTest", systemRoleInTest);
            query.setParameter("inTest", inTestRoles.getTest());
            List<TestInstanceParticipants> list = query.getResultList();
            if (list.size() > 0) {
                return list;
            }

        } catch (Exception e) {
            log.error("Error getTestInstanceParticipantsBySiSByRoleInTest ! ");
            log.error("inRoleInTest  = " + inTestRoles.getRoleInTest().getId());
            log.error("inSystemInSession  = " + inSystemInSession.getId());
            log.error("inTest  = " + inTestRoles.getTest().getId());
            log.error("", e);
        }
        return null;
    }

    //TODO need to review this query
    public static List<TestInstanceParticipants> getTestInstanceParticipantsBySiSByRoleInTest1(
            SystemInSession inSystemInSession, TestRoles inTestRoles, RoleInTest systemRoleInTest) {
        try {
            TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
            q.roleInTest().eq(systemRoleInTest);
            q.testInstance().test().eq(inTestRoles.getTest());
            q.systemInSessionUser().systemInSession().eq(inSystemInSession);

            List<TestInstanceParticipants> list = q.getListDistinct();
            if (list.size() > 0) {
                return list;
            }

        } catch (Exception e) {
            log.error("Error getTestInstanceParticipantsBySiSByRoleInTest ! ");
            log.error("inRoleInTest  = " + inTestRoles.getRoleInTest().getId());
            log.error("inSystemInSession  = " + inSystemInSession.getId());
            log.error("inTest  = " + inTestRoles.getTest().getId());
            log.error("", e);
        }
        return null;
    }

    public static List<TestInstanceParticipants> getTestInstanceParticipantsByTestInstanceBySystemInSession(
            TestInstance inTestInstance, SystemInSession inSystemInSession) {

        try {
            TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
            q.systemInSessionUser().systemInSession().eq(inSystemInSession);
            q.testInstance().eq(inTestInstance);
            List<TestInstanceParticipants> list = q.getListDistinct();
            if (list.size() > 0) {
                return list;
            }
        } catch (Exception e) {
            log.error("Error getTestInstanceParticipantsByTestInstanceBySystemInSession ! ");
            log.error("inTestInstance  = " + inTestInstance.getId());
            log.error("inSystemInSession  = " + inSystemInSession.getId());
            log.error("", e);
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestInstance getTestInstance() {
        return testInstance;
    }

    public void setTestInstance(TestInstance testInstance) {
        this.testInstance = testInstance;
    }

    public SystemInSessionUser getSystemInSessionUser() {
        return systemInSessionUser;
    }

    public void setSystemInSessionUser(SystemInSessionUser systemInSessionUser) {
        this.systemInSessionUser = systemInSessionUser;
    }

    public TestInstanceParticipantsStatus getStatus() {
        return status;
    }

    public void setStatus(TestInstanceParticipantsStatus status) {
        this.status = status;
    }

    public ActorIntegrationProfileOption getActorIntegrationProfileOption() {
        return actorIntegrationProfileOption;
    }

    public void setActorIntegrationProfileOption(ActorIntegrationProfileOption actorIntegrationProfileOption) {
        this.actorIntegrationProfileOption = actorIntegrationProfileOption;
    }

    public RoleInTest getRoleInTest() {
        return roleInTest;
    }

    public void setRoleInTest(RoleInTest roleInTest) {
        this.roleInTest = roleInTest;
    }

    public AIPO getAipo() {
        return HibernateHelper.getLazyValue(this, "aipo", this.aipo);
    }

}
