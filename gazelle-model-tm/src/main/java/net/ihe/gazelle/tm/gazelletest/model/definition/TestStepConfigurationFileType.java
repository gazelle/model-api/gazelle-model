package net.ihe.gazelle.tm.gazelletest.model.definition;

/**
 * Defined File Types, as of Mar. 03, 2014, are:
 * <ul>
 * <li>VALIDATION CONTEXT</li>
 * <li>CODE TABLE</li>
 * <li>DATA SHEET</li>
 * <li>VALUE SETS</li>
 * <li>BASE VALIDATION PROFILE</li>
 * <li>EXAMPLE MESSAGE</li>
 * </ul>
 *
 * @author rizwan.tanoli
 */
public enum TestStepConfigurationFileType {

    VALIDATION_CONTEXT,
    CODE_TABLE,
    DATA_SHEET,
    VALUE_SETS,
    BASE_VALIDATION_PROFILE,
    EXAMPLE_MESSAGE;

    public static TestStepConfigurationFileType getConfigurationFileType(String fileType) throws Exception {
        TestStepConfigurationFileType type = null;

        for (TestStepConfigurationFileType val : values()) {
            if (val.name().equalsIgnoreCase(fileType)) {
                type = val;
            }
        }

        if (type == null) {
            throw new Exception("Invalid Configuration File Type specified.");
        } else {
            return type;
        }
    }
}
