package net.ihe.gazelle.tm.gazelletest.model.reversed;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MonitorTestId implements java.io.Serializable {

    private static final long serialVersionUID = -2722741520812300119L;

    @Column(name = "monitor_id", nullable = false)
    private int monitorId;

    @Column(name = "test_id", nullable = false)
    private int testId;

    public MonitorTestId() {
    }

    public MonitorTestId(int monitorId, int testId) {
        this.monitorId = monitorId;
        this.testId = testId;
    }

    public int getMonitorId() {
        return this.monitorId;
    }

    public void setMonitorId(int monitorId) {
        this.monitorId = monitorId;
    }

    public int getTestId() {
        return this.testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + monitorId;
        result = (prime * result) + testId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MonitorTestId other = (MonitorTestId) obj;
        if (monitorId != other.monitorId) {
            return false;
        }
        if (testId != other.testId) {
            return false;
        }
        return true;
    }

}
