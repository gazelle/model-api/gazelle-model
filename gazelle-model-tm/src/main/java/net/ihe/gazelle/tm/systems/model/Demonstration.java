/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.users.model.Iso3166CountryCode;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>Demonstration<br>
 * <br>
 * This class describes the Demonstration object, used by the Gazelle application. It contains a list of demonstrations.
 * <p/>
 * Demonstration table has the following attributes :
 * <ul>
 * <li><b>id</b> : The Demonstration id</li>
 * <li><b>name</b> : The demonstration name (example : HIMSS 2009)</li>
 * <li><b>description</b> : A brief description of the demonstration</li>
 * <li><b>country</b> : country where the demonstration will be held</li>
 * <li><b>url</b> : URL of the demonstration</li>
 * </ul>
 * </br> <br>
 *
 * @author Xiaojun Ding (MIR - St Louis USA) - Jean-Renan Chatel (INRIA - Rennes FR)
 * @version 1.0 - 2008, July 2
 * @class Demonstration.java
 * @package net.ihe.gazelle.tm.systems.model
 */

@Entity
@Table(name = "tm_demonstration", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "tm_demonstration_sequence", sequenceName = "tm_demonstration_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Demonstration extends AuditedObject implements java.io.Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(Demonstration.class);

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911131541280660L;

    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_demonstration_sequence")
    private Integer id;

    @NotNull(message = "demonstration name is required")
    private String name;

    @Size(max = 1024)
    private String description;

    @ManyToOne
    @JoinColumn(name = "country")
    @Fetch(value = FetchMode.SELECT)
    private Iso3166CountryCode iso3166CountryCode;

    @Column(name = "beginning_date")
    private Date beginningDemonstration;

    @Column(name = "ending_date")
    private Date endingDemonstration;

    @Column(name = "url")
    private String url;

    @Column(name = "is_active")
    private Boolean active;

    /* A demonstration defines many systems in session */
    @ManyToMany
    @JoinTable(name = "tm_demonstration_system_in_session", joinColumns = @JoinColumn(name = "demonstration_id"), inverseJoinColumns = @JoinColumn(name = "system_in_session_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "demonstration_id", "system_in_session_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<SystemInSession> systemsInSession;

    /* A demonstration is mapped with one or many testing sessions */
    @ManyToMany
    @JoinTable(name = "tm_demonstrations_in_testing_sessions", joinColumns = @JoinColumn(name = "demonstration_id"), inverseJoinColumns = @JoinColumn(name = "testing_session_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "demonstration_id", "testing_session_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestingSession> testingSessions;

    // Constructors
    public Demonstration() {
    }

    public Demonstration(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Demonstration(Integer id, String name, String description, Iso3166CountryCode iso3166CountryCode, String url) {
        this.id = id;
        this.name = name;
        this.iso3166CountryCode = iso3166CountryCode;
        this.url = url;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    // Static methods
    public static List<Demonstration> getActiveDemonstrationsForSession(TestingSession testingSession) {
        if (testingSession == null) {
            return null;
        }
        DemonstrationQuery q = new DemonstrationQuery();
        q.testingSessions().id().eq(testingSession.getId());
        q.active().eq(true);
        List<Demonstration> demos = q.getList();
        if ((demos != null) && !demos.isEmpty()) {
            return demos;
        } else {
            return null;
        }
    }

    /**
     * if activated is null, returns all demonstrations, otherwise return activated/deactivated demonstrations
     *
     * @param activated
     * @return
     */
    public static List<Demonstration> getAllDemonstrations(Boolean activated) {
        DemonstrationQuery q = new DemonstrationQuery();
        if (activated != null) {
            q.addRestriction(q.active().eqRestriction(activated));
        }
        List<Demonstration> demonstrations = q.getList();
        if ((demonstrations != null) && !demonstrations.isEmpty()) {
            return demonstrations;
        } else {
            return null;
        }
    }

    /**
     * Delete a demonstration (with a find method calling entityManager)
     *
     * @param selectedDemonstration : Demonstration to delete
     * @param em                    : EntityManager of this session
     */
    public static void deleteDemonstrationWithFind(Demonstration selectedDemonstration, EntityManager em) {
        // Delete the demonstration
        Demonstration demo = em.find(Demonstration.class, selectedDemonstration.getId());
        em.remove(demo);
    }

    /**
     * @param inName
     * @return
     */
    public static Demonstration getDemonstrationByName(String inName) {
        DemonstrationQuery q = new DemonstrationQuery();
        q.addRestriction(q.name().likeRestriction(inName));
        List<Demonstration> demonstrations = q.getList();
        if ((demonstrations != null) && !demonstrations.isEmpty()) {
            return demonstrations.get(0);
        } else {
            return null;
        }
    }

    /**
     * @param demonstrationId
     * @return
     */
    public static Demonstration getDemonstrationById(Integer demonstrationId) {
        DemonstrationQuery q = new DemonstrationQuery();
        q.id().eq(demonstrationId);
        return q.getUniqueResult();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Iso3166CountryCode getIso3166CountryCode() {
        return iso3166CountryCode;
    }

    public void setIso3166CountryCode(Iso3166CountryCode iso3166CountryCode) {
        this.iso3166CountryCode = iso3166CountryCode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getBeginningDemonstration() {
        return beginningDemonstration;
    }

    public void setBeginningDemonstration(Date beginningDemonstration) {
        this.beginningDemonstration = beginningDemonstration;
    }

    public Date getEndingDemonstration() {
        return endingDemonstration;
    }

    public void setEndingDemonstration(Date endingDemonstration) {
        this.endingDemonstration = endingDemonstration;
    }

    public List<SystemInSession> getSystemsInSession() {
        return systemsInSession;
    }

    public void setSystemsInSession(List<SystemInSession> systemsInSession) {
        this.systemsInSession = systemsInSession;
    }

    public List<TestingSession> getTestingSessions() {
        return testingSessions;
    }

    public void setTestingSessions(List<TestingSession> testingSessions) {
        this.testingSessions = testingSessions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Demonstration that = (Demonstration) o;

        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (iso3166CountryCode != null ? !iso3166CountryCode.equals(that.iso3166CountryCode) : that.iso3166CountryCode != null) {
            return false;
        }
        if (beginningDemonstration != null ? !beginningDemonstration.equals(that.beginningDemonstration) : that.beginningDemonstration != null) {
            return false;
        }
        if (endingDemonstration != null ? !endingDemonstration.equals(that.endingDemonstration) : that.endingDemonstration != null) {
            return false;
        }
        if (url != null ? !url.equals(that.url) : that.url != null) {
            return false;
        }
        if (lastChanged != null ? !lastChanged.equals(that.lastChanged) : that.lastChanged != null) {
            return false;
        }
        if (lastModifierId != null ? !lastModifierId.equals(that.lastModifierId) : that.lastModifierId != null) {
            return false;
        }
        return active != null ? active.equals(that.active) : that.active == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (iso3166CountryCode != null ? iso3166CountryCode.hashCode() : 0);
        result = 31 * result + (beginningDemonstration != null ? beginningDemonstration.hashCode() : 0);
        result = 31 * result + (endingDemonstration != null ? endingDemonstration.hashCode() : 0);
        result = 31 * result + (lastChanged != null ? lastChanged.hashCode() : 0);
        result = 31 * result + (lastModifierId != null ? lastModifierId.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }
}
