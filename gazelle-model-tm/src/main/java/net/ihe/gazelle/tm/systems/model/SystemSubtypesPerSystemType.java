/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * <b>Class Description : </b>SystemSubtypesPerSystemType<br>
 * <br>
 * This class describes the relation between a System Type and a System SubType. This relation corresponds to an object, it is used by the Gazelle application.
 * <p/>
 * SystemSubtypesPerSystemType possesses the following attributes :
 * <ul>
 * <li><b>Id</b> : Id corresponding to the association SystemType / SystemSubType</li>
 * <li><b>systemTypeId</b> : System type Id associated to the System</li>
 * <li><b>systemSubtypesPerSystemType</b> : SystemSubtypesPerSystemType Id (combination of system type and system subtypes) associated to the System</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 23
 * @class SystemSubtypesPerSystemType.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "sys_system_subtypes_per_system_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_type_id", "system_subtype_id"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "sys_system_subtypes_per_system_type_sequence", sequenceName = "sys_system_subtypes_per_system_type_id_seq", allocationSize = 1)
@Exchanged(set = SetRegistration.class)
public class SystemSubtypesPerSystemType extends AuditedObject implements java.io.Serializable,
        Comparable<SystemSubtypesPerSystemType> {

    public static final String SYSTEM_SUBTYPES_PER_SYSTEM_TYPE_ID = " SystemSubtypesPerSystemType : [id=";
    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450900631531282710L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sys_system_subtypes_per_system_type_sequence")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name = "system_type_id", nullable = false)
    private SystemType systemType;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name = "system_subtype_id", nullable = true)
    private SystemSubType systemSubType;

    // Constructors

    public SystemSubtypesPerSystemType() {
    }

    public SystemSubtypesPerSystemType(Integer id) {
        super();
        this.id = id;
    }

    public SystemSubtypesPerSystemType(SystemType systemType, SystemSubType systemSubType) {

        this.systemType = systemType;
        this.systemSubType = systemSubType;

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemType getSystemType() {
        return systemType;
    }

    public void setSystemType(SystemType systemType) {
        this.systemType = systemType;
    }

    public SystemSubType getSystemSubType() {
        return systemSubType;
    }

    public void setSystemSubType(SystemSubType systemSubType) {
        this.systemSubType = systemSubType;
    }

    // *********************************************************
    // hashCode and equals methods
    // *********************************************************

    @Override
    public String toString() {

        if (systemType == null) {
            if (systemSubType == null) {
                return "";
            } else {
                return SYSTEM_SUBTYPES_PER_SYSTEM_TYPE_ID + id + ", systemSubType=" + systemSubType + "]";
            }
        } else {
            if (systemSubType == null) {
                return SYSTEM_SUBTYPES_PER_SYSTEM_TYPE_ID + id + " systemType=" + systemType.getSystemTypeKeyword()
                        + "]";
            } else {
                return SYSTEM_SUBTYPES_PER_SYSTEM_TYPE_ID + id + ", systemSubType="
                        + systemSubType.getSystemSubTypeKeyword() + ", systemType=" + systemType.getSystemTypeKeyword()
                        + "]";
            }
        }

    }

    public String getSelectableLabel() {
        String systemTypeString = "";
        if (systemType != null) {
            systemTypeString = systemType.getSystemTypeKeyword();
        }
        if (systemSubType != null) {
            return systemTypeString + " - " + systemSubType.getSystemSubTypeKeyword();
        } else {
            return systemTypeString;
        }
    }

    @Override
    public int compareTo(SystemSubtypesPerSystemType o) {
        SystemType st1 = getSystemType();
        SystemType st2 = o.getSystemType();
        if (!st1.getSystemTypeKeyword().equals(st2.getSystemTypeKeyword())) {
            return st1.getSystemTypeKeyword().compareTo(st2.getSystemTypeKeyword());
        }
        SystemSubType sst1 = getSystemSubType();
        SystemSubType sst2 = o.getSystemSubType();
        if (sst1 == null) {
            return -1;
        }
        if (sst2 == null) {
            return 1;
        }
        return sst1.getSystemSubTypeKeyword().compareTo(sst2.getSystemSubTypeKeyword());
    }

}
