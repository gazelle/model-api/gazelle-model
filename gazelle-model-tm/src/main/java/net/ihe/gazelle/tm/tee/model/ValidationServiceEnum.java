/**
 *
 */
package net.ihe.gazelle.tm.tee.model;

/**
 * @author rchitnis
 */
public enum ValidationServiceEnum {


    GAZELLEHL7V2VALIDATOR("Gazelle HL7v2 Validator");

    private String description;

    ValidationServiceEnum(String srv) {
        this.setDescription(srv);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String name) {
        this.description = name;
    }


}
