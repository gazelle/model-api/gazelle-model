package net.ihe.gazelle.tm.systems.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(TestingSessionAdminKey.class)
@Table(name = "tm_testing_session_admin")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestingSessionAdmin implements Serializable {

    private static final long serialVersionUID = -4798379124721119074L;

    @Id
    @Column(name = "user_id")
    private String userId;
    @Id
    @Column(name = "testing_session_id")
    private Integer testingSessionId;

    public TestingSessionAdmin() {
    }

    public TestingSessionAdmin(String userId) {
        this.userId = userId;
    }

    public TestingSessionAdmin(String userId, Integer testingSessionId) {
        this.userId = userId;
        this.testingSessionId = testingSessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getTestingSessionId() {
        return testingSessionId;
    }

    public void setTestingSessionId(Integer testingSessionId) {
        this.testingSessionId = testingSessionId;
    }
}
