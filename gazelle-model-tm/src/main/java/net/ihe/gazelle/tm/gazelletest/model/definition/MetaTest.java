package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tm.systems.model.System;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "MetaTest")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_meta_test", schema = "public")
@SequenceGenerator(name = "tm_meta_test_sequence", sequenceName = "tm_meta_test_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class MetaTest extends AuditedObject implements Serializable, Comparable<MetaTest> {


    public static final int MAX_KEYWORD_LENGTH = 64;
    public static final int MAX_DESCRIPTION_LENGTH = 512;
    private static final long serialVersionUID = 5803673825666471395L;
    private static Logger log = LoggerFactory.getLogger(MetaTest.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_meta_test_sequence")
    private Integer id;

    @Column(name = "keyword", unique = true, nullable = false)
    @Size(max = MAX_KEYWORD_LENGTH)
    private String keyword;

    @Column(name = "short_description", nullable = false)
    @Size(max = MAX_DESCRIPTION_LENGTH)
    private String description;

    @OneToMany(fetch = FetchType.LAZY)
    @AuditJoinTable
    @JoinTable(name = "tm_meta_test_test_roles", joinColumns = @JoinColumn(name = "meta_test_id"), inverseJoinColumns = @JoinColumn(name = "test_roles_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "meta_test_id", "test_roles_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestRoles> testRolesList;

    public MetaTest(String keyword, String description) {
        this.keyword = keyword;
        this.description = description;
    }

    public MetaTest() {

    }

    public static List<MetaTest> getAllMetaTest() {
        MetaTestQuery query = new MetaTestQuery();
        return query.getListNullIfEmpty();

    }

    protected static TestRolesQuery getQuery(MetaTest inMetaTest, System inSystem, Domain inDomain,
                                             IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption,
                                             Actor inActor, Test inTest, List<TestType> testTypes, TestPeerType inTestPeerType, TestOption inTestOption,
                                             TestStatus inTestStatus, Boolean inTested) {
        TestRolesQuery query = new TestRolesQuery();
        query.metaTest().eqIfValueNotNull(inMetaTest);
        query.roleInTest().testParticipantsList().aipo().systemActorProfiles().system().eqIfValueNotNull(inSystem);
        if (inDomain != null) {
            query.roleInTest().testParticipantsList().actorIntegrationProfileOption().actorIntegrationProfile()
                    .integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }
        query.roleInTest().testParticipantsList().actorIntegrationProfileOption().actorIntegrationProfile()
                .integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        query.roleInTest().testParticipantsList().actorIntegrationProfileOption().integrationProfileOption()
                .eqIfValueNotNull(inIntegrationProfileOption);
        query.roleInTest().testParticipantsList().actorIntegrationProfileOption().actorIntegrationProfile().actor()
                .eqIfValueNotNull(inActor);
        query.test().eqIfValueNotNull(inTest);
        if ((testTypes != null) && !testTypes.isEmpty()) {
            query.test().testType().in(testTypes);
        }
        query.test().testPeerType().eqIfValueNotNull(inTestPeerType);
        query.testOption().eqIfValueNotNull(inTestOption);
        query.test().testStatus().eqIfValueNotNull(inTestStatus);
        query.roleInTest().testParticipantsList().tested().eqIfValueNotNull(inTested);
        return query;
    }

    public static List<TestRoles> getTestRolesFiltered(MetaTest inMetaTest, System inSystem, Domain inDomain,
                                                       IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption,
                                                       Actor inActor, Test inTest, List<TestType> testTypes, TestPeerType inTestPeerType, TestOption inTestOption,
                                                       TestStatus inTestStatus, Boolean inTested) {

        TestRolesQuery query = getQuery(inMetaTest, inSystem, inDomain, inIntegrationProfile,
                inIntegrationProfileOption, inActor, inTest, testTypes, inTestPeerType, inTestOption, inTestStatus,
                inTested);

        return query.getList();
    }

    public static List<MetaTest> getMetaTestFiltered(System inSystem, Domain inDomain,
                                                     IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption,
                                                     Actor inActor, Test inTest, List<TestType> testTypes, TestPeerType inTestPeerType, TestOption inTestOption,
                                                     TestStatus inTestStatus, Boolean inTested) {

        TestRolesQuery query = getQuery(null, inSystem, inDomain, inIntegrationProfile, inIntegrationProfileOption,
                inActor, inTest, testTypes, inTestPeerType, inTestOption, inTestStatus, inTested);
        return query.metaTest().getListDistinct();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TestRoles> getTestRolesList() {
        return HibernateHelper.getLazyValue(this, "testRolesList", this.testRolesList);
    }

    public void setTestRolesList(List<TestRoles> testRolesList) {
        this.testRolesList = testRolesList;
    }

    @Override
    public int compareTo(MetaTest o) {
        return 0;
    }

    public boolean emptyMetaTest() {
        if ((description == null) && (keyword == null)) {
            return true;
        }
        return false;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        if (!emptyMetaTest()) {
            MetaTestQuery query = new MetaTestQuery();
            query.keyword().eq(this.keyword);
            query.description().eq(this.description);

            List<MetaTest> listDistinct;
            listDistinct = query.getListDistinct();
            if (listDistinct.size() == 1) {
                this.id = listDistinct.get(0).id;
                entityManager.merge(this);
            } else {
                    entityManager.persist(this);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
