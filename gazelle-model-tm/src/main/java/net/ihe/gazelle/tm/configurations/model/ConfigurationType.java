/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @class ConfigurationType.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "configurationType")
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Audited
@Table(name = "tm_configuration_type", schema = "public")
@SequenceGenerator(name = "tm_configuration_type_sequence", sequenceName = "tm_configuration_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ConfigurationType extends AuditedObject implements Serializable, Comparable<ConfigurationType> {


    private static final long serialVersionUID = 1L;

    /**
     * Id attribute!
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_configuration_type_sequence")
    private Integer id;

    /**
     * Name used for this type of configuration
     */
    @XmlAttribute(name = "name")
    @Column(name = "type_name")
    private String typeName;

    @XmlAttribute(name = "category")
    @Column(name = "category")
    private String category;

    @XmlAttribute(name = "className")
    @Column(name = "class_name", nullable = true, unique = true)
    private String className;

    @XmlAttribute(name = "usedForProxy")
    @Column(name = "used_for_proxy")
    private Boolean usedForProxy;

    @XmlTransient
    @OneToMany(mappedBy = "configurationType", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    private Set<Configuration> configurations;

    public static ConfigurationType GetConfigurationTypeByClassName(String className)
            throws ClassNotFoundException {
        Class.forName(className);
        ConfigurationTypeQuery query = new ConfigurationTypeQuery();
        query.className().eq(className);
        return query.getUniqueResult();
    }

    @SuppressWarnings("unchecked")
    public static ConfigurationType GetConfigurationTypeByClass(Class class_) throws ClassNotFoundException {
        ConfigurationTypeQuery query = new ConfigurationTypeQuery();
        query.className().eq(class_.getCanonicalName());
        return query.getUniqueResult();
    }

    @SuppressWarnings("unchecked")
    public static List<ConfigurationType> GetConfigurationTypeCompatibleWithAProxy() {
        ConfigurationTypeQuery query = new ConfigurationTypeQuery();
        query.usedForProxy().eq(true);
        return query.getListDistinct();
    }

    @SuppressWarnings("unchecked")
    public static List<ConfigurationType> GetAllConfigurationType() {
        ConfigurationTypeQuery query = new ConfigurationTypeQuery();
        query.typeName().order(true);
        return query.getListDistinct();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Set<Configuration> getConfigurations() {
        return HibernateHelper.getLazyValue(this, "configurations", this.configurations);
    }

    public void setConfigurations(Set<Configuration> configurations) {
        this.configurations = configurations;
    }

    public String getClassName() {
        return className;
    }

    /**
     * Test with Class.forName if the given parameter exist
     *
     * @param className
     * @throws ClassNotFoundException
     */
    public void setClassName(String className) throws ClassNotFoundException {
        Class.forName(className);

        this.className = className;
    }

    public Boolean getUsedForProxy() {
        return usedForProxy;
    }

    public void setUsedForProxy(Boolean usedForProxy) {
        this.usedForProxy = usedForProxy;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int compareTo(ConfigurationType o) {
        return getTypeName().compareTo(o.getTypeName());
    }

}
