package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>Status<br>
 * <br>
 * This class describes the TestInstanceParticipantsStatus object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestInstanceParticipantsStatus possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestInstanceParticipantsStatus</li>
 * <li><b>keyword</b> : keyword of the TestInstanceParticipantsStatus.</li>
 * <li><b>description</b> : description of the TestInstanceParticipantsStatus</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the TestInstanceParticipantsStatus</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 27 janv. 2009
 * @class Status.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@Entity
@Table(name = "tm_test_instance_participants_status", schema = "public")
@SequenceGenerator(name = "tm_test_instance_participants_status_sequence", sequenceName = "tm_test_instance_participants_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class TestInstanceParticipantsStatus implements Serializable {


    private static final long serialVersionUID = 1L;

    private final static String STATUS_PASSED_STRING = "verified";
    private final static String STATUS_FAILED_STRING = "partially verified";
    private final static String STATUS_UNVERIFIED_STRING = "unverified";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_instance_participants_status_sequence")
    private Integer id;

    @Column(name = "keyword")
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    public TestInstanceParticipantsStatus() {
    }

    public TestInstanceParticipantsStatus(TestInstanceParticipantsStatus inTestInstanceParticipantsStatus) {
        if (inTestInstanceParticipantsStatus.getDescription() != null) {
            this.description = inTestInstanceParticipantsStatus.getDescription();
        }
        if (inTestInstanceParticipantsStatus.getKeyword() != null) {
            this.keyword = inTestInstanceParticipantsStatus.getKeyword();
        }
        if (inTestInstanceParticipantsStatus.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestInstanceParticipantsStatus.getLabelToDisplay();
        }
    }

    public TestInstanceParticipantsStatus(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public static TestInstanceParticipantsStatus getSTATUS_PASSED() {
        return TestInstanceParticipantsStatus.getStatusByKeyword(STATUS_PASSED_STRING);
    }

    public static TestInstanceParticipantsStatus getSTATUS_FAILED() {
        return TestInstanceParticipantsStatus.getStatusByKeyword(STATUS_FAILED_STRING);
    }

    public static TestInstanceParticipantsStatus getSTATUS_UNVERIFIED() {
        return TestInstanceParticipantsStatus.getStatusByKeyword(STATUS_UNVERIFIED_STRING);
    }

    public static TestInstanceParticipantsStatus getStatusByKeyword(String keyword) {
        if (keyword != null) {
            TestInstanceParticipantsStatusQuery q = new TestInstanceParticipantsStatusQuery();
            q.setCachable(true);
            q.keyword().eq(keyword);

            List<TestInstanceParticipantsStatus> tps = q.getList();

            if ((tps == null) || (tps.size() != 1)) {
                return null;
            }

            return tps.get(0);
        }
        return null;

    }

    public static List<TestInstanceParticipantsStatus> getListStatus() {
        TestInstanceParticipantsStatusQuery q = new TestInstanceParticipantsStatusQuery();
        q.setCachable(true);
        return q.getList();
    }

    public Integer getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
