/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.sync.SetGazelle;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>GazelleLanguage<br>
 * <br>
 * This class describes the GazelleLanguage object, used by the Gazelle application. This class belongs to the Users module.
 * <p/>
 * GazelleLanguage possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the GazelleLanguage</li>
 * <li><b>keyword</b> : keyword corresponding to the GazelleLanguage</li>
 * <li><b>description</b> : description corresponding to the GazelleLanguage</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 14 mai 2009
 * @class GazelleLanguage.java
 * @package net.ihe.gazelle.users.model
 */

@XmlRootElement(name = "GazelleLanguage")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "usr_gazelle_language", schema = "public")
@SequenceGenerator(name = "usr_gazelle_language_sequence", sequenceName = "usr_gazelle_language_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class GazelleLanguage extends AuditedObject implements Serializable, Comparable<GazelleLanguage> {

    private static final long serialVersionUID = 3991719817326532526L;

    private static final String ENGLISH_LANGUAGE_DESCRIPTION = "EN";

    private static Logger log = LoggerFactory.getLogger(GazelleLanguage.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_gazelle_language_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "keyword", unique = true, nullable = false)
    private String keyword;

    @Column(name = "description", unique = true, nullable = false)
    private String description;

    @OneToMany(mappedBy = "gazelleLanguage", fetch = FetchType.LAZY)
    @XmlTransient
    private List<TestDescription> testDescriptions;

    public GazelleLanguage() {

    }

    public GazelleLanguage(String keyword, String description) {
        this.keyword = keyword;
        this.description = description;
    }

    /**
     * Copy Constructor
     *
     * @param inGazelleLanguage
     */

    public GazelleLanguage(GazelleLanguage inGazelleLanguage) {
        if (inGazelleLanguage.getKeyword() != null) {
            this.setKeyword(inGazelleLanguage.getKeyword());
        }
        if (inGazelleLanguage.getDescription() != null) {
            this.setKeyword(inGazelleLanguage.getDescription());
        }
    }

    public static List<GazelleLanguage> getLanguageList() {
        return new GazelleLanguageQuery().getList();
    }

    public static GazelleLanguage getGazelleLanguageByDescription(String description) {
        GazelleLanguageQuery q = new GazelleLanguageQuery();
        q.description().eq(description);
        q.setCachable(true);
        List<GazelleLanguage> list = q.getList();

        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public static GazelleLanguage getDefaultLanguage() {
        String defaultGazelleLanguage = PreferenceService.getString("default_gazelle_language");
        if (StringUtils.trimToNull(defaultGazelleLanguage) == null) {
            defaultGazelleLanguage = ENGLISH_LANGUAGE_DESCRIPTION;
        }
        return getGazelleLanguageByDescription(defaultGazelleLanguage);
    }

    public static GazelleLanguage getGazelleLanguageDescriptionIgnoringCase(String keyword) {
        List<GazelleLanguage> gazelleLanguageList = GazelleLanguage.getLanguageList();
        if ((gazelleLanguageList != null) && (gazelleLanguageList.size() > 0)) {
            for (GazelleLanguage gazelleLanguage : gazelleLanguageList) {
                if (gazelleLanguage.getDescription().equalsIgnoreCase(keyword)) {
                    return gazelleLanguage;
                }
            }
        }
        return null;
    }

    public List<TestDescription> getTestDescriptions() {
        return testDescriptions;
    }

    public void setTestDescriptions(List<TestDescription> testDescriptions) {
        this.testDescriptions = testDescriptions;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GazelleLanguage [id=" + id + ", keyword=" + keyword + ", description=" + description + "]";
    }

    @Override
    public int compareTo(GazelleLanguage o) {
        return getDescription().compareTo(o.getDescription());
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        GazelleLanguageQuery query = new GazelleLanguageQuery();
        query.description().eq(this.description);
        query.keyword().eq(this.keyword);
        List<GazelleLanguage> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
