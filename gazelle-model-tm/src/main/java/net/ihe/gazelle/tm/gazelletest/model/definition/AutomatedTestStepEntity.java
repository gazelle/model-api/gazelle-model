package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Audited
@Table(name = "tm_automated_test_step", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@PrimaryKeyJoinColumn(name = "test_steps_id")
public class AutomatedTestStepEntity extends TestSteps {

    private static final long serialVersionUID = -4757100297769307345L;

    @Column(name = "test_script_id", nullable = false)
    private String testScriptID;

    @OneToMany(mappedBy = "automatedTestStep", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<AutomatedTestStepInput> inputs;

    public AutomatedTestStepEntity() {
        // empty for database
    }

    public AutomatedTestStepEntity(TestStepDomain domain) {
        super(domain);
        this.testScriptID = domain.getTestScriptID();
        this.inputs = new ArrayList<>();
        for (AutomatedTestStepInputDomain inputDomain : domain.getInputs()) {
            this.inputs.add(new AutomatedTestStepInput(inputDomain, this));
        }
    }

    @Override
    public TestStepDomain asDomain() {
        TestStepDomain domain = super.asDomain();
        domain.setType(TestStepType.AUTOMATED);
        domain.setTestScriptID(this.testScriptID);
        domain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());
        for (AutomatedTestStepInput input : this.inputs) {
            domain.addInput(input.asDomain());
        }
        return domain;
    }

    public String getTestScriptID() {
        return testScriptID;
    }

    public void setTestScriptID(String testScriptID) {
        this.testScriptID = testScriptID;
    }

    public List<AutomatedTestStepInput> getInputs() {
        if (inputs != null) {
            return new ArrayList<>(inputs);
        } else {
            return null;
        }
    }

    public void setInputs(List<AutomatedTestStepInput> inputs) {
        this.inputs = new ArrayList<>(inputs);
    }
}
