package net.ihe.gazelle.tm.systems.model;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import net.ihe.gazelle.hql.FilterLabelProvider;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.translate.TranslateService;

import java.util.Arrays;
import java.util.Collection;

public enum IntegrationStatementStatus implements
        FilterLabelProvider {

    CREATED("created", "", false, false, false, false),

    PUBLISHED("published", "", true, true, true, false),

    VERIFIED("verified", "", true, false, true, true),

    REJECTED("rejected", "", false, false, false, true),

    UNREACHABLE("unreachable", "", false, false, true, false),

    UNMATCHING("unmatching", "", true, true, true, false);

    private static final Predicate<IntegrationStatementStatus> PREDICATE_VISIBLE = new PredicateVisible();
    private static final Predicate<IntegrationStatementStatus> PREDICATE_WAITING = new PredicateWaiting();
    private static final Predicate<IntegrationStatementStatus> PREDICATE_CRAWLING = new PredicateCrawling();
    private static final Predicate<IntegrationStatementStatus> PREDICATE_ADMIN_STATUS = new PredicateAdminStatus();
    public static IntegrationStatementStatus[] visibles = getStatuses(PREDICATE_VISIBLE);
    public static IntegrationStatementStatus[] waitings = getStatuses(PREDICATE_WAITING);
    public static IntegrationStatementStatus[] crawlings = getStatuses(PREDICATE_CRAWLING);
    public static IntegrationStatementStatus[] adminStatuses = getStatuses(PREDICATE_ADMIN_STATUS);
    private static String VISIBLE_MESSAGE = "gazelle.productregistry.system.VisibleIntoSearches";
    private static String NOT_VISIBLE_MESSAGE = "gazelle.productregistry.system.NotVisibleIntoSearches";
    private String keyword;
    private String image;
    private boolean visible;
    private boolean waitingForValidation;
    private boolean needCrawling;
    private boolean adminStatus;

    IntegrationStatementStatus(String keyword, String image, boolean visible, boolean waitingForValidation,
                               boolean needCrawling, boolean adminStatus) {
        this.keyword = keyword;
        this.image = image;
        this.visible = visible;
        this.waitingForValidation = waitingForValidation;
        this.needCrawling = needCrawling;
        this.adminStatus = adminStatus;
    }

    private static IntegrationStatementStatus[] getStatuses(Predicate<IntegrationStatementStatus> predicate) {
        IntegrationStatementStatus[] result;
        Collection<IntegrationStatementStatus> visibleList = Collections2.filter(Arrays.asList(values()), predicate);
        result = visibleList.toArray(new IntegrationStatementStatus[visibleList.size()]);
        return result;
    }

    private static HQLRestriction getRestriction(HQLSafePathBasic<IntegrationStatementStatus> statusPath,
                                                 IntegrationStatementStatus[] statuses) {
        HQLRestriction[] restrictions = new HQLRestriction[statuses.length];
        for (int i = 0; i < statuses.length; i++) {
            restrictions[i] = statusPath.eqRestriction(statuses[i]);
        }
        return HQLRestrictions.or(restrictions);
    }

    public static HQLRestriction systemIntegrationStatementVisible(SystemAttributes systemAttributes) {
        return getRestriction(systemAttributes.prStatus(), visibles);
    }

    public static HQLRestriction systemIntegrationStatementWaitingForValidation(SystemAttributes systemAttributes) {
        return getRestriction(systemAttributes.prStatus(), waitings);
    }

    public static HQLRestriction systemIntegrationStatementNeedCrawling(SystemAttributes systemAttributes) {
        return getRestriction(systemAttributes.prStatus(), crawlings);
    }

    @Override
    public String getSelectableLabel() {
        return TranslateService.getTranslation("gazelle.productregistry.status." + keyword);
    }

    public String getLabel() {
        return getSelectableLabel();
    }

    public String getDetails() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getSelectableLabel());
        stringBuilder.append(" - ");
        if (visible) {
            stringBuilder.append(TranslateService.getTranslation(VISIBLE_MESSAGE));
        } else {
            stringBuilder.append(TranslateService.getTranslation(NOT_VISIBLE_MESSAGE));
        }
        return stringBuilder.toString();
    }

    public boolean isVisible() {
        return visible;
    }

    public boolean isWaitingForValidation() {
        return waitingForValidation;
    }

    public boolean isNeedCrawling() {
        return needCrawling;
    }

    public boolean isAdminStatus() {
        return adminStatus;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getImage() {
        return image;
    }

    private static class PredicateVisible implements Predicate<IntegrationStatementStatus> {
        @Override
        public boolean apply(IntegrationStatementStatus input) {
            return (input != null) && input.isVisible();
        }

        @Override
        public boolean equals(Object object) {
            return object == PREDICATE_VISIBLE;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    private static class PredicateWaiting implements Predicate<IntegrationStatementStatus> {
        @Override
        public boolean apply(IntegrationStatementStatus input) {
            return (input != null) && input.isWaitingForValidation();
        }

        @Override
        public boolean equals(Object object) {
            return object == PREDICATE_WAITING;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    private static class PredicateCrawling implements Predicate<IntegrationStatementStatus> {
        @Override
        public boolean apply(IntegrationStatementStatus input) {
            return (input != null) && input.isNeedCrawling();
        }

        @Override
        public boolean equals(Object object) {
            return object == PREDICATE_CRAWLING;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

    }

    private static class PredicateAdminStatus implements Predicate<IntegrationStatementStatus> {
        @Override
        public boolean apply(IntegrationStatementStatus input) {
            return (input != null) && input.isAdminStatus();
        }

        @Override
        public boolean equals(Object object) {
            return object == PREDICATE_CRAWLING;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

}
