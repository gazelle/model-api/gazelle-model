/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model.HL7;

import net.ihe.gazelle.tm.configurations.model.Configuration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "tm_hl7_initiator_configuration", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class HL7V2InitiatorConfiguration extends AbstractHL7Configuration {

    private static final long serialVersionUID = 1984069555336195202L;

    public HL7V2InitiatorConfiguration() {
        super();
    }

    public HL7V2InitiatorConfiguration(Configuration inConfiguration, String inSendingReceivingApplication,
                                       String inSendingReceivingFacility, String inAssigningAuthority, String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
    }

    public HL7V2InitiatorConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public HL7V2InitiatorConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public HL7V2InitiatorConfiguration(HL7V2InitiatorConfiguration hl7v2InitiatorConfiguration) {
        super(hl7v2InitiatorConfiguration);
    }

    public HL7V2InitiatorConfiguration(Configuration inConfiguration, String inSendingReceivingApplication,
                                       String inSendingReceivingFacility, String inAssigningAuthority, String inAssigningAuthorityOID,
                                       String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> headers = super.getCSVHeaders();
        headers.add("sending application");
        headers.add("sending facility");
        return headers;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> values = super.getCSVValues();
        if (this.getSendingReceivingApplication() != null) {
            values.add(this.getSendingReceivingApplication());
        } else {
            values.add("");
        }
        if (this.getSendingReceivingFacility() != null) {
            values.add(this.getSendingReceivingFacility());
        } else {
            values.add("");
        }
        return values;
    }

}
