/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.LabelKeywordDescriptionClass;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>SystemInSessionStatus<br>
 * <br>
 * This class describes the SystemInSessionStatus object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * SystemInSessionStatus possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the SystemInSessionStatus</li>
 * <li><b>keyword</b> : keyword of the SystemInSessionStatus.</li>
 * <li><b>description</b> : description of the SystemInSessionStatus</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the SystemInSessionStatus</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 29 juin 2009
 * @class SystemInSessionStatus.java
 * @package net.ihe.gazelle.tm.systems.model
 */

@Entity
@Table(name = "tm_system_in_session_status", schema = "public")
@SequenceGenerator(name = "tm_system_in_session_status_sequence", sequenceName = "tm_system_in_session_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class SystemInSessionStatus extends LabelKeywordDescriptionClass implements Serializable {


    private static final long serialVersionUID = 1L;
    private final static String STATUS_READY_STRING = "ready";
    private final static String STATUS_BUSY_TESTING_STRING = "busy testing";
    private final static String STATUS_BUSY_CONFIGURING_STRING = "busy configuring";
    private final static String STATUS_FINISHED_FOR_TODAY_STRING = "finished for today";
    private final static String STATUS_INSTALLING_STRING = "installing";
    private final static String STATUS_NOT_HERE_YET_STRING = "not here yet";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_in_session_status_sequence")
    private Integer id;

    public SystemInSessionStatus() {
        super();
    }

    public SystemInSessionStatus(LabelKeywordDescriptionClass inLabelKeywordDescriptionClass) {
        super(inLabelKeywordDescriptionClass);
    }
    public SystemInSessionStatus(String keyword, String labelToDisplay, String description) {
        this(keyword, labelToDisplay, description, null);
    }

    public SystemInSessionStatus(String keyword, String labelToDisplay, String description, Integer rank) {
        super(keyword, labelToDisplay, description, rank);
    }

    public static SystemInSessionStatus getSTATUS_READY() {
        return SystemInSessionStatus.getStatusByKeyword(SystemInSessionStatus.STATUS_READY_STRING);
    }

    public static SystemInSessionStatus getSTATUS_BUSY_TESTING() {
        return SystemInSessionStatus.getStatusByKeyword(STATUS_BUSY_TESTING_STRING);
    }

    public static SystemInSessionStatus getSTATUS_BUSY_CONFIGURING() {
        return SystemInSessionStatus.getStatusByKeyword(STATUS_BUSY_CONFIGURING_STRING);
    }

    public static SystemInSessionStatus getSTATUS_FINISHED_FOR_TODAY() {
        return SystemInSessionStatus.getStatusByKeyword(STATUS_FINISHED_FOR_TODAY_STRING);
    }

    public static SystemInSessionStatus getSTATUS_INSTALLING() {
        return SystemInSessionStatus.getStatusByKeyword(STATUS_INSTALLING_STRING);
    }

    public static SystemInSessionStatus getSTATUS_NOT_HERE_YET() {
        return SystemInSessionStatus.getStatusByKeyword(STATUS_NOT_HERE_YET_STRING);
    }

    public static SystemInSessionStatus getStatusByKeyword(String status) {
        SystemInSessionStatusQuery query = new SystemInSessionStatusQuery();
        query.keyword().eq(status);
        return query.getUniqueResult();
    }

    public static List<SystemInSessionStatus> getListStatus() {
        SystemInSessionStatusQuery query = new SystemInSessionStatusQuery();
        query.keyword().order(true);
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
