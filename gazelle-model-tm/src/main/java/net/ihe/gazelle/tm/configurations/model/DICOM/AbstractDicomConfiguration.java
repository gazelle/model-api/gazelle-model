/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model.DICOM;

import net.ihe.gazelle.tm.configurations.model.AbstractConfiguration;
import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.SopClass;
import net.ihe.gazelle.tm.configurations.model.interfaces.ServerConfiguration;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Locale;

@MappedSuperclass
public abstract class AbstractDicomConfiguration extends AbstractConfiguration implements ServerConfiguration {

    private static final long serialVersionUID = -5056625819601391084L;
    private static int MAX_LENGTH_FOR_AETITLE = 16;
    /**
     * ae_title attribute , Application Entity Title - The representation used to identify the DICOM nodes communicating between each other.
     */
    @Column(name = "ae_title")
    @Pattern(regexp = "[A-Za-z0-9_]{1,16}", message = "AE Title should be formatted with letters and _, size max is 16 characters")
    @NotNull
    @Size(max = 16)
    private String aeTitle;
    /****************************************************************************
     * sopClass attributes <br/>
     * The information object and the service class are the two fundamental components of DICOM.<br/>
     * An understanding of these components makes it possible to comprehend, at least at a functional level, what DICOM does and why it is so useful.<br/>
     * Information objects define the core contents of medical imaging, and service classes define what to do with those contents.<br/>
     * The service classes and information objects are combined to form the functional units of DICOM.<br/>
     * This combination is called a service-object pair, or SOP. Since DICOM is an object-oriented standard, the combination is actually called a service-object pair class, or SOP class.<br/>
     * The SOP class is the elemental unit of DICOM; everything that DICOM does when implemented is based on the use of SOP classes.
     */
    @ManyToOne
    @JoinColumn(name = "sop_class_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private SopClass sopClass;

    /**
     * Transfer role attributes
     */
    @Column(name = "transfer_role")
    @NotNull
    private String transferRole;

    /**
     * modalityType attribute
     */
    @Column(name = "modality_type")
    private String modalityType;

    @Range(min = 0, max = 65635)
    @Column(name = "port")
    private Integer port;

    @Column(name = "port_secure")
    @Range(min = 0, max = 65635)
    private Integer portSecured;

    @Column(name = "port_proxy")
    @Range(min = 0, max = 65635)
    private Integer portProxy;

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new DicomConfiguration object.
     */
    public AbstractDicomConfiguration() {
        super();
    }

    public AbstractDicomConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public AbstractDicomConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public AbstractDicomConfiguration(AbstractDicomConfiguration inAbstractDicomConf) {
        super(inAbstractDicomConf);
        if (inAbstractDicomConf != null) {
            this.aeTitle = inAbstractDicomConf.aeTitle;
            this.modalityType = inAbstractDicomConf.modalityType;
            this.port = inAbstractDicomConf.port;
            this.portSecured = inAbstractDicomConf.portSecured;
            this.sopClass = inAbstractDicomConf.sopClass;
            this.transferRole = inAbstractDicomConf.transferRole;
            getProxyPortIfNeeded(null);
        }
    }

    public AbstractDicomConfiguration(Configuration inConfiguration, String inAETitle, Integer inPort,
                                      Integer inPortSecure, Integer inPortProxy, SopClass inSopClass, String inTransfertRole) {
        super(inConfiguration);
        aeTitle = aeTitlePostTreatment(inAETitle);
        port = inPort;
        portSecured = inPortSecure;
        sopClass = inSopClass;
        transferRole = inTransfertRole;
        portProxy = inPortProxy;
    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * get the AE Title for the configuration
     *
     * @return a String aeTitle
     */
    public String getAeTitle() {
        return aeTitle;
    }

    /**
     * set the AE Title for the configuration
     *
     * @param inAeTitle a string
     */
    public void setAeTitle(final String inAeTitle) {
        this.aeTitle = inAeTitle;
    }

    /**
     * get the Sop class
     *
     * @return sopClass
     */
    public SopClass getSopClass() {
        return sopClass;
    }

    /**
     * set a new sop Class
     *
     * @param inSopClass a String
     */
    public void setSopClass(final SopClass inSopClass) {
        this.sopClass = inSopClass;
    }

    /**
     * get the Transfer Role for the current configuration
     *
     * @return transferRole a String
     */
    public String getTransferRole() {
        return transferRole;
    }

    /**
     * set a new Transfer
     *
     * @param transferRole a String
     */
    public void setTransferRole(final String inTransferRole) {
        this.transferRole = inTransferRole;
    }

    /**
     * get the modality type
     *
     * @return modalityType
     */
    public String getModalityType() {
        return modalityType;
    }

    /**
     * set a new modality type
     *
     * @param inModalityType
     */
    public void setModalityType(final String inModalityType) {
        this.modalityType = inModalityType;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public Integer getPortSecured() {
        return portSecured;
    }

    @Override
    public void setPortSecured(Integer portSecured) {
        this.portSecured = portSecured;
    }

    public Integer getPortIfConfNotSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == false)) {
            return port;
        }
        return null;
    }

    public Integer getPortSecuredIfConfSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == true)) {
            return portSecured;
        }
        return null;
    }

    @Override
    public Integer getPortProxy() {
        return portProxy;
    }

    @Override
    public void setPortProxy(Integer portProxy) {
        this.portProxy = portProxy;
    }

    private String aeTitlePostTreatment(String inAETitle) {
        if (inAETitle.length() < MAX_LENGTH_FOR_AETITLE) {
            return inAETitle.toUpperCase(Locale.getDefault()).replaceAll("([^A-Z0-9_])", "_");
        } else {
            return inAETitle.substring(0, MAX_LENGTH_FOR_AETITLE).toUpperCase(Locale.getDefault()).replaceAll("([^A-Z0-9_])", "_");
        }
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> headers = super.getCSVHeaders();
        headers.add("aeTitle");
        headers.add("sopClass");
        headers.add("transferRole");
        addCSVServerHeaders(this, headers);
        return headers;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> values = super.getCSVValues();

        if (this.aeTitle != null) {
            values.add(this.aeTitle);
        } else {
            values.add("");
        }
        if (this.sopClass != null) {
            values.add(this.sopClass.getKeyword());
        } else {
            values.add("");
        }
        if (this.transferRole != null) {
            values.add(this.transferRole);
        } else {
            values.add("");
        }

        addCSVServerValues(this, values);
        return values;
    }
}
