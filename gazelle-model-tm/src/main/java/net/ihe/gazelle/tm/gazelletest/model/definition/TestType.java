/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class TestType : </b>TestType<br>
 * <br>
 * This class describes the Test type object, used by the Gazelle application. This class belongs to the Technical Framework module. This class should contain two type of test : Connectathon and Mesa
 *
 * @author Jean-Baptiste MEYER / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                         http://ihe.irisa.fr
 *                         </pre>
 *         <p>
 *         <pre>
 *                         jmeyer@irisa.fr
 *                         </pre>
 * @version 1.0 , 08 jun. 2009
 * @class TestType.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "testType")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_type", schema = "public")
@SequenceGenerator(name = "tm_test_type_sequence", sequenceName = "tm_test_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestType extends AuditedObject implements Serializable {


    public static final String TYPE_CONNECTATHON_STRING = "connectathon";
    public static final String TYPE_MESA_STRING = "pre-connectathon";
    public static final String TYPE_INTEROPERABILITY_STRING = "interoperability-testbed";
    private static final long serialVersionUID = 514902951514749L;
    private static TestType TYPE_CONNECTATHON;
    private static TestType TYPE_MESA;
    private static TestType TYPE_INTEROPERABILITY;

    private static Logger log = LoggerFactory.getLogger(TestType.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_type_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "objectUsageType", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    private Set<ObjectInstance> objectInstances;

    public TestType() {
    }

    public TestType(TestType inTestType) {
        if (inTestType.getDescription() != null) {
            this.description = inTestType.getDescription();
        }
        if (inTestType.getKeyword() != null) {
            this.keyword = inTestType.getKeyword();
        }
        if (inTestType.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestType.getLabelToDisplay();
        }
    }

    public TestType(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public static TestType getTYPE_CONNECTATHON() {
        if (TYPE_CONNECTATHON == null) {
            TYPE_CONNECTATHON = getTypeByKeyword(TYPE_CONNECTATHON_STRING);
        }

        return TYPE_CONNECTATHON;
    }

    public static TestType getTYPE_MESA() {
        if (TYPE_MESA == null) {
            TYPE_MESA = getTypeByKeyword(TYPE_MESA_STRING);
        }

        return TYPE_MESA;
    }

    public static TestType getTYPE_INTEROPERABILITY() {
        if (TYPE_INTEROPERABILITY == null) {
            TYPE_INTEROPERABILITY = getTypeByKeyword(TYPE_INTEROPERABILITY_STRING);
        }
        return TYPE_INTEROPERABILITY;
    }

    public static TestType getTypeByKeyword(String keyword) {
        TestTypeQuery query = new TestTypeQuery();
        query.keyword().eq(keyword);
        return query.getUniqueResult();
    }

    public static List<TestType> getTypeList() {
        TestTypeQuery q = new TestTypeQuery();
        q.setCachable(true);
        return q.getList();
    }

    public static List<TestType> getTestTypesWithoutMESA() {
        TestTypeQuery testTypeQuery = new TestTypeQuery();
        testTypeQuery.keyword().order(true);
        testTypeQuery.id().neq(TestType.getTYPE_MESA().getId());
        return testTypeQuery.getList();
    }

    public static List<TestType> getTestTypesWithoutMESAandITB() {
        TestTypeQuery testTypeQuery = new TestTypeQuery();
        testTypeQuery.keyword().order(true);
        testTypeQuery.id().neq(TestType.getTYPE_MESA().getId());
        testTypeQuery.id().neq(TestType.getTYPE_INTEROPERABILITY().getId());
        return testTypeQuery.getList();
    }

    public Set<ObjectInstance> getObjectInstances() {
        return HibernateHelper.getLazyValue(this, "objectInstances", this.objectInstances);
    }

    public void setObjectInstances(Set<ObjectInstance> objectInstances) {
        this.objectInstances = objectInstances;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @FilterLabel
    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        TestTypeQuery query = new TestTypeQuery();
        query.keyword().eq(this.keyword);
        List<TestType> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
