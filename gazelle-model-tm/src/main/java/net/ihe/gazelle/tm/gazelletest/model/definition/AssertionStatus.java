package net.ihe.gazelle.tm.gazelletest.model.definition;

public class AssertionStatus {

    private String assertionId;
    private String idScheme;
    private int testStepId;
    private AssertionStatusEnum status;

    public AssertionStatus(String assertionId, String idScheme, int testStepId) {
        super();
        this.assertionId = assertionId;
        this.idScheme = idScheme;
        this.testStepId = testStepId;
    }

    public AssertionStatus(String assertionId, int testStepId) {
        super();
        this.assertionId = assertionId;
        int indexOfHyphen = assertionId.indexOf("-");
        this.idScheme = assertionId.substring(0, indexOfHyphen);
        this.testStepId = testStepId;
    }

    public String getAssertionId() {
        return assertionId;
    }

    public void setAssertionId(String assertionId) {
        this.assertionId = assertionId;
    }

    public String getIdScheme() {
        return idScheme;
    }

    public void setIdScheme(String idScheme) {
        this.idScheme = idScheme;
    }

    public AssertionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AssertionStatusEnum status) {
        this.status = status;
    }

    public void checkStatus() {

    }

    public int getTestStepId() {
        return testStepId;
    }

    public void setTestStepId(int testStepId) {
        this.testStepId = testStepId;
    }

    public enum AssertionStatusEnum {

        LINKED("Linked"),
        AVAILABLE("Available"),
        DOESNTEXISTS("Assertion doesn't exists");

        private String friendlyName;

        AssertionStatusEnum(String friendlyName) {
            this.friendlyName = friendlyName;
        }

        public String getFriendlyName() {
            return friendlyName;
        }
    }

}
