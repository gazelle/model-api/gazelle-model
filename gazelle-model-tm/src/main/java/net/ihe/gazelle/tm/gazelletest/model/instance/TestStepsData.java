/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.sync.SetGazelle;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.jboss.seam.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * @author aboufahj
 */

@Entity
@Table(name = "tm_test_steps_data", schema = "public")
@SequenceGenerator(name = "tm_test_steps_data_sequence", sequenceName = "tm_test_steps_data_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class TestStepsData extends AuditedObject implements Serializable, Comparable<TestStepsData>,
        EVSClientValidatedObject {

    private static final long serialVersionUID = 6953227034420364183L;
    private static Logger log = LoggerFactory.getLogger(TestStepsData.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_steps_data_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "data_type_id")
    @Fetch(value = FetchMode.SELECT)
    private DataType dataType;

    @Column(name = "value")
    @Lob
    @Type(type = "text")
    private String value;

    @Column(name = "comment")
    @Lob
    @Type(type = "text")
    private String comment;

    @Column(name = "withoutFileName")
    private Boolean withoutFileName;

    public TestStepsData() {
    }

    public TestStepsData(Integer id, DataType dataType, String comment, String value) {
        super();
        this.id = id;
        this.dataType = dataType;
        this.comment = comment;
        this.value = value;
    }

    public static String createURL(String value, DataType dt) {
        String url = null;
        if (dt != null) {
            if (value != null) {
                if (dt.equals(DataType.getDataTypeByKeyword(DataType.OBJECT_DT))) {
                    url = PreferenceService.getString("application_url") + "objects/sample.seam?id=" + value;
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.PROXY_DT))) {
                    url = PreferenceService.getString("gazelle_proxy_url") + value;
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.DATAHOUSE_DT))) {
                    url = PreferenceService.getString("gazelle_datahouse_ui_base_url") + value;
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.GSS_DT))) {
                    String tlsUrl = ApplicationPreference.getTlsUrl();
                    if (tlsUrl != null) {
                        url = tlsUrl + value;
                    }
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.EVS_DT))) {
                    url = PreferenceService.getString("evs_client_url") + value;
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.XDS_DT))) {
                    url = value;
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.URL))) {
                    url = value;
                } else if (dt.equals(DataType.getDataTypeByKeyword(DataType.AUTOMATION_INPUT_DT))) {
                    url = value;
                }
            }
        }
        return url;
    }

    public static void removeTestStepsData(TestStepsData tsd, EntityManager entityManager) {
        tsd = entityManager.find(TestStepsData.class, tsd.getId());
        if (tsd != null) {
            entityManager.remove(tsd);
            entityManager.flush();
        }
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public String getComment() {
        return comment;
    }

    public Boolean getWithoutFileName() {
        return withoutFileName;
    }

    public void setWithoutFileName(Boolean withoutFileName) {
        this.withoutFileName = withoutFileName;
    }

    public String getTypeLabel() {
        return "net.ihe.gazelle.tm.dataType." + dataType.getKeyword();
    }

    public boolean isFile() {
        return dataType.isFile();
    }

    public boolean isComment() {
        return dataType.equals(DataType.getDataTypeByKeyword(DataType.COMMENT));
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String createURL() {
        return TestStepsData.createURL(this.value, this.dataType);
    }

    public String getDisplay() {
        if (StringUtils.isNotEmpty(comment)) {
            return value + " (" + comment + ")";
        } else {
            return value;
        }
    }

    @Override
    public int compareTo(TestStepsData o) {
        if (getId() == null) {
            return 1;
        }
        if (o == null) {
            return -1;
        }
        if (o.getId() == null) {
            return -1;
        }
        return getId().compareTo(o.getId());
    }

    public String getCompleteFilePath() {
        StringBuilder filepath = new StringBuilder();
        if ((this.getWithoutFileName() != null) && this.getWithoutFileName()) {
            filepath.append(PreferenceService.getString("gazelle_home_path"));
            filepath.append(java.io.File.separatorChar);
            filepath.append(PreferenceService.getString("data_path"));
            filepath.append(java.io.File.separatorChar);
            filepath.append(PreferenceService.getString("file_steps_path"));
            filepath.append(java.io.File.separatorChar);
            filepath.append(this.getId());
        } else {
            filepath.append(PreferenceService.getString("gazelle_home_path"));
            filepath.append(java.io.File.separatorChar);
            filepath.append(PreferenceService.getString("data_path"));
            filepath.append(java.io.File.separatorChar);
            filepath.append(PreferenceService.getString("file_steps_path"));
            filepath.append(java.io.File.separatorChar);
            filepath.append(this.getId());
            filepath.append('_');
            filepath.append(this.getValue());
        }
        return filepath.toString();
    }

    @Override
    public String getUniqueKey() {
        return "stepdata_" + id;
    }

    @Override
    public PartSource getPartSource() {
        String filepath = getCompleteFilePath();
        File fileToValidate = new File(filepath);
        if (fileToValidate.exists()) {
            String content = null;
            content = Base64.encodeFromFile(filepath);
            return new ByteArrayPartSource(getType(), content.getBytes(Charset.forName("UTF-8")));
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return "stepdata";
    }

}
