package net.ihe.gazelle.tm.application.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tm_section_type", schema = "public")
@SequenceGenerator(name = "tm_section_type_sequence", sequenceName = "tm_section_type_id_seq", allocationSize = 1)
public class SectionType {

    private static final long serialVersionUID = 5149058517625532787L;

    @Id
    @GeneratedValue(generator = "tm_section_type_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    public SectionType() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
