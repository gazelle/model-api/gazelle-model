package net.ihe.gazelle.tm.gazelletest.jira;

import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Audited
@Table(name = "tm_jira_issues_to_test", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "jira_key", "test_id"}))
@SequenceGenerator(name = "tm_jira_issues_to_test_sequence", sequenceName = "tm_jira_issues_to_test_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class JiraIssuesToTest {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_jira_issues_to_test_sequence")
    @XmlTransient
    private Integer id;
    @Column(name = "jira_key")
    private String key;
    @Column(name = "jira_summary")
    private String summary;
    @Column(name = "jira_status")
    private String status;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_id")
    private Test test;

    public JiraIssuesToTest(String key, String summary, String status, Test test) {
        super();
        this.key = key;
        this.summary = summary;
        this.status = status;
        this.test = test;
    }

    public JiraIssuesToTest(String key, String summary, String status) {
        super();
        this.key = key;
        this.summary = summary;
        this.status = status;
    }

    public JiraIssuesToTest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public String toString() {
        return key + " " + status + " " + summary;
    }

    public JiraIssuesToTest alreadyExists() {
        if (test != null) {
            JiraIssuesToTestQuery query = new JiraIssuesToTestQuery();
            query.setCachable(false);
            query.key().eq(key);
            query.test().id().eq(test.getId());
            JiraIssuesToTest jiraissueToTest = query.getUniqueResult();
            return jiraissueToTest;
        }
        return null;
    }
}
