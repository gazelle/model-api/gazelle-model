/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.LabelKeywordDescriptionClass;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import net.ihe.gazelle.translate.TranslateService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>Status<br>
 * <br>
 * This class describes the Status for results object, used by the Gazelle application.
 * <p/>
 * Status possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Status</li>
 * <li><b>keyword</b> : keyword of the Status.</li>
 * <li><b>description</b> : description of the Status</li>
 * </ul>
 * </br>
 *
 * @author JB_Meyer / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         jmeyer@irisa.fr
 *         </pre>
 * @version 1.0 , 27 janv. 2009
 * @class Status.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@Entity
@Table(name = "tm_status_results", schema = "public")
@SequenceGenerator(name = "tm_status_results_sequence", sequenceName = "tm_status_results_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class StatusResults extends LabelKeywordDescriptionClass implements Serializable {


    private static final long serialVersionUID = -5678858537129119731L;

    private final static String STATUS_PASSED_STRING = "passed";
    private final static String STATUS_FAILED_STRING = "failed";
    private final static String STATUS_AT_RISK_STRING = "atrisk";
    private final static String STATUS_NO_GRADING_STRING = "nograding";
    private final static String STATUS_WITHDRAWN = "withdrawn";
    private final static String STATUS_NO_PEER = "nopeer";


    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_status_results_sequence")
    private Integer id;

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public StatusResults() {
        super();
    }

    public StatusResults(LabelKeywordDescriptionClass inLabelKeywordDescriptionClass) {
        super(inLabelKeywordDescriptionClass);
    }
    public StatusResults(String keyword, String labelToDisplay, String description) {
        this(keyword, labelToDisplay, description, null);
    }

    public StatusResults(String keyword, String labelToDisplay, String description, Integer rank) {
        super(keyword, labelToDisplay, description, rank);
    }

    public static StatusResults getSTATUS_RESULT_PASSED() {
        return StatusResults.getStatusByKeyword(STATUS_PASSED_STRING);
    }

    public static StatusResults getSTATUS_RESULT_FAILED() {
        return StatusResults.getStatusByKeyword(STATUS_FAILED_STRING);
    }

    public static StatusResults getSTATUS_AT_RISK() {
        return StatusResults.getStatusByKeyword(STATUS_AT_RISK_STRING);
    }

    public static StatusResults getSTATUS_NO_GRADING() {
        return StatusResults.getStatusByKeyword(STATUS_NO_GRADING_STRING);
    }

    public static StatusResults getStatusByKeyword(String keyword) {
        StatusResultsQuery query = new StatusResultsQuery();
        query.keyword().eq(keyword);
        return query.getUniqueResult();
    }

    public static List<StatusResults> getListStatusResults() {
        StatusResultsQuery query = new StatusResultsQuery();
        query.keyword().order(true);
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StatusResults [getKeyword()=");
        builder.append(getKeyword());
        builder.append("]");
        return builder.toString();
    }

    @FilterLabel
    public String getFilterLabel() {
        return TranslateService.getTranslation(getLabelToDisplay());
    }

}
