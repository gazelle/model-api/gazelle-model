/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.csv.CSVExportable;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.tools.StringIP;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description :  </b>Host<br><br>
 * This class describes the host configuration.
 * <p/>
 * </ul></br>
 */

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @class Host.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */
@Entity
@Table(name = "tm_host", schema = "public")
@SequenceGenerator(name = "tm_host_sequence", sequenceName = "tm_host_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Host extends AuditedObject implements Serializable, CSVExportable {
    // ~ Statics variables and Class initializer
    // ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = 124392223397281644L;
    private static final String WITHOUT_UNDERSCORE = "^((?!_).)*$";
    private static Logger log = LoggerFactory.getLogger(Host.class);
    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_host_sequence")
    private Integer id;

    // ~ Attributes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * hostname attribute ,
     */
    @Column(name = "hostname")
    @NotNull
    @Pattern(regexp = WITHOUT_UNDERSCORE, message = "Underscore are forbidden")
    private String hostname;
    @Column(name = "ignore_domain_name")
    private Boolean ignoreDomainName;
    /**
     * Ip of the System configuration
     */
    @Column(name = "ip")
    @Pattern(regexp = NetworkConfigurationForTestingSession._stringIPRegex_, message = "Should be a valid IP Address")
    private String ip;
    @Deprecated
    @Transient
    private StringIP ipString;
    /**
     * Comments on this configuration
     */
    @Column(name = "comment")
    private String comment;

    /**
     * Alias of the configuration available
     */
    @Column(name = "alias")
    @Pattern(regexp = WITHOUT_UNDERSCORE, message = "Underscore are forbidden")
    private String alias;

    @OneToMany(mappedBy = "host", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<Configuration> configurations;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "testing_session_id")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    @ManyToOne
    @JoinColumn(name = "institution_id")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Institution institution;

    @Column(name = "used_by_more_one_system_in_session")
    private boolean usedByMoreOneSystemInSession;

    /**
     * Creates a new Host object.
     */
    public Host() {
    }

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new Host object.
     *
     * @param inHostname hostname for this configuration
     */
    public Host(final String inHostname) {
        this.hostname = inHostname;
    }

    public Host(Host inHost) {
        if (inHost.getAlias() != null) {
            this.alias = inHost.getAlias();
        }
        if (inHost.getComment() != null) {
            this.comment = inHost.getComment();
        }
        if (inHost.getHostname() != null) {
            this.hostname = inHost.getHostname();
        }
        if (inHost.getIp() != null) {
            this.ip = inHost.getIp();
        }
    }

    public static Host getAvailableHostForASystemInSession(List<Host> hosts, SystemInSession sIs) {
        if (hosts != null) {
            for (Host host : hosts) {
                if (host.getSystemsInSession() == null || host.getSystemsInSession().isEmpty() || host.getSystemsInSession().contains(sIs)) {
                    return host;
                }
            }
        }
        return null;
    }

    public static List<Host> getNetworkConfigurationsForSystemInSession(SystemInSession inSystemSession) {
        HostQuery q = new HostQuery();
        q.testingSession().eq(inSystemSession.getTestingSession());
        q.institution().eq(inSystemSession.getSystem().getUniqueInstitution());
        List<Host> listConf = q.getList();
        return listConf;
    }

    public static List<Host> getListOfHostForASession(TestingSession currentSession) {
        HostQuery q = new HostQuery();
        q.testingSession().eq(currentSession);
        q.hostname().order(true);
        return q.getList();
    }

    public static List<Host> getListOfHostForASystemInSession(SystemInSession inSystemInSession) {
        HostQuery query = new HostQuery();
        query.testingSession().eq(inSystemInSession.getTestingSession());
        query.institution().eq(inSystemInSession.getSystem().getUniqueInstitution());
        return query.getList();
    }

    public static List<Host> getListOfHostForASystemInSessionAsAdmin(SystemInSession inSystemInSession) {
        List<Institution> institutions = System.getInstitutionsForASystem(inSystemInSession.getSystem());
        HostQuery query = new HostQuery();
        query.testingSession().eq(inSystemInSession.getTestingSession());
        query.institution().in(institutions);
        query.hostname().order(true);
        // GZL-4821: only display hosts for non-dropped systems, include hosts without configurations
        query.addRestriction(HQLRestrictions.or(
                query.configurations().isEmptyRestriction(),
                query.configurations().systemInSession().registrationStatus().neqRestriction(SystemInSessionRegistrationStatus.DROPPED)));
        return query.getListDistinctOrdered();
    }

    public synchronized static Host FindHostWithHostname(String host) {
        HostQuery q = new HostQuery();
        q.hostname().eq(host);
        Host host1 = null;
        try {
            host1 = q.getUniqueResult();
        } catch (HibernateException e) {
            log.error("hostname : " + host);
            log.error("" + e);
            host1 = new Host("ERROR");
        }
        return host1;
    }

    /**
     * Generate a new host with a hostname incremented ( CompanyKeywordN+1) and persist it
     *
     * @return the host persisted
     */

    public synchronized static Host generateNewHostForSessionWithPersistance(Institution company, SystemInSession inSystemInSession) {
        if ((company == null) || (inSystemInSession == null)) {
            return null;
        }

        EntityManager entityManager = EntityManagerService.provideEntityManager();

        Host hostToPersist = new Host();
        int i = 0;
        String hostname = company.getKeyword().toLowerCase().replace(' ', '-').replace('_', '-').replace(',', '-') + i;
        Host host = FindHostWithHostname(hostname);
        Host previousHost = null;
        while (host != null && !(host.getHostname().equals("ERROR"))) {
            hostname = company.getKeyword().toLowerCase().replace(' ', '-').replace('_', '-').replace(',', '-') + (++i);
            previousHost = host;
            host = FindHostWithHostname(hostname);
        }

        if ((host == null && previousHost == null)
                || (previousHost != null && !(previousHost.getHostname().equals("ERROR")))) {
            hostToPersist.setHostname(hostname);
            hostToPersist.setInstitution(company);
            hostToPersist.setTestingSession(inSystemInSession.getTestingSession());
            if (entityManager.isOpen()) {
                entityManager.persist(hostToPersist);
                entityManager.flush();
                hostToPersist = entityManager.merge(hostToPersist);
                return hostToPersist;
            } else {
                return null;
            }
        } else {
            log.error("Duplicate Hostname in DB : " + hostname);
            FacesMessages.instance().add("Problem with hostname of " + hostname);
            return null;
        }
    }

    public static Host generateNewHostForSessionWithoutPersistance(Institution company,
                                                                   SystemInSession inSystemInSession) {
        EntityManagerService.provideEntityManager();

        Host host = new Host();
        int i = 0;
        String hostname = company.getKeyword().toLowerCase().replace(' ', '-').replace('_', '-').replace(',', '-') + i;
        while (FindHostWithHostname(hostname) != null) {
            hostname = company.getKeyword().toLowerCase().replace(' ', '-').replace('_', '-').replace(',', '-') + (++i);
        }
        host.setHostname(hostname);
        host.setInstitution(company);
        host.setTestingSession(inSystemInSession.getTestingSession());
        return host;
    }

    public static List<SystemInSession> getListOfSystemUsedWithHost(Host inHost) {
        ConfigurationQuery q = new ConfigurationQuery();
        q.host().id().eq(inHost.getId());

        List<SystemInSession> listOfSystems = q.systemInSession().getListDistinct();
        Collections.sort(listOfSystems);

        return listOfSystems;
    }

    public static String getStringListOfSystemUsedWithHost(Host inHost) {
        List<SystemInSession> listOfSystems = getListOfSystemUsedWithHost(inHost);

        StringBuffer buffer = new StringBuffer();

        for (SystemInSession sIs : listOfSystems) {
            buffer.append(sIs.getSystem().getName() + " ");
        }

        return buffer.toString();
    }

    public List<SystemInSession> getSystemsInSession() {
        HostQuery q = new HostQuery();
        q.setCachable(false);
        q.id().eq(this.id);
        return q.configurations().systemInSession().getListDistinct();
    }

    /**
     * get the Index of the configuration
     *
     * @return the index
     */
    public Integer getId() {
        return id;
    }

    /**
     * set the index of the configuration
     *
     * @param inId
     */
    public void setIndex(Integer inId) {
        this.id = inId;
    }

    /**
     * get the hostname of the configuration
     *
     * @return
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * set the hostname of the configuration
     *
     * @param inHostname
     */
    public void setHostname(String inHostname) {
        this.hostname = inHostname;
    }

    public Boolean getIgnoreDomainName() {
        return ignoreDomainName;
    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setIgnoreDomainName(Boolean ignoreDomainName) {
        this.ignoreDomainName = ignoreDomainName;
    }

    /**
     * get the Ip of the configuration
     *
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * set the Ip for the configuration
     *
     * @param inIp
     */
    public void setIp(String inIp) {
        this.ip = inIp;
    }

    /**
     * get the comment inserted for this configuration
     *
     * @return comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * set the comment for this configuration
     *
     * @param inComment
     */
    public void setComment(String inComment) {
        this.comment = inComment;
    }

    public Set<Configuration> getConfigurations() {
        return HibernateHelper.getLazyValue(this, "configurations", this.configurations);
    }

    public void setConfigurations(Set<Configuration> configurations) {
        this.configurations = configurations;
    }

    /**
     * Get the alias of this configuration
     *
     * @return alias
     */

    public String getAlias() {
        return alias;
    }

    /**
     * set an alias for this configuration
     *
     * @param inAlias
     */
    public void setAlias(String inAlias) {
        this.alias = inAlias;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public StringIP getIpString() {
        return ipString;
    }

    public void setIpString(StringIP ipString) {
        this.ipString = ipString;
    }

    public boolean isUsedByMoreOneSystemInSession() {
        return usedByMoreOneSystemInSession;
    }

    public void setUsedByMoreOneSystemInSession(boolean usedByMoreOneSystemInSession) {
        this.usedByMoreOneSystemInSession = usedByMoreOneSystemInSession;
    }

    @PrePersist
    @PreUpdate
    public void autoSetIgnoreHostname() {
        if (getTestingSession() != null && (getTestingSession().getInternetTesting() != null) && getTestingSession().getInternetTesting()) {
            setIgnoreDomainName(true);
        }
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> result = new ArrayList<String>();
        result.add("hostname");
        result.add("ip");
        result.add("alias");
        result.add("system");
        result.add("comment");
        return result;
    }

    @Override
    public List<String> getCSVValues() {

        List<String> result = new ArrayList<String>();

        if (this.hostname != null) {
            result.add(this.hostname);
        } else {
            result.add("");
        }
        if (this.ip != null) {
            result.add(this.ip);
        } else {
            result.add("");
        }
        if (this.alias != null) {
            result.add(this.alias);
        } else {
            result.add("");
        }
        if (getSystemsInSession() != null) {
            String keyword = "";
            for (SystemInSession systemInSession : getSystemsInSession()) {
                keyword = systemInSession.getSystem().getKeyword();
            }
            result.add(keyword);
        } else {
            result.add("");
        }
        if (this.comment != null) {
            result.add(this.comment);
        } else {
            result.add("");
        }

        return result;
    }
}
