package net.ihe.gazelle.tm.tee.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * The persistent class for the tm_step_instance_msg_validation database table.
 *
 * @author tnabeel
 */
@Entity
@Table(name = "tm_step_instance_msg_validation")
@SequenceGenerator(name = "tm_step_instance_msg_validation_id_generator", sequenceName = "tm_step_instance_msg_validation_sequence")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TmStepInstanceMsgValidation implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_step_instance_msg_validation_id_generator")
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed")
    private Date lastChanged;

    @Column(name = "validation_report")
    @Basic(fetch = FetchType.LAZY)
    private byte[] validationReport;

    @ManyToOne
    @JoinColumn(name = "message_validation_service_id")
    private MessageValidationService messageValidationService;

    @ManyToOne
    @JoinColumn(name = "tm_step_instance_message_id")
    private TmStepInstanceMessage tmStepInstanceMessage;

    @ManyToOne
    @JoinColumn(name = "validation_status_id")
    private ValidationStatus validationStatus;

    public TmStepInstanceMsgValidation() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getLastChanged() {
        return this.lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public byte[] getValidationReport() {
        return this.validationReport;
    }

    public void setValidationReport(byte[] validationReport) {
        this.validationReport = validationReport.clone();
    }

    public MessageValidationService getMessageValidationService() {
        return this.messageValidationService;
    }

    public void setMessageValidationService(MessageValidationService messageValidationService) {
        this.messageValidationService = messageValidationService;
    }

    public TmStepInstanceMessage getTmStepInstanceMessage() {
        return this.tmStepInstanceMessage;
    }

    public void setTmStepInstanceMessage(TmStepInstanceMessage tmStepInstanceMessage) {
        this.tmStepInstanceMessage = tmStepInstanceMessage;
    }

    public ValidationStatus getValidationStatus() {
        return this.validationStatus;
    }

    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getValidationReportAsString() {
        return new String(validationReport, Charset.forName("UTF-8"));
    }

}