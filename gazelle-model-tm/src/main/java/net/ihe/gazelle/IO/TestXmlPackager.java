package net.ihe.gazelle.IO;

import net.ihe.gazelle.tm.gazelletest.model.definition.Test;

import javax.persistence.EntityManager;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "TestsPackage")
@XmlAccessorType(XmlAccessType.FIELD)
public class TestXmlPackager {

    private List<Test> tests;

    public TestXmlPackager() {
    }

    public TestXmlPackager(List<Test> tests) {
        super();
        this.tests = tests;
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public int[] saveOrMerge(EntityManager entityManager) {
        int totalTests = tests.size();
        int counter = 1;
        int[] testsIds = new int[totalTests];

        for (Test test : tests) {
            testsIds[counter - 1] = test.saveOrMerge(entityManager, counter, totalTests);
            counter++;
        }
        return testsIds;
    }
}
