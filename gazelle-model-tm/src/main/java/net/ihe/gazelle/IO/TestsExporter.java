package net.ihe.gazelle.IO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class TestsExporter {
    private static Logger log = LoggerFactory.getLogger(TestsExporter.class);
    private JAXBContext jaxbContext;
    private ByteArrayOutputStream outputStream;

    public TestsExporter() {
        super();
        try {
            this.jaxbContext = JAXBContext.newInstance(TestXmlPackager.class);
        } catch (JAXBException e) {
            log.error("Failed to initialize TestsIO");
            log.error("JAXBContext cannot create an instance of TestXmlPackager");
        }
    }

    public boolean exportTest(net.ihe.gazelle.tm.gazelletest.model.definition.Test test) throws JAXBException {
        List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> testList = new ArrayList<net.ihe.gazelle.tm.gazelletest.model.definition.Test>();
        testList.add(test);
        return exportTests(testList);
    }

    public boolean exportTests(List<net.ihe.gazelle.tm.gazelletest.model.definition.Test> tests) throws JAXBException {

        boolean status = true;
        TestXmlPackager testsToExport = new TestXmlPackager(tests);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        try {
            outputStream = new ByteArrayOutputStream();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(testsToExport, outputStream);
        } catch (JAXBException e) {
            status = false;
            log.error("Failed to export tests " + e.getCause().getMessage());
        }

        return status;
    }

    public void write(OutputStream os) throws IOException {
        os.write(outputStream.toByteArray(), 0, outputStream.size());
    }
}
