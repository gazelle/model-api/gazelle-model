/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>ObjectInstance<br>
 * <br>
 * This class describes the ObjectInstance object, used by the Gazelle application.
 * <p/>
 * This class belongs to the TM module.
 * <p/>
 * ObjectInstance possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectInstance id</li>
 * <li><b>object</b> : Object Type corresponding</li>
 * <li><b>system</b> : System In Session corresponding</li>
 * <li><b>location</b> : the location of objectInstance</li>
 * <li><b>name</b>
 * <li><b>description</b>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class ObjectInstance.java
 * @package net.ihe.gazelle.objects.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@Entity
@Table(name = "tm_object_instance")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_instance_sequence", sequenceName = "tm_object_instance_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ObjectInstance extends AuditedObject implements Serializable, Comparable<ObjectInstance> {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1881422500711441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectInstance id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_instance_sequence")
    private Integer id;

    /**
     * Object
     */
    @ManyToOne
    @JoinColumn(name = "object_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ObjectType object;

    /**
     * Keyword
     */
    @ManyToOne
    @JoinColumn(name = "system_in_session_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private SystemInSession system;

    /**
     * Description
     */
    @Column(name = "name", unique = false, nullable = true)
    @Size(max = 250)
    private String name;

    /**
     * Location
     */
    @Column(name = "description", unique = false, nullable = true)
    @Lob
    @Type(type = "text")
    private String description;

    @ManyToOne
    @JoinColumn(name = "validation_id", unique = false, nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private ObjectInstanceValidation validation;

    @ManyToOne
    @JoinColumn(name = "test_type_id", unique = false, nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private TestType objectUsageType;

    /**
     * Completed
     */
    @Column(name = "completed")
    private boolean completed;

    @OneToMany(mappedBy = "objectInstance", fetch = FetchType.LAZY)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectInstanceAnnotation> objectInstanceAnnotations;

    @OneToMany(mappedBy = "instance", fetch = FetchType.LAZY)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectInstanceAttribute> objectInstanceAttributes;

    @OneToMany(mappedBy = "instance", fetch = FetchType.LAZY)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectInstanceFile> objectInstanceFiles;

    // constructors //////////////////////////////////////////////////////////////////////////////////////////////////////////
    public ObjectInstance() {
    }

    public ObjectInstance(ObjectType object, SystemInSession system, String description, String name,
                          boolean completed, ObjectInstanceValidation oiv) {
        this.object = object;
        this.system = system;
        this.description = description;
        this.name = name;
        this.completed = completed;
        this.validation = oiv;
    }

    public ObjectInstance(ObjectInstance OIns) {
        if (OIns.description != null) {
            this.description = OIns.description;
        }
        if (OIns.name != null) {
            this.name = OIns.name;
        }
        if (OIns.object != null) {
            this.object = OIns.object;
        }
        if (OIns.system != null) {
            this.system = OIns.system;
        }
        if (OIns.validation != null) {
            this.validation = OIns.validation;
        }
        this.completed = OIns.completed;
        if (OIns.objectUsageType != null) {
            this.objectUsageType = OIns.objectUsageType;
        }
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void deleteObjectInstance(ObjectType objectType) {
        if (objectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectInstance> list_objectInstance = ObjectInstance.getObjectInstanceFiltered(objectType, null, null);
            for (ObjectInstance objectInstance : list_objectInstance) {
                ObjectInstanceAnnotation.deleteObjectInstanceAnnotation(objectInstance);
                ObjectInstanceFile.deleteObjectInstanceFile(objectInstance);
                ObjectInstanceAttribute.deleteObjectInstanceAttribute(objectInstance);
                ObjectInstance objectInstanceToRemove = entityManager
                        .find(ObjectInstance.class, objectInstance.getId());
                entityManager.remove(objectInstanceToRemove);
            }
        }
    }

    public static void deleteObjectInstance(SystemInSession inSystemInSession) {
        if (inSystemInSession != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectInstance> list_objectInstance = ObjectInstance.getObjectInstanceFiltered(null,
                    inSystemInSession, null);
            for (ObjectInstance objectInstance : list_objectInstance) {
                ObjectInstanceAnnotation.deleteObjectInstanceAnnotation(objectInstance);
                ObjectInstanceFile.deleteObjectInstanceFile(objectInstance);
                ObjectInstanceAttribute.deleteObjectInstanceAttribute(objectInstance);
                ObjectInstance objectInstanceToRemove = entityManager
                        .find(ObjectInstance.class, objectInstance.getId());
                entityManager.remove(objectInstanceToRemove);
            }
        }
    }

    public static void deleteObjectInstance(ObjectInstance objectInstance) {
        if (objectInstance != null) {
            ObjectInstanceAnnotation.deleteObjectInstanceAnnotation(objectInstance);
            ObjectInstanceFile.deleteObjectInstanceFile(objectInstance);
            ObjectInstanceAttribute.deleteObjectInstanceAttribute(objectInstance);
            ObjectInstance.deleteObjectInstanceFromDisc(objectInstance);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectInstance objectInstanceToRemove = entityManager.find(ObjectInstance.class, objectInstance.getId());
            entityManager.remove(objectInstanceToRemove);
        }
    }

    public static void deleteObjectInstanceFromDisc(ObjectInstance inObjectInstance) {
        if (inObjectInstance != null) {
            String objectpath = getObjectsPath();
            String objectinstancepath = objectpath + java.io.File.separatorChar + inObjectInstance.getId();
            java.io.File fileToDelete = new java.io.File(objectinstancepath);
            if (fileToDelete.exists()) {
                fileToDelete.delete();
            }
        }
    }

    public static String getObjectsPath() {
        return PreferenceService.getString("gazelle_home_path") + File.separatorChar
                + PreferenceService.getString("data_path") + File.separatorChar
                + PreferenceService.getString("objects_path");
    }

    public static List<ObjectInstance> getListObjectInstance() {
        ObjectInstanceQuery query = new ObjectInstanceQuery();
        return query.getList();
    }

    public static List<ObjectInstance> getObjectInstanceFiltered(ObjectType inObjectType,
                                                                 SystemInSession inSystemInSession, TestingSession inTestingSession) {
        return getObjectInstanceFiltered(inObjectType, inSystemInSession, inTestingSession, null);
    }

    protected static ObjectInstanceQuery getQuery(ObjectType inObjectType, SystemInSession inSystemInSession,
                                                  TestingSession inTestingSession, String name) {
        ObjectInstanceQuery query = new ObjectInstanceQuery();
        query.object().eqIfValueNotNull(inObjectType);
        query.system().eqIfValueNotNull(inSystemInSession);
        query.system().testingSession().eqIfValueNotNull(inTestingSession);
        query.name().eqIfValueNotNull(name);
        return query;
    }

    public static List<ObjectInstance> getObjectInstanceFiltered(ObjectType inObjectType,
                                                                 SystemInSession inSystemInSession, TestingSession inTestingSession, String name) {
        ObjectInstanceQuery query = getQuery(inObjectType, inSystemInSession, inTestingSession, name);
        return query.getList();
    }

    public static int getNumberObjectInstanceFiltered(ObjectType inObjectType, SystemInSession inSystemInSession,
                                                      TestingSession inTestingSession, String name) {
        ObjectInstanceQuery query = getQuery(inObjectType, inSystemInSession, inTestingSession, name);
        return query.getCount();
    }

    public static ObjectInstance getObjectInstanceById(int id) {
        ObjectInstanceQuery query = new ObjectInstanceQuery();
        query.id().eq(id);
        return query.getUniqueResult();
    }

    public TestType getObjectUsageType() {
        if (this.objectUsageType == null) {
            objectUsageType = TestType.getTYPE_CONNECTATHON();
        }
        return objectUsageType;
    }

    public void setObjectUsageType(TestType objectUsageType) {
        this.objectUsageType = objectUsageType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ObjectType getObject() {
        return object;
    }

    public void setObject(ObjectType object) {
        this.object = object;
    }

    public SystemInSession getSystem() {
        return system;
    }

    public void setSystem(SystemInSession system) {
        this.system = system;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    // ~ Methods ////////////////////////////////////////////////////////////////////

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Set<ObjectInstanceAnnotation> getObjectInstanceAnnotations() {
        return HibernateHelper.getLazyValue(this, "objectInstanceAnnotations", this.objectInstanceAnnotations);
    }

    public void setObjectInstanceAnnotations(Set<ObjectInstanceAnnotation> objectInstanceAnnotations) {
        this.objectInstanceAnnotations = objectInstanceAnnotations;
    }

    public Set<ObjectInstanceAttribute> getObjectInstanceAttributes() {
        return HibernateHelper.getLazyValue(this, "objectInstanceAttributes", this.objectInstanceAttributes);
    }

    public void setObjectInstanceAttributes(Set<ObjectInstanceAttribute> objectInstanceAttributes) {
        this.objectInstanceAttributes = objectInstanceAttributes;
    }

    public Set<ObjectInstanceFile> getObjectInstanceFiles() {
        return HibernateHelper.getLazyValue(this, "objectInstanceFiles", this.objectInstanceFiles);
    }

    public void setObjectInstanceFiles(Set<ObjectInstanceFile> objectInstanceFiles) {
        this.objectInstanceFiles = objectInstanceFiles;
    }

    public ObjectInstanceValidation getValidation() {
        return validation;
    }

    public void setValidation(ObjectInstanceValidation validation) {
        this.validation = validation;
    }

    @Override
    public int compareTo(ObjectInstance o) {
        if (this.getName().compareToIgnoreCase(o.getName()) > 0) {
            return 1;
        }
        if (this.getName().compareToIgnoreCase(o.getName()) < 0) {
            return (-1);
        }
        return 0;
    }

    public String getPermanentLink() {
        String res = "";
        if (this.getId() != null) {
            String pl = PreferenceService.getString("application_url") + "objects/sample.seam?id=" + this.getId();
            return pl;
        }
        return res;
    }

}
