/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.LabelKeywordDescriptionClass;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>ObjectTypeStatus<br>
 * <br>
 * This class describes the TestStatus object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestStatus possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestStatus</li>
 * <li><b>keyword</b> : keyword of the TestStatus.</li>
 * <li><b>description</b> : description of the TestStatus</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the TestStatus</li>
 * </ul>
 * </br>
 *
 * @author Abderrazek Boufahja - Abdalah miladi / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         aboufahj@irisa.fr
 *         </pre>
 * @version 1.0 , 9 dec 2009
 * @class ObjectTypeStatus.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "objectTypeStatus")
@Entity
@Audited
@Table(name = "tm_object_type_status", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"keyword"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_type_status_sequence", sequenceName = "tm_object_type_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectTypeStatus extends LabelKeywordDescriptionClass implements Serializable {


    private static final long serialVersionUID = -7731805561694624913L;

    private final static String STATUS_READY_STRING = "ready";
    private final static String STATUS_DEPRECATED_STRING = "deprecated";
    private final static String STATUS_TOBECOMPLETED_STRING = "to be completed";
    private final static String STATUS_STORAGESUBSTITUTE_STRING = "storage/substitute";

    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_type_status_sequence")
    private Integer id;

    @XmlTransient
    @OneToMany(mappedBy = "objectTypeStatus", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectType> objectTypes;

    public ObjectTypeStatus() {
        super();
    }

    public ObjectTypeStatus(LabelKeywordDescriptionClass inLabelKeywordDescriptionClass) {
        super(inLabelKeywordDescriptionClass);
    }

    public ObjectTypeStatus(String keyword, String labelToDisplay, String description) {
        this(keyword, labelToDisplay, description, null);
    }
    public ObjectTypeStatus(String keyword, String labelToDisplay, String description, Integer rank) {
        super(keyword, labelToDisplay, description, rank);
    }

    public static ObjectTypeStatus getStatusByKeyword(String status) {
        ObjectTypeStatusQuery query = new ObjectTypeStatusQuery();
        query.keyword().eq(status);
        return query.getUniqueResult();
    }

    public static List<ObjectTypeStatus> getListStatus() {
        ObjectTypeStatusQuery query = new ObjectTypeStatusQuery();
        query.keyword().order(true);
        return query.getList();
    }

    public static ObjectTypeStatus getSTATUS_READY() {
        return ObjectTypeStatus.getStatusByKeyword(STATUS_READY_STRING);
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static ObjectTypeStatus getSTATUS_DEPRECATED() {
        return ObjectTypeStatus.getStatusByKeyword(STATUS_DEPRECATED_STRING);
    }

    public static ObjectTypeStatus getSTATUS_STORAGESUBSTITUTE() {
        return ObjectTypeStatus.getStatusByKeyword(STATUS_STORAGESUBSTITUTE_STRING);
    }

    public static ObjectTypeStatus getSTATUS_TOBECOMPLETED() {
        return ObjectTypeStatus.getStatusByKeyword(STATUS_TOBECOMPLETED_STRING);
    }

    public Integer getId() {
        return id;
    }

    public Set<ObjectType> getObjectTypes() {
        return HibernateHelper.getLazyValue(this, "objectTypes", this.objectTypes);
    }

    public void setObjectTypes(Set<ObjectType> objectTypes) {
        this.objectTypes = objectTypes;
    }

}
