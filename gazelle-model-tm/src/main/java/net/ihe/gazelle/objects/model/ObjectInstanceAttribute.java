/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tm_object_instance_attribute")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_instance_attribute_sequence", sequenceName = "tm_object_instance_attribute_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ObjectInstanceAttribute extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 1L;

    // attributes //////////////////////////////////////////////////////////////////////////////////////////

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_instance_attribute_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "instance_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ObjectInstance instance;

    @ManyToOne
    @JoinColumn(name = "attribute_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ObjectAttribute attribute;

    @Column(name = "description", unique = false, nullable = true)
    private String description;

    // constructors /////////////////////////////////////////////////////////////////////////////////////////////
    public ObjectInstanceAttribute() {
    }

    public ObjectInstanceAttribute(ObjectInstance instance, ObjectAttribute attribute, String description) {
        this.instance = instance;
        this.attribute = attribute;
        this.description = description;
    }

    public ObjectInstanceAttribute(ObjectInstanceAttribute OIA) {
        if (OIA.instance != null) {
            this.instance = OIA.instance;
        }
        if (OIA.attribute != null) {
            this.attribute = OIA.attribute;
        }
        if (OIA.description != null) {
            this.description = OIA.description;
        }
    }

    // getter and setter /////////////////////////////////////////////////////////////////////////////////////////

    public static List<ObjectInstanceAttribute> getObjectInstanceAttributeFiltered(ObjectInstance inObjectInstance) {
        return getObjectInstanceAttributeFiltered(inObjectInstance, null);
    }

    public static List<ObjectInstanceAttribute> getObjectInstanceAttributeFiltered(ObjectInstance inObjectInstance,
                                                                                   ObjectAttribute objectAttribute) {
        ObjectInstanceAttributeQuery query = new ObjectInstanceAttributeQuery();
        query.instance().eqIfValueNotNull(inObjectInstance);
        query.attribute().eqIfValueNotNull(objectAttribute);
        return query.getList();
    }

    public static void deleteObjectInstanceAttribute(ObjectAttribute objectAttribute) {
        if (objectAttribute != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectInstanceAttribute> list_objectIAttr = getObjectInstanceAttributeFiltered(null, objectAttribute);
            for (ObjectInstanceAttribute objectIAttr : list_objectIAttr) {
                ObjectInstanceAttribute objectIAttrToRemove = entityManager.find(ObjectInstanceAttribute.class,
                        objectIAttr.getId());
                entityManager.remove(objectIAttrToRemove);
            }
        }
    }

    public static void deleteObjectInstanceAttribute(ObjectInstance objectInstance) {
        if (objectInstance != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectInstanceAttribute> list_objectInstanceAttribute = getObjectInstanceAttributeFiltered(objectInstance);
            for (ObjectInstanceAttribute objectIAttribute : list_objectInstanceAttribute) {
                ObjectInstanceAttribute objectIAttrToRemove = entityManager.find(ObjectInstanceAttribute.class,
                        objectIAttribute.getId());
                entityManager.remove(objectIAttrToRemove);
            }
        }
    }

    public static void deleteObjectInstanceAttribute(ObjectInstanceAttribute objectInstanceAttribute) {
        if (objectInstanceAttribute != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectInstanceAttribute objectIAttrToRemove = entityManager.find(ObjectInstanceAttribute.class,
                    objectInstanceAttribute.getId());
            entityManager.remove(objectIAttrToRemove);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ObjectInstance getInstance() {
        return instance;
    }

    // ~ Methods ////////////////////////////////////////////////////////////////////

    public void setInstance(ObjectInstance instance) {
        this.instance = instance;
    }

    public ObjectAttribute getAttribute() {
        return attribute;
    }

    public void setAttribute(ObjectAttribute attribute) {
        this.attribute = attribute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
