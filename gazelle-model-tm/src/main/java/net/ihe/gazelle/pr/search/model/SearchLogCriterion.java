/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.pr.search.model;

import net.ihe.gazelle.hql.HibernateHelper;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>SearchLogCriterion<br>
 * <br>
 * <p/>
 * This class describes a criterion used for a search report. This is a used by search logger. A search report is mapped with some criteria (SearchLogCriterion), the list of criteria used for a
 * search. * This class belongs to the ProductRegistry module. This class is an entity.
 * <p/>
 * SearchLogCriterion possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the SearchLogCriterion object</li>
 * <li><b>criterion</b> : String - corresponding to the criterion objectclass (eg. net.ihe.gazelle.Actor )</li>
 * <li><b>valueId</b> : Integer - Value of id of the objectclass (eg. 3 for the ActorId 3)</li>
 * <li><b>valueDate</b> : Date - Value of the publication Date</li>
 * <li><b>isBeforeDate</b> : Boolean - flag indicating if the search performed is before or after a given date (if the search criterion is a publication date)</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class SearchLogCriterion.java
 * @package net.ihe.gazelle.pr.search.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "pr_search_log_criterion", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "pr_search_log_criterion_sequence", sequenceName = "pr_search_log_criterion_id_seq", allocationSize = 1)
public class SearchLogCriterion implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -2104617132454299300L;

    // Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pr_search_log_criterion_sequence")
    private Integer id;

    /**
     * Criterion - corresponding to an object class (eg. net.ihe.gazelle.Actor )
     */
    @Column(name = "criterion")
    private String criterion;

    /**
     * Value of the object Id (if the search criterion is NOT a publication Date)
     */
    @Column(name = "value_id")
    private Integer valueId;

    /**
     * Value of the object Id (if the search criterion is NOT a publication Date)
     */
    @Column(name = "text_value")
    private String valueString;

    /**
     * Value of the Date (if the search criterion is a publication Date)
     */
    @Column(name = "value_date")
    private Date valueDate;

    /**
     * flag indicating if the search performed is before or after a given date (if the search criterion is a publication date)
     */
    @Column(name = "isBeforeDate")
    private Boolean isBeforeDate;

    // Variables used for Foreign Keys : pr_search_criteria_per_report table

    /**
     * Report associated to this criterion
     */
    @ManyToMany
    @JoinTable(name = "pr_search_criteria_per_report", joinColumns = @JoinColumn(name = "search_log_criterion_id"), inverseJoinColumns = @JoinColumn(name = "search_log_report_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "search_log_criterion_id", "search_log_report_id"}))
    private List<SearchLogReport> searchLogReport;

    // Constructor

    public SearchLogCriterion() {

    }

    public SearchLogCriterion(Integer id) {
        super();
        this.id = id;

    }

    public SearchLogCriterion(Integer id, String criterion, Integer valueId, Date valueDate, Boolean isBeforeDate) {
        super();
        this.id = id;
        this.criterion = criterion;
        this.valueId = valueId;
        this.valueDate = valueDate;
        this.isBeforeDate = isBeforeDate;
    }

    public SearchLogCriterion(Integer id, String criterion, Integer valueId, String valueString, Date valueDate,
                              Boolean isBeforeDate) {
        super();
        this.id = id;
        this.criterion = criterion;
        this.valueId = valueId;
        this.valueString = valueString;
        this.valueDate = valueDate;
        this.isBeforeDate = isBeforeDate;
    }

    // *********************************************************
    // Getters and Setters : setup the object properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCriterion() {
        return criterion;
    }

    public void setCriterion(String criterion) {
        this.criterion = criterion;
    }

    public Integer getValueId() {
        return valueId;
    }

    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public Boolean getIsBeforeDate() {
        return isBeforeDate;
    }

    public void setIsBeforeDate(Boolean isBeforeDate) {
        this.isBeforeDate = isBeforeDate;
    }

    public List<SearchLogReport> getSearchLogReport() {
        return searchLogReport;
    }

    public void setSearchLogReport(List<SearchLogReport> searchLogReport) {
        this.searchLogReport = searchLogReport;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
