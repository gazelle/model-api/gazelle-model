package net.ihe.gazelle.users.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.InstitutionQuery;
import net.ihe.gazelle.users.model.InstitutionType;

import java.util.List;

public class InstitutionFactory {

    private static int number = 1;

    public static Institution createInstitutionWithMandatoryFields() {
        Institution institution = new Institution();
        institution.setKeyword("Ker" + number);
        number += 1;
        institution.setUrl("http://kereval.com");
        institution.setName("Kereval" + number);
        institution.setInstitutionType(InstitutionTypeFactory.createInstitutionTypeWithMandatoryFields());

        institution = (Institution) DatabaseManager.writeObject(institution);
        return institution;
    }

    public static Institution createInstitutionWithNULLKeyword(InstitutionType type) {
        Institution institution = new Institution();
        institution.setKeyword("NULL");
        number += 1;
        institution.setUrl("http://kereval.com");
        institution.setName("Kereval" + String.valueOf(number));
        institution.setInstitutionType(type);

        institution = (Institution) DatabaseManager.writeObject(institution);
        return institution;
    }

    public static void cleanInstitution() {

        InstitutionQuery institutionQuery = new InstitutionQuery();
        List<Institution> institutions = institutionQuery.getListDistinct();
        for (Institution institution : institutions) {
            DatabaseManager.removeObject(institution);
        }
        InstitutionTypeFactory.cleanInstitutionType();
    }

    public static Institution provideInstitutionWithMandatoryFields() {
        Institution institution = new Institution();
        institution.setKeyword("Ker" + number);
        number += 1;
        institution.setUrl("http://kereval.com");
        institution.setName("Kereval" + number);
        institution.setInstitutionType(InstitutionTypeFactory.provideInstitutionTypeWithMandatoryFields());

        return institution;
    }
}
