package net.ihe.gazelle.users.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.users.model.InstitutionType;
import net.ihe.gazelle.users.model.InstitutionTypeQuery;

import java.util.List;

public class InstitutionTypeFactory {

    private static int number = 1;

    public static InstitutionType createInstitutionTypeWithMandatoryFields() {
        InstitutionType institutionType = provideInstitutionTypeWithMandatoryFields();
        institutionType = (InstitutionType) DatabaseManager.writeObject(institutionType);
        return institutionType;
    }

    public static void cleanInstitutionType() {
        InstitutionTypeQuery institutionTypeQuery = new InstitutionTypeQuery();
        List<InstitutionType> institutionType = institutionTypeQuery.getListDistinct();
        for (InstitutionType type : institutionType) {
            DatabaseManager.removeObject(type);
        }
    }

    public static InstitutionType provideInstitutionTypeWithMandatoryFields() {
        InstitutionType institutionType = new InstitutionType();
        institutionType.setType("Company" + number);
        number += 1;
        return institutionType;
    }
}