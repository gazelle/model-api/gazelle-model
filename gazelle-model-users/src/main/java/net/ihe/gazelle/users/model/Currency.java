/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <b>Class Description : </b>Currency<br>
 * <br>
 * This class describes the Currency object, used by the Gazelle application. It corresponds to Currency reference for an institution participating to a testing session This class belongs to the
 * TestManagement module.
 * <p/>
 * Currency possesses the following attributes :
 * <ul>
 * <li><b>keyword</b> : keyword corresponding to the Currency (example : USD, EUR...)</li>
 * <li><b>name</b> : Name of this curenncy (example : Canadian Dollar...)</li>
 * <li><b>comment</b> : Note/comment concerning this Currency</li>
 * </ul>
 * </br> <b>Example of Currency</b> : EUR, Euro, "Currency used for European registration"<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, December 16
 * @class Currency.java
 * @package net.ihe.gazelle.users.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "usr_currency", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class Currency extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 832587980009418950L;

    /**
     * Field indicating the keyword for that currency (eg. USD, EUR...)
     */
    @Id
    @Column(name = "keyword", unique = true, nullable = false, length = 12)
    @NotNull
    @Size(max = 12)
    private String keyword;

    /**
     * Field indicating the printable name for that currency
     */
    @Column(name = "name", unique = true)
    private String name;

    /**
     * Field indicating a comment on that currency
     */
    @Column(name = "comment")
    private String comment;

    // Constructors
    public Currency() {
    }

    public Currency(String keyword, String name, String comment) {
        super();
        this.keyword = keyword;
        this.name = name;
        this.comment = comment;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return keyword;
    }

    public String getId() {
        return keyword;
    }

}
